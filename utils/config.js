function index_items(){
  var items = [
    [{
      image: "/img/manage/search_prodcut.png",
      title: "搜产品",
      identify: "0",
    },
    {
      image: "/img/manage/find_elites.png",
      title: "找精英",
      identify: "1",
    }, {
      image: "/img/manage/find_client.png",
      title: "找客户",
      identify: "2",
    },
    {
      image: "/img/manage/grab_client.png",
      title: "抢客户",
      identify: "3",
    }],
    [{
      image: "/img/manage/peer_map.png",
      title: "同行地图",
      identify: "4",
    }, 
    {
      image: "/img/manage/activity_mange.png",
      title: "活动管理",
      identify: "5",
    },
    {
      image: "/img/manage/push_for_commission.png",
      title: "推广赚佣金",
      identify: "6",
    },
    {
      image: "/img/manage/calculate_tool.png",
      title: "计算工具",
      identify: "7",
    },]
  ]; 
  return items;
}

function mange_items() {
  var total_array = [
    // 第一列
    {
      line_image_path:"/img/square01.png",
      title: "产品管理",
      array: [
        // 第一行
        [{
          image: "/img/manage/mine.png",
          title: "我的产品",
          identify: "000",
        },
        {
          image: "/img/manage/wait.png",
          title: "等待发布",
          identify: "001",
        }, {
          image: "/img/manage/productCollection.png",
          title: "我的收藏",
          identify: "002",
        } ], 
        // ...其余行
      ],
    }, 
    // 第二列
    {
      line_image_path: "/img/square02.png",
      title: "管理工具",
      array: [
        // 第一行
        [
          {
            image: "/img/manage/copyOrder.png",
            title: "录单设置",
            identify: "102",
          },
          {
            image: "/img/manage/activityManage.png",
            title: "活动管理",
            identify: "100",
          },
          {
            image: "/img/manage/employee.png",
            title: "招聘管理",
            identify: "101",
          }
        ],
        // ...其余行
      ],
    },
    {
      line_image_path: "/img/square01.png",
      title: "展业工具",
      array: [
        // 第一行
        [{
          image: "/img/manage/map.png",
          title: "同行地图",
          identify: "200",
        },
        {
          image: "/img/manage/companySearch.png",
          title: "公司查询",
          identify: "201",
        }, {
          image: "/img/manage/eliteSearch.png",
          title: "精英查询",
          identify: "202",
        },
        
        ],
        // ...其余行
      ],
    },
    {
      line_image_path: "/img/square02.png",
      title: "企业管理",
      array: [
        // 第一行
        [{
          image: "/img/manage/eliteManage.png",
          title: "精英管理",
          identify: "300",
        },
        {
          image: "/img/manage/companyManage.png",
          title: "公司管理",
          identify: "301",
        }, {
          image: "/img/manage/collection.png",
          title: "收藏管理",
          identify: "302",
        },

        ],
        // ...其余行
      ],
    }
  ];
  return total_array;
}

function temp_pro_array(){
  var pro_itemDataArray = [
    {
      "pro_id": 16,
      "pro_name": "工行装修贷模板",
      "commission": 1,
      "commission_type": 0,
      "max_amount": 99,
      "open_days": 7,
      "mon_interest": 0.1,
      "limit_months": 3,
      "origin": "牛管家金融"
    },
    {
      "pro_id": 15,
      "pro_name": "徽商贷款",
      "commission": 5,
      "commission_type": 0,
      "max_amount": 5,
      "open_days": 3,
      "mon_interest": 5,
      "limit_months": 3,
      "origin": "牛管家金融"
    },
    {
      "pro_id": 12,
      "pro_name": "徽商银行贷",
      "commission": 2,
      "commission_type": 0,
      "max_amount": 50,
      "open_days": 0,
      "mon_interest": 3,
      "limit_months": 120,
      "origin": "牛管家金融"
    },
    {
      "pro_id": 11,
      "pro_name": "prodcut_04",
      "commission": 0.03,
      "commission_type": 0,
      "max_amount": 50,
      "open_days": 0,
      "mon_interest": 0.02,
      "limit_months": 36,
      "origin": "推客网络"
    },
    {
      "pro_id": 3,
      "pro_name": "合肥热门装修贷",
      "commission": 0.8,
      "commission_type": 0,
      "max_amount": 300,
      "open_days": 12,
      "mon_interest": 0.7,
      "limit_months": 36,
      "origin": "123"
    },
  ];
  return pro_itemDataArray;
}
 
function pro_filter_array(){
  var array = [
    {
      "key": "credit",
      "title": "授信方式",
      "imagePath": "/img/manage/img_trash.png",
      "filterArray": [
        {
          "title": "抵押贷",
          "selectedTitle": "抵押贷",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "信用贷",
          "selectedTitle": "信用贷",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "大数据",
          "selectedTitle": "大数据",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "信用卡",
          "selectedTitle": "信用卡",
          "matchingId": "4",
          "isSelected": false
        },
        {
          "title": "过桥类",
          "selectedTitle": "过桥类",
          "matchingId": "5",
          "isSelected": false
        },
        {
          "title": "服务类",
          "selectedTitle": "服务类",
          "matchingId": "6",
          "isSelected": false
        },
        {
          "title": "其他",
          "selectedTitle": "其他",
          "matchingId": "7",
          "isSelected": false
        }
      ]
    },
    {
      "key": "pro_condition",
      "title": "进件条件",
      "imagePath": "",
      "filterArray": [
        {
          "title": "房产",
          "selectedTitle": "房产",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "车产",
          "selectedTitle": "车产",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "保单",
          "selectedTitle": "保单",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "学历",
          "selectedTitle": "学历",
          "matchingId": "6",
          "isSelected": false
        },
        {
          "title": "营业执照",
          "selectedTitle": "营业执照",
          "matchingId": "4",
          "isSelected": false
        },
        {
          "title": "薪金流水",
          "selectedTitle": "薪金流水",
          "matchingId": "5",
          "isSelected": false
        },
        {
          "title": "其他",
          "selectedTitle": "其他",
          "matchingId": "7",
          "isSelected": false
        }
      ]
    },
    {
      "key": "mechanism",
      "title": "放款机构",
      "imagePath": "",
      "filterArray": [
        {
          "title": "正规银行",
          "selectedTitle": "正规银行",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "消费金融",
          "selectedTitle": "消费金融",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "企业机构",
          "selectedTitle": "企业机构",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "个人服务",
          "selectedTitle": "个人服务",
          "matchingId": "4",
          "isSelected": false
        }
      ]
    },
    {
      "key": "commission",
      "title": "服务佣金",
      "imagePath": "",
      "filterArray": [
        {
          "title": "0.01-1%",
          "selectedTitle": "0.01-1%",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "1.01-2%",
          "selectedTitle": "1.01-2%",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "2.01-3%",
          "selectedTitle": "2.01-3%",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "3.01-4%",
          "selectedTitle": "3.01-4%",
          "matchingId": "4",
          "isSelected": false
        },
        {
          "title": "4.01-6%",
          "selectedTitle": "4.01-6%",
          "matchingId": "5",
          "isSelected": false
        },
        {
          "title": "6%以上",
          "selectedTitle": "6%以上",
          "matchingId": "6",
          "isSelected": false
        },
        {
          "title": "一口价",
          "selectedTitle": "一口价",
          "matchingId": "8",
          "isSelected": false
        }
      ]
    },
    {
      "key": "mon_interest",
      "title": "月息区间",
      "imagePath": "",
      "filterArray": [
        {
          "title": "0.01%-0.6%",
          "selectedTitle": "0.01%-0.6%",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "0.61%-1.0%",
          "selectedTitle": "0.61%-1.0%",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "1.01%-1.5%",
          "selectedTitle": "1.01%-1.5%",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "1.51%-2.0%",
          "selectedTitle": "1.51%-2.0%",
          "matchingId": "4",
          "isSelected": false
        },
        {
          "title": "2.01%-3.0%",
          "selectedTitle": "2.01%-3.0%",
          "matchingId": "5",
          "isSelected": false
        },
        {
          "title": "3.01%以上",
          "selectedTitle": "3.01%以上",
          "matchingId": "6",
          "isSelected": false
        }
      ]
    },
    {
      "key": "pay_kind",
      "title": "还款方式",
      "imagePath": "",
      "filterArray": [
        {
          "title": "等本等息",
          "selectedTitle": "等本等息",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "等额本息",
          "selectedTitle": "等额本息",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "等额本金",
          "selectedTitle": "等额本金",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "先息后本",
          "selectedTitle": "先息后本",
          "matchingId": "4",
          "isSelected": false
        },
        {
          "title": "后息后本",
          "selectedTitle": "后息后本",
          "matchingId": "5",
          "isSelected": false
        },
        {
          "title": "复合方式",
          "selectedTitle": "复合方式",
          "matchingId": "6",
          "isSelected": false
        }
      ]
    },
    {
      "key": "open_days",
      "title": "放款时间",
      "imagePath": "",
      "filterArray": [
        {
          "title": "当天",
          "selectedTitle": "当天",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "2-5工作日",
          "selectedTitle": "2-5工作日",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "6-10工作日",
          "selectedTitle": "6-10工作日",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "11-20工作日",
          "selectedTitle": "11-20工作日",
          "matchingId": "4",
          "isSelected": false
        },
        {
          "title": "20工作日以上",
          "selectedTitle": "20工作日以上",
          "matchingId": "5",
          "isSelected": false
        }
      ]
    },
    {
      "key": "limit_months",
      "title": "借款时限",
      "imagePath": "",
      "filterArray": [
        {
          "title": "30天以内",
          "selectedTitle": "30天以内",
          "matchingId": "1",
          "isSelected": false
        },
        {
          "title": "1-12月",
          "selectedTitle": "1-12月",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "1-3年",
          "selectedTitle": "1-3年",
          "matchingId": "3",
          "isSelected": false
        },
        {
          "title": "3-5年",
          "selectedTitle": "3-5年",
          "matchingId": "4",
          "isSelected": false
        },
        {
          "title": "5-10年",
          "selectedTitle": "5-10年",
          "matchingId": "5",
          "isSelected": false
        },
        {
          "title": "10年以上",
          "selectedTitle": "10年以上",
          "matchingId": "6",
          "isSelected": false
        }
      ]
    }
  ]
  return array;
}

// 个人中心数据
function info_data() {
  var array = [
    {
      title: "会员中心",
      image: "/img/info/hat.png",
      hint: "",
      bottomLine: true,
      topLine: true,
      rightArrow: true,
      identify: "0",
    }, {
      title: "我的订单",
      image: "/img/info/order.png",
      hint: "",
      bottomLine: true,
      topLine: true,
      rightArrow: true,
      identify: "2",
    },
    {
      title: "用户授权",
      image: "/img/info/auth.png",
      hint: "",
      bottomLine: true,
      topLine: false,
      rightArrow: true,
      identify: "6",
    },
    {
      title: "推广赚佣",
      image: "/img/info/getm.png",
      hint: "",
      bottomLine: false,
      topLine: true,
      rightArrow: true,
      identify: "1",
    },
    // {
    //   title: "实名认证",
    //   image: "/img/info/auth.png",
    //   hint: "",
    //   bottomLine: true,
    //   topLine: false,
    //   rightArrow: true,
    //   identify: "3",
    // }, 
    {
      title: "推广代码",
      image: "/img/info/brother.png",
      hint: "",
      bottomLine: false,
      topLine: false,
      rightArrow: true,
      identify: "4",
    }, {
      title: "设置",
      image: "/img/info/set.png",
      hint: "",
      bottomLine: false,
      topLine: true,
      rightArrow: true,
      identify: "5",
    },
  ]
  return array;
}

// vip特权
function vip_items() {
  var total_array = [
    // 第一列
    {
      title: "会员特权",
      array: [
        // 第一行
        [{
          image: "https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_browse_privilege.png",
          title: "浏览特权",
          identify: "100",
        },
        {
          image: "https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_activity_privilege.png",
          title: "活动特权",
          identify: "101",
        }, {
          image: "https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_vip_privilege.png",
          title: "尊贵标识",
          identify: "102",
        }],
        [{
          image: "https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_manage_privilege.png",
          title: "管理特权",
          identify: "103",
        },
        {
          image: "https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_product_privilege.png",
          title: "产品特权",
          identify: "104",
        }, {
          image: "https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_recruit_privilege.png",
          title: "招聘特权",
          identify: "105",
        }],
      ],
    },
  ];
  return total_array;
}

// vip购买列表
function vip_buy_items() {
  return [
    // {
    //   'title': '连续包月',
    //   'detail': '到期自动续费1个月, 可随时取消',
    //   'price': '27',
    //   'old_price': '',
    //   'identify': '0',
    // },
    {
      'title': '12个月',
      'detail': '',
      'price': '298',
      'old_price': '360',
      'identify': '1',
    },
    {
      'title': '6个月',
      'detail': '',
      'price': '150',
      'old_price': '180',
      'identify': '2',
    },
    {
      'title': '1个月',
      'detail': '',
      'price': '10',
      'old_price': '30',
      'identify': '4',
    },
  ]
}

// 临时产品数据
function temp_pro_array() {
  var pro_itemDataArray = [ 
    {
      "pro_id": 16,
      "pro_name": "工行装修贷模板",
      "commission": 1,
      "commission_type": 0,
      "max_amount": 99,
      "open_days": 7,
      "mon_interest": 0.1,
      "limit_months": 3,
      "origin": "牛管家金融"
    },
    {
      "pro_id": 15,
      "pro_name": "徽商贷款",
      "commission": 5,
      "commission_type": 0,
      "max_amount": 5,
      "open_days": 3,
      "mon_interest": 5,
      "limit_months": 3,
      "origin": "牛管家金融"
    },
    {
      "pro_id": 12,
      "pro_name": "徽商银行贷",
      "commission": 2,
      "commission_type": 0,
      "max_amount": 50,
      "open_days": 0,
      "mon_interest": 3,
      "limit_months": 120,
      "origin": "牛管家金融"
    },
    {
      "pro_id": 11,
      "pro_name": "prodcut_04",
      "commission": 0.03,
      "commission_type": 0,
      "max_amount": 50,
      "open_days": 0,
      "mon_interest": 0.02,
      "limit_months": 36,
      "origin": "推客网络"
    },
    {
      "pro_id": 3,
      "pro_name": "合肥热门装修贷",
      "commission": 0.8,
      "commission_type": 0,
      "max_amount": 300,
      "open_days": 12,
      "mon_interest": 0.7,
      "limit_months": 36,
      "origin": "123"
    },
  ];
  return pro_itemDataArray;
}
// 招聘发布
function recruit() {
  return [
    {
      item_title: '职位名称',
      item_input: '请选择',
      right_image: true,
      margin_top: 20,
      margin_bottom: 0,
      identify: 'title',
      matchingId: "0",
      tag_type: 'picker',
    },
    {
      item_title: '工作年限',
      item_input: '请选择',
      right_image: true,
      margin_top: 20,
      margin_bottom: 0,
      identify: 'years',
      matchingId: "0",
      tag_type: 'picker',
    },
    {
      item_title: '工作地点',
      item_input: '请选择',
      right_image: true,
      margin_top: 1,
      margin_bottom: 0,
      identify: 'location',
      matchingId: "0",
      tag_type: 'view',
    },
    {
      item_title: '详细地址',
      item_input: '请输入详细地址',
      right_image: true,
      margin_top: 1,
      margin_bottom: 0,
      identify: 'detail_address',
      matchingId: "0",
      tag_type: 'detail',
    },
    {
      item_title: '学历要求',
      item_input: '请选择',
      right_image: true,
      margin_top: 20,
      margin_bottom: 0,
      identify: 'education',
      matchingId: "0",
      tag_type: 'picker',
    },
    { 
      item_title: '工作性质',
      item_input: '请选择',
      right_image: true,
      margin_top: 1,
      margin_bottom: 0,
      identify: 'category',
      matchingId: "0",
      tag_type: 'picker',
    },
    {
      item_title: '薪资范围',
      item_input: '请选择',
      right_image: true,
      margin_top: 1,
      margin_bottom: 0,
      identify: 'salary',
      matchingId: "0",
      tag_type: 'picker',
    }
  ]
}

function recruit_subList(item) {
  var sub_array = []
  if (!item) {
    return sub_array
  }
  if (item.identify == 'title') {
    sub_array = {
      title: '职位名称',
      array: [
        {
          title: '营销经理',
          matchingId: "0",
        },
        {
          title: '团队主任',
          matchingId: "1",
        },
        {
          title: '渠道经理',
          matchingId: "2",
        },
        {
          title: '渠道主管',
          matchingId: "3",
        },
        {
          title: '客服经理',
          matchingId: "4",
        },
        {
          title: '客服主管',
          matchingId: "5",
        },
        {
          title: '产品经理',
          matchingId: "6",
        },
        {
          title: '贷后经理',
          matchingId: "7",
        },
        {
          title: '风控专员',
          matchingId: "8",
        },
        {
          title: '风控主管',
          matchingId: "9",
        },
        {
          title: '其它',
          matchingId: "10",
        },
      ]
    }
  } else if (item.identify == 'years') {
    sub_array = {
      title: '工作年限',
      array: [
        {
          title: '经验不限',
          matchingId: "0",
        },
        {
          title: '一年以下',
          matchingId: "1",
        },
        {
          title: '1-3年',
          matchingId: "2",
        },
        {
          title: '3-5年',
          matchingId: "3",
        },
        {
          title: '5-10年',
          matchingId: "4",
        },
        {
          title: '10年以上',
          matchingId: "5",
        },
      ]
    }
  } else if (item.identify == 'education') {
    sub_array = {
      title: '学历要求',
      array: [
        {
          title: '学历不限',
          matchingId: "0",
        },
        {
          title: '本科',
          matchingId: "1",
        },
        {
          title: '硕士',
          matchingId: "2",
        },
        {
          title: '大专',
          matchingId: "3",
        },
        {
          title: '中专',
          matchingId: "4",
        },
        {
          title: '高中',
          matchingId: "5",
        },
      ]
    }
  } else if (item.identify == 'category') {
    sub_array = {
      title: '工作性质',
      array: [
        {
          title: '全职',
          matchingId: "0",
        },
        {
          title: '兼职',
          matchingId: "1",
        },
        {
          title: '实习',
          matchingId: "2",
        },
      ]
    }
  } else if (item.identify == 'salary') {
    sub_array = {
      title: '薪资范围',
      array: [
        {
          title: '2k-5k',
          matchingId: "0",
        },
        {
          title: '5k-10k',
          matchingId: "1",
        },
        {
          title: '10k-20k',
          matchingId: "2",
        },
        {
          title: '20k以上',
          matchingId: "2",
        },
      ]
    }
  }
  return sub_array
}

module.exports = {
  // 假产品列表数据
  temp_pro_array: temp_pro_array,

  // 首页数据
  index_items: index_items,
  // 管理页面数据
  mange_items: mange_items,
  // 个人中心数据
  info_data: info_data,
  // 产品搜索筛选 
  pro_filter_array: pro_filter_array,
  // vip特权
  vip_items: vip_items,
  // vip购买价格列表
  vip_buy_items: vip_buy_items,
  //招聘
  recruit: recruit,
  recruit_subList: recruit_subList,

}