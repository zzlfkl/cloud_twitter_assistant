var date_util = require('date_util.js');

// 贷款类
function Loan(amount, year, interest, discount, start_date_string, pay_way) {
  this.amount = amount
  this.interest = interest
  this.year = year
  this.start_date_string = start_date_string
  this.discount = discount
  this.pay_way = pay_way  
  var temp_date = date_util.convertDateFromString(this.start_date_string)
  if ((temp_date instanceof Date) && temp_date.getMonth()) {
    this.start_date = date_util.convertDateFromString(this.start_date_string)
  }  

  // 更换日期
  this.changeStartDate = changeStartDate
  function changeStartDate(start_date_string) {
    this.start_date_string = start_date_string
    var temp_date = date_util.convertDateFromString(start_date_string)
    if ((temp_date instanceof Date) && temp_date.getMonth() + 1) {
      this.start_date = date_util.convertDateFromString(this.start_date_string)
    }  
  }

  // 验证是否可以计算
  this.verify = verify
  function verify() { 
    if (this.amount == undefined || isNaN(this.amount) || this.amount.length == 0) {
      console.log("贷款总额为空或者不是数字")
      return false
    }
    if (this.year == undefined || isNaN(this.year) || this.year.length == 0) {
      console.log("贷款年限为空或者不是数字")
      return false
    }
    if (this.interest == undefined || isNaN(this.interest) || this.interest.length == 0) {
      console.log("贷款利率为空或者不是数字")
      return false
    }
    if (this.discount == undefined || isNaN(this.discount) || this.discount.length == 0) {
      console.log("折扣为空或者不是数字")
      return false
    }
    if (this.start_date_string == undefined || this.start_date_string.length == 0) {
      console.log("日期字符串为空")
      return false
    } 

    // if (this.start_date == undefined || !(this.start_date instanceof Date)) {
    //   console.log("还款日期为空或者日期格式不对")
    //   return false
    // }
 
    return true
  }
  
  // 统一计算
  this.calculation = calculation
  function calculation() {
    if (this.pay_way == 0) {
      return this.equal_principal_interest()
    } else if (this.pay_way == 1) {
      return this.equal_amount_interest()
    } else if (this.pay_way == 2) {
      return this.equal_amount_principal()
    } 
  }

  /**
   * 等本等息
  */
  this.equal_principal_interest = equal_principal_interest
  function equal_principal_interest() {
    // 贷款总额
    var exact_money = this.amount * 10000
    // 最终利息
    var last_interest = this.interest * this.discount / 100
    // 月利率
    var mon_interest = last_interest / 12
    // 总共月数量
    var month = this.year * 12
    // 总利息
    var total_pay_interest = exact_money * mon_interest * month
    // 总还款
    var total_pay = exact_money + total_pay_interest
    // 月供
    var average_month_pay = exact_money * mon_interest + exact_money / month
    // 月付利息
    var month_pay_interest = average_month_pay - exact_money / month;
    var detail_array = new Array()
    for (var i = 0; i < month; i++) { 
      var item = {
       
        periods: "第[" + (i + 1) + "]期",
       
        repay: (exact_money / month).toFixed(2),
        interest: month_pay_interest.toFixed(2),
        left_money: (exact_money - (exact_money / month * (i + 1))).toFixed(2)
      }
      detail_array.push(item) 
    } 
    var loan_cal = new Loan_Calculate((last_interest * 100).toFixed(2), total_pay_interest.toFixed(2), total_pay.toFixed(2), average_month_pay.toFixed(2), detail_array)
    return loan_cal  
  }

  /**
   * 等额本息
  */
  this.equal_amount_interest = equal_amount_interest
  function equal_amount_interest() {
    // 贷款总额
    var exact_money = this.amount * 10000
    // 最终利息
    var last_interest = this.interest * this.discount / 100
    // 月利率
    var mon_interest = last_interest / 12
    // 总共月数量
    var month = this.year * 12
    // 总共支付

    // 平均月供
    var average_month_pay = exact_money * mon_interest * Math.pow((1 + mon_interest), month) / (Math.pow(1 + mon_interest, month) - 1)

    // var month_principal = exact_money / month
    // 总还款
    var total_pay = 0  
    var detail_array = new Array()
    var total_pay_principal = 0;
    for (var i = 0; i < month; i++) {
      // 月还本金
      var month_principal = exact_money * mon_interest * Math.pow(1 + mon_interest, i)/(Math.pow(1 + mon_interest, month) - 1);
      total_pay += average_month_pay
      total_pay_principal += month_principal;
      var item = {
        // periods: "第[" + (i + 1) + "]期" + date_util.convertStringFromDate(date_util.addMonth(this.start_date, i), "yyyy-MM"),
        periods: "第[" + (i + 1) + "]期",
        // repay: average_month_pay.toFixed(2),
        repay: month_principal.toFixed(2),
        interest: (average_month_pay - month_principal).toFixed(2),
        left_money: (exact_money - total_pay_principal).toFixed(2)
      }
      detail_array.push(item) 
      // console.log("第" + (i + 1) + "期 " + date_util.convertStringFromDate(date_util.addMonth(this.start_date, i), "yyyy-MM") + " 月付" + average_month_pay.toFixed(2) + "利息" + (average_month_pay - month_principal))
    }
    // 总利息
    var total_pay_interest = total_pay - exact_money
    var loan_cal = new Loan_Calculate((last_interest * 100).toFixed(2), total_pay_interest.toFixed(2), total_pay.toFixed(2), average_month_pay.toFixed(2), detail_array)
    return loan_cal   
  }

  /**
  * 等额本金
  */
  this.equal_amount_principal = equal_amount_principal
  function equal_amount_principal() {
    // 贷款总额
    var exact_money = this.amount * 10000
    // 最终利息
    var last_interest = this.interest * this.discount / 100
    // 月利率
    var mon_interest = last_interest / 12
    // 总共月数量
    var month = this.year * 12
    // 总利息
    //〔(总贷款额÷还款月数+总贷款额×月利率)+总贷款额÷还款月数×(1+月利率)〕÷2×还款月数-总贷款额
    // (总贷款额÷还款月数+总贷款额×月利率)
    var temp1 = exact_money / month + exact_money * mon_interest
    // 总贷款额÷还款月数×(1+月利率)
    var temp2 = exact_money / month * (1 + mon_interest)
    var total_pay_interest = (temp1 + temp2) / 2 * month - exact_money
    // 总还款
    var total_pay = total_pay_interest + exact_money
    // 月还本金
    var month_principal = exact_money / month; 
    var detail_array = new Array()
    for (var i = 0; i < month; i++) {
      var month_pay = (exact_money / month) + (exact_money - month_principal * i) * mon_interest;
      var item = {
        // periods: "第[" + (i + 1) + "]期" + date_util.convertStringFromDate(date_util.addMonth(this.start_date, i), "yyyy-MM"),
        periods: "第[" + (i + 1) + "]期",
        // repay: month_pay.toFixed(2),
        repay: month_principal.toFixed(2),
        interest: (month_pay - month_principal).toFixed(2),
        left_money: (exact_money - month_principal * (i + 1)).toFixed(2)
      }
      detail_array.push(item) 
      // console.log("第" + (i + 1) + "期 " + date_util.convertStringFromDate(date_util.addMonth(this.start_date, i), "yyyy-MM") + "月付" + month_pay.toFixed(2))
    }
    var loan_cal = new Loan_Calculate((last_interest * 100).toFixed(2), total_pay_interest.toFixed(2), total_pay.toFixed(2), (total_pay/month).toFixed(2), detail_array)
    return loan_cal    
  }
}

function Loan_Calculate(last_interest, total_pay_interest, total_pay, average_month_pay, detail_array, left_) {
  this.last_interest = last_interest
  this.total_pay_interest = total_pay_interest
  this.total_pay = total_pay
  this.average_month_pay = average_month_pay 
  this.detail_array = detail_array
}

module.exports = {
  Loan: Loan,
}
