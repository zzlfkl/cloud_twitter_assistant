var util = require('utils/util.js');
var config = require('utils/config.js')
var updateManager = wx.getUpdateManager();
App({
  //全局初始变量
  data: {
    //本地测试地址 
    // url: "http://192.168.2.210:81/api/",

    //线上版本地址
    url: "https://wx.ytk18.com/api/",
    version: 225
  },

  globalData: {
    sysWidth: wx.getSystemInfoSync().windowWidth,
    sysHeight: wx.getSystemInfoSync().windowHeight,
  },

  /**
   * 小程序启动
   */
  onLaunch: function(option) {
    wx.getSystemInfo({
      success: res => {
        //导航高度
        this.globalData.navHeight = res.statusBarHeight + 46;
        if (res.system.indexOf('iOS') !== -1) {
          this.globalData.userSys = 'iOS';
        } else if (res.system.indexOf('Android') !== -1) {
          this.globalData.userSys = 'Android';
        }
      },
      fail(err) {
        console.log(err);
      }
    })

    // 工具类
    this.config = config;
    this.util = util;
    wx.getSystemInfo({
      success: function(res) {
        if (res.version <= '6.5.7') {
          wx.showModal({
            title: '微信版本过低',
            content: '当前微信版本过低，将无法正常使用本小程序',
            showCancel: false,
            success: function(res) {
              if (res.confirm) {
                wx.navigateBack({
                  delta: 2
                })
              }
            }
          })
          setTimeout(function() {
            wx.navigateBack();
          }, 1500)
        }
      },
    })
    //版本更新
    if (wx.getUpdateManager) {
      updateManager.onCheckForUpdate(function(res) {
        // 请求完新版本信息的回调
        updateManager.onUpdateReady(function() {
          wx.showModal({
            title: '更新提示',
            content: '新版本已经准备好，是否重启应用？',
            success: function(res) {
              if (res.confirm) {
                // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                updateManager.applyUpdate()
              }
            },
            fail: function(e) {

            }
          })
        })
        updateManager.onUpdateFailed(function() {
          wx.showToast({
            title: '更新失败，请稍后重试',
            icon: 'none'
          })
          setTimeout(function() {
            wx.navigateBack({
              delta: 1
            })
          }, 1500)
          // 新的版本下载失败
        })
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
      return;
    }
  },
  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function(option) {

  }
})