// pages/client/grap/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    winHeight: "", // 窗口高度
    currentTab: 0, // 预设当前项的值
    scrollLeft: 0, // tab标题的滚动条位置
    tabs: [{
      title: '待处理',
      currentTab: 0,
      data: []
    }, {
      title: '已处理',
      currentTab: 1,
      data: []
    }]
  },

  // 客户详情
  clientDetail: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/client/detail/detail?id=' + id,
    })
  },

  // 滚动切换标签样式
  switchTab: function(e) {
    this.setData({
      currentTab: e.detail.current
    });
    if (!this.data.tabs[e.detail.current].data.length) {
      this.getClientList(e.detail.current, 1)
    }
    this.checkCor();
  },

  // 获取数据
  getClientList: function(kind, page) {
    var this_ = this;
    var param = {
      'type': kind,
      'page': page
    };
    app.util.api('client/client_buy_list', 'GET', param, function(data) {
      if (data.data.length) {
        for (var i = 0; i < this_.data.tabs.length; i++) {
          if (this_.data.currentTab == i) {
            this_.data.tabs[i].data = this_.data.tabs[i].data.concat(data.data)
          }
        }
        this_.setData({
          tabs: this_.data.tabs
        })
      }
    })
  },

  // 点击标题切换当前页时改变样式
  swichNav: function(e) {
    var cur = e.target.dataset.current;
    if (this.data.currentTaB == cur) {
      return false;
    } else {
      this.setData({
        currentTab: cur
      })
    }
  },

  // 判断当前滚动超过一屏时，设置tab标题滚动条。
  checkCor: function() {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollLeft: 300
      })
    } else {
      this.setData({
        scrollLeft: 0
      })
    }
  },

  // 计算内容高度
  calContentHeight: function() {
    var that = this;
    //  高度自适应
    wx.getSystemInfo({
      success: function(res) {
        var clientHeight = res.windowHeight,
          clientWidth = res.windowWidth,
          rpxR = 750 / clientWidth;
        var calc = clientHeight * rpxR - 80;
        that.setData({
          winHeight: calc
        })
      }
    })
  },

  // 到达顶部
  upper: function() {
    this.onPullDownRefresh();
  },

  // 到达顶部
  lower: function() {
    this.setData({
      page: this.data.page + 1
    })
    this.getClientList(this.data.currentTab, this.data.page);
  },

  /**
   * 修改处理状态
   */
  chageOperation:function(e){
    var this_ = this;
    var status = e.currentTarget.dataset.status;
    var client_id = e.currentTarget.dataset.id;
    var title = '处理状态'
    if(status){
      var content = '确认标记为未处理吗？';
    }else{
      var content = '确认标记为已处理吗？';
    }
    wx.showModal({
      title: title,
      content: content,
      success:function(res){
        if(res.confirm){
          this_.operation(status, client_id)
        }
      }
    })
  },

  operation: function (status, client_id){
    var this_ = this;
    var param = {
      status: status,
      client_id: client_id
    }
    app.util.api("client/change_operation", 'POST', param, function (data) {
      if (data.success) {
        this_.onPullDownRefresh();
      }
    });
  },

  // 拨打电话
  makePhoneCall:function(e){
    var mobile = e.currentTarget.dataset.phone;
    wx.getSystemInfo({
      success: function (res) {
        if (res.system.indexOf('iOS') !== -1) {
          wx.makePhoneCall({
            phoneNumber: mobile.toString(),
          })
        } else if (res.system.indexOf('Android') !== -1) {
          wx.showModal({
            title: '拨打电话',
            content: '确定要拨打'+mobile+'？',
            success:function(e){
              if(e.confirm){
                wx.makePhoneCall({
                  phoneNumber: mobile.toString(),
                })
              }
            }
          })
        }
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.calContentHeight();
    this.getClientList(this.data.currentTab, this.data.page);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.data.tabs[this.data.currentTab].data = [];
    this.setData({
      page: 1,
      tabs: this.data.tabs
    })
    this.getClientList(this.data.currentTab, this.data.page);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function() {

  // }
})