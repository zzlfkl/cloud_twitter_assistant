// pages/client/index/index.js
var app = getApp();
Page({

  /**
 * 页面的初始数据
 */
  data: {
    itemDataArray: [
      //   {
      //   title: "我要甩单",
      //   image: "/img/client/index/client_share.png",
      //   hint: "",
      //   bottomLine: true,
      //   topLine: true,
      //   rightArrow: true,
      //   identify: "0",
      //   show: true
      // }, 
      {
        title: "甩单管理",
        image: "/img/client/index/client_share.png",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "1",
        show: true
      }, {
        title: "抢单管理",
        image: "/img/client/index/clitent_grap_manage.png",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "2",
        show: true
      },
      {
        title: "客户市场",
        image: "/img/client/index/clitent_market.png",
        hint: "",
        bottomLine: false,
        topLine: false,
        rightArrow: true,
        identify: "3",
        show: true
      }
    ],
  },

  // 分类点击
  itemTap: function (e) {
    var identify = e.currentTarget.dataset.identify
    if (identify == 0) { // 甩单
      wx.navigateTo({
        url: '/pages/client/release/index',
      })
    } else if (identify == 1) { // 甩单管理
      wx.navigateTo({
        url: '/pages/client/share/index',
      })
    } else if (identify == 2) { // 抢单管理
      wx.navigateTo({
        url: '/pages/client/grap/index',
      })
    } else if (identify == 3) { // 客户列表
      wx.navigateTo({
        url: "/pages/client/list/index?des=客户列表",
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})