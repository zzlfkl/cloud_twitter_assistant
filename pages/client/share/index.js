// pages/client/share/index.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    winHeight: "", // 窗口高度
    currentTab: 0, // 预设当前项的值
    scrollLeft: 0, // tab标题的滚动条位置
    tabs: [{
      title: '已甩单',
      currentTab: 0,
      data: []
    }, {
      title: '待甩单',
      currentTab: 1,
      data: []
    }]
  },

  clientDown: function(e) {
    var client_id = e.currentTarget.dataset.id;
    var param = {
      client_id: client_id
    };
    var this_ = this;
    app.util.api("client/client_down", 'POST', param, function(data) {
      if (data.success) {
        wx.showToast({
          title: data.msg,
          duration: 1200
        });
        setTimeout(function() {
          this_.onPullDownRefresh();
        }, 1200)
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    });
  },

  clientOn: function(e) {
    var client_id = e.currentTarget.dataset.id;
    var param = {
      client_id: client_id
    };
    var this_ = this;
    app.util.api("client/client_on", 'POST', param, function(data) {
      if (data.success) {
        wx.showToast({
          title: data.msg,
          duration: 1200
        });
        setTimeout(function() {
          this_.onPullDownRefresh();
        }, 1200)
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    });
  },

  clientDel: function(e) {
    var this_ = this;
    var client_id = e.currentTarget.dataset.id;
    var param = {
      client_id: client_id
    };
    wx.showModal({
      title: '谨慎操作',
      content: '确认要删除该客户吗？',
      success: function(res) {
        if (res.confirm) {
          app.util.api("client/client_del", 'POST', param, function(data) {
            if (data.success) {
              wx.showToast({
                title: data.msg,
                duration: 1200
              });
              setTimeout(function() {
                this_.onPullDownRefresh();
              }, 1200)
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          });
        }
      }
    })
  },

  /**
   * 获取管理列表
   * @param kind 管理类型
   * @param page 页数
   */
  getManageList: function(kind, page) {
    var this_ = this;
    var param = {
      'type': kind,
      'page': page
    };
    app.util.api("client/manage_list", 'GET', param, function(data) {
      console.log(data);
      if (data.data.length) {
        for (var i = 0; i < this_.data.tabs.length; i++) {
          if (this_.data.currentTab == i) {
            this_.data.tabs[i].data = this_.data.tabs[i].data.concat(data.data)
          }
        }
        this_.setData({
          tabs: this_.data.tabs
        })
      }
    })
  },

  // 滚动切换标签样式
  switchTab: function(e) {
    this.setData({
      currentTab: e.detail.current
    });
    this.checkCor();
    if (!this.data.tabs[e.detail.current].data.length) {
      this.getManageList(!e.detail.current, 1)
    }
  },

  // 点击标题切换当前页时改变样式
  swichNav: function(e) {
    var cur = e.target.dataset.current;
    if (this.data.currentTaB == cur) {
      return false;
    } else {
      this.setData({
        currentTab: cur
      })
    }
  },

  // 判断当前滚动超过一屏时，设置tab标题滚动条。
  checkCor: function() {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollLeft: 300
      })
    } else {
      this.setData({
        scrollLeft: 0
      })
    }
  },

  // 计算内容高度
  calContentHeight: function() {
    var that = this;
    //  高度自适应
    wx.getSystemInfo({
      success: function(res) {
        var clientHeight = res.windowHeight,
          clientWidth = res.windowWidth,
          rpxR = 750 / clientWidth;
        var calc = clientHeight * rpxR - 80;
        console.log(calc)
        that.setData({
          winHeight: calc
        })
      }
    })
  },

  // 到达顶部
  upper: function() {
    this.onPullDownRefresh();
  },

  // 到达顶部
  lower: function() {
    this.setData({
      page: this.data.page + 1
    })
    this.getManageList(!this.data.currentTab, this.data.page)
  },

  // 点击某一列
  clientTap: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url:'/pages/client/detail/detail?id='+id 
      // url: '/pages/client/release/index?id=' + id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.calContentHeight();
    this.getManageList(!this.data.currentTab, this.data.page);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.data.tabs[this.data.currentTab].data = [];
    this.setData({
      page: 1,
      tabs: this.data.tabs
    })
    this.getManageList(!this.data.currentTab, this.data.page);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {},

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function() {

  // }
})