// pages/client/release/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sub_status: true,
    array: [
      // 基本信息
      {
        title: '基本信息',
        list: [{
          need: true,
          needInput: true,
          title: '真实姓名：',
          placeholder: '请输入客户真实姓名',
          input_type: 'text',
          name: 'real_name',
          choose_array: [{
            tag_image_path: '/img/customer/choose_seleted.png',
            tag_title: '男',
            tag_identify: 'gender',
            selected: true,
            tag_number: 0,
            value: 1
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '女',
            tag_identify: 'gender',
            selected: false,
            tag_number: 1,
            value: 2
          }],
        }, {
          need: true,
          needInput: true,
          title: '手机号码：',
          name: 'mobile',
          input_type: 'number',
          placeholder: '请输入相关手机号码',
        }, {
          need: true,
          needInput: true,
          title: '需求金额：',
          input_type: 'number',
          name: 'demand_amount',
          placeholder: '请输入客户需求金额',
          unit: '万'
        }, {
          need: true,
          needBtn: true,
          title: '贷款时限：',
          picker_array: ['1个月', '2个月', '3个月', '半年', '1年', '2年', '3年', '4年', '5年', '10年', '20年', '30年'],
          mode: 'selector',
          name: 'expected_years',
          placeholder: '请选择'
        }, {
          need: true,
          needBtn: true,
          picker_array: [],
          mode: 'date',
          title: '出生年月：',
          name: 'birthday',
          start: '1953-01-01',
          end: '2000-01-01',
          placeholder: '请选择'
        }, {
          need: true,
          needSelect: true,
          title: '贷款位置：',
          picker_array: [],
          mode: 'region',
          name: 'city_location',
          placeholder: '请选择'
        }, {
          need: true,
          needSelect: true,
          title: '户籍所在：',
          picker_array: [],
          mode: 'region',
          name: 'census_register',
          placeholder: '请选择'
        }, {
          need: true,
          title: '婚姻状况：',
          choose_array: [{
            tag_image_path: '/img/customer/choose_seleted.png',
            tag_title: '未婚',
            tag_identify: 'marital_status',
            selected: true,
            tag_number: 0,
            value: '未婚'
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '已婚',
            tag_identify: 'marital_status',
            selected: false,
            tag_number: 1,
            value: '已婚'
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '离异',
            tag_identify: 'marital_status',
            selected: false,
            tag_number: 2,
            value: '离异'
          }],
        }, {
          need: true,
          title: '学历学位：',
          choose_array: [{
            tag_image_path: '/img/customer/choose_seleted.png',
            tag_title: '高中',
            tag_identify: 'education',
            selected: true,
            tag_number: 0,
            value: '高中'
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '大专',
            tag_identify: 'education',
            selected: false,
            tag_number: 1,
            value: '大专'
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '本科',
            tag_identify: 'education',
            selected: false,
            tag_number: 2,
            value: '本科'
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '硕士及以上',
            tag_identify: 'education',
            selected: false,
            tag_number: 3,
            value: '硕士及以上'
          }],
        }, ]
      },
      // 职业信息
      {
        title: '职业信息',
        list: [{
          need: true,
          needInput: true,
          title: '职业身份：',
          placeholder: '请输入客户职业身份',
          choose_array: [{
            tag_image_path: '/img/customer/choose_seleted.png',
            tag_title: '企业主',
            tag_identify: 'occupation',
            selected: true,
            tag_number: 0,
            value: '企业主'
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '个体户',
            tag_identify: 'occupation',
            selected: false,
            tag_number: 1,
            value: '个体户'
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '公务员',
            value: '公务员',
            tag_identify: 'occupation',
            selected: false,
            tag_number: 2
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '企业员工',
            tag_identify: 'occupation',
            selected: false,
            tag_number: 3,
            value: '企业员工'
          }],
        }, {
          needInput: true,
          subTitle: '公司名称：',
          placeholder: '请输入公司名称',
          name: 'company_name',
          unit: '有限公司'
        }, {
          needInput: true,
          subTitle: '月均收入：',
          placeholder: '请输入月均收入',
          input_type: 'number',
          name: 'monthly_income',
          unit: '元'
        }, {
          need: true,
          title: '社保公积：',
          choose_array: [{
            tag_image_path: '/img/customer/choose_seleted.png',
            tag_title: '公积金',
            tag_identify: 'social_security',
            selected: true,
            tag_number: 0,
            value: 1
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '社保',
            tag_identify: 'social_security',
            selected: false,
            tag_number: 1,
            value: 2
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '无',
            tag_identify: 'social_security',
            selected: false,
            tag_number: 2,
            value: 0
          }],
        }, {
          hidden: false,
          name: 'deposit_base',
          tag_identify: 'social_security',
          needInput: true,
          subTitle: '缴存基数：',
          placeholder: '请输入缴存基数',
          input_type: 'number',
          unit: '元'
        }, {
          hidden: false,
          name: 'deposit_monthly',
          tag_identify: 'social_security',
          needInput: true,
          subTitle: '月总缴存：',
          input_type: 'number',
          placeholder: '请输入月总缴存',
          unit: '元'
        }, ]
      },
      // 资产信息
      {
        title: '资产信息',
        list: [{
            need: true,
            title: '有无房产：',
            choose_array: [{
              tag_image_path: '/img/customer/choose_seleted.png',
              tag_title: '按揭房',
              tag_identify: 'house',
              selected: true,
              tag_number: 0,
              value: 1
            }, {
              tag_image_path: '/img/customer/setup_unselected.png',
              tag_title: '全款房',
              tag_identify: 'house',
              selected: false,
              tag_number: 1,
              value: 2
            }, {
              tag_image_path: '/img/customer/setup_unselected.png',
              tag_title: '无',
              tag_identify: 'house',
              selected: false,
              tag_number: 2,
              value: 0
            }],
          }, {
            id: 0,
            tag_identify: 'house',
            name: 'house_value',
            needInput: true,
            subTitle: '房屋现值：',
            input_type: 'number',
            placeholder: '请输入房屋现值',
            unit: '万'
          }, {
            id: 1,
            tag_identify: 'house',
            needInput: true,
            name: 'monthly_supply',

            subTitle: '月供金额：',
            input_type: 'number',
            placeholder: '请输入月供金额',
            unit: '元'
          }, {
            id: 2,
            tag_identify: 'house',
            needInput: true,
            name: 'already_repaid',
            input_type: 'number',
            subTitle: '已还期数：',
            placeholder: '请输入已还期数',
            unit: '期'
          }, {
            title: '有无车产：',
            need: true,
            choose_array: [{
              tag_image_path: '/img/customer/choose_seleted.png',
              tag_title: '有',
              tag_identify: 'car',
              selected: true,
              tag_number: 0,
              value: 1
            }, {
              tag_image_path: '/img/customer/setup_unselected.png',
              tag_title: '无',
              tag_identify: 'car',
              selected: false,
              tag_number: 1,
              value: 0
            }],
          }, {
            tag_identify: 'car',
            needInput: true,
            name: 'car_value',
            subTitle: '车辆现值：',
            input_type: 'number',
            placeholder: '请输入车辆价值',
            unit: '万'
          }, {
            tag_identify: 'car',
            subTitle: '抵押状态：',
            choose_array: [{
              tag_image_path: '/img/customer/choose_seleted.png',
              tag_title: '无抵押',
              tag_identify: 'mortgage_status',
              selected: true,
              tag_number: 0,
              value: 0
            }, {
              tag_image_path: '/img/customer/setup_unselected.png',
              tag_title: '已抵押',
              tag_identify: 'mortgage_status',
              selected: false,
              tag_number: 1,
              value: 1
            }],
          },
          {
            need: true,
            title: '信用卡：',
            choose_array: [{
              tag_image_path: '/img/customer/choose_seleted.png',
              tag_title: '有',
              tag_identify: 'credit_card',
              selected: true,
              tag_number: 0,
              value: 1
            }, {
              tag_image_path: '/img/customer/setup_unselected.png',
              tag_title: '无',
              tag_identify: 'credit_card',
              selected: false,
              tag_number: 1,
              value: 0
            }],
          },
          {
            tag_identify: 'credit_card',
            name: 'credit_total',
            needInput: true,
            subTitle: '授信总额：',
            input_type: 'number',
            placeholder: '请输入授信总额',
            unit: '万'
          }, {
            tag_identify: 'credit_card',
            needInput: true,
            subTitle: '使用总额：',
            input_type: 'number',
            name: 'used_total',
            placeholder: '请输入使用总额',
            unit: '万'
          }, {
            title: '微粒贷：',
            need: true,
            choose_array: [{
              tag_image_path: '/img/customer/choose_seleted.png',
              tag_title: '有',
              tag_identify: 'particulate_loan',
              selected: true,
              tag_number: 0,
              value: 1
            }, {
              tag_image_path: '/img/customer/setup_unselected.png',
              tag_title: '无',
              tag_identify: 'particulate_loan',
              selected: false,
              tag_number: 1,
              value: 0
            }],
          }, {
            tag_identify: 'particulate_loan',
            name: 'particulate_total',
            needInput: true,
            subTitle: '授信额度：',
            input_type: 'number',
            placeholder: '请输入授信额度',
            unit: '万'
          }, {
            tag_identify: 'particulate_loan',
            name: 'particulate_used',
            needInput: true,
            subTitle: '已用总额：',
            input_type: 'number',
            placeholder: '请输入已用总额',
            unit: '万'
          }, {
            title: '寿险保单：',
            need: true,
            choose_array: [{
              tag_image_path: '/img/customer/choose_seleted.png',
              tag_title: '有',
              tag_identify: 'insurance_policy',
              selected: true,
              tag_number: 0,
              value: 1
            }, {
              tag_image_path: '/img/customer/setup_unselected.png',
              tag_title: '无',
              tag_identify: 'insurance_policy',
              selected: false,
              tag_number: 1,
              value: 0
            }],
          }
        ]
      },
      // 客户描述
      {
        title: '客户描述',
        list: []
      }
    ]
  },

  choose_location: function(e) {
    var kind = e.currentTarget.dataset.kind;
    wx.navigateTo({
      url: '/pages/index/location/city?kind=' + kind,
    })
  },

  bindDateChange: function(e) {
    var dateArr = ['1个月', '2个月', '3个月', '半年', '1年', '2年', '3年', '4年', '5年', '10年', '20年', '30年'];
    var this_ = this;
    var name = e.currentTarget.dataset.name;
    var arr = this.data.array;
    for (var i in arr) {
      var list = arr[i].list;
      for (var j in list) {
        if (list[j].name == name) {
          if (list[j].name == 'expected_years') {
            list[j].value = dateArr[e.detail.value];
          } else {
            list[j].value = e.detail.value;
          }
          this_.setData({
            array: arr,
          })
        }
      }
    }
    this.setData({
      date: e.detail.value
    })
  },

  // 选择某项
  chooseItemTag: function(e) {
    var item = e.currentTarget.dataset.item
    var identify = item.tag_identify
    var tempArray = this.data.array
    var that = this
    for (var i in tempArray) {
      var list = tempArray[i].list
      for (var j in list) {
        // 判断选择项
        var choose_array = list[j].choose_array
        if (undefined != choose_array && choose_array.length > 0) {
          for (var z in choose_array) {
            var model = choose_array[z]
            if (model.tag_identify == identify) {
              if (model.tag_number == item.tag_number) {
                this.setData({
                  [identify]: model.tag_number
                })
                model.selected = true;
                model.tag_image_path = '/img/customer/choose_seleted.png'
              } else {
                model.selected = false;
                model.tag_image_path = '/img/customer/setup_unselected.png'
              }
            }
          }
        }
        //
        // 判断是否隐藏
        var model = list[j]
        // 公积金
        if ('social_security' == model.tag_identify) {
          if (this.data.social_security == 2) {
            model.hidden = true;
          } else {
            model.hidden = false;
          }
        }
        // 房产
        if ('house' == model.tag_identify) {
          if (this.data.house == 1) {
            if (model.id > 0) {
              model.hidden = true;
            } else {
              model.hidden = false;
            }
          } else if (this.data.house == 2) {
            model.hidden = true;
          } else {
            model.hidden = false;
          }
        }
        // 车产
        if ('car' == model.tag_identify) {
          if (this.data.car == 1) {
            model.hidden = true;
          } else {
            model.hidden = false;
          }
        }
        // 信用卡
        if ('credit_card' == model.tag_identify) {
          if (this.data.credit_card == 1) {
            model.hidden = true;
          } else {
            model.hidden = false;
          }
        }
        // 微粒贷
        if ('particulate_loan' == model.tag_identify) {
          if (this.data.particulate_loan == 1) {
            model.hidden = true;
          } else {
            model.hidden = false;
          }
        }
      }
    }
    this.setData({
      array: tempArray
    })

  },

  chooseAction: function(e) {
    console.log(e.currentTarget.dataset)
  },

  // 发布
  publish: function(e) {
    var this_ = this;
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
    
    this.setData({
      sub_status: false
    })

    var param = e.detail.value;
    if (app.data.city_location !== undefined) {
      param.city_code = app.data.city_location.val;
    } else {
      param.city_code = app.data.globalCity.val;
    }
    app.util.api('client/publish', 'POST', param, function(data) {
      console.log(data)
      if (data.success) {
        wx.showToast({
          title: data.msg
        })
        setTimeout(function() {
          wx.navigateBack();
        }, 1000)

      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
        this_.setData({
          sub_status: true
        })
      }

    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var arr = this.data.array;
    arr[0].list[1].val = app.data.userinfo.mobile;
    this.setData({
      array: arr
    })
    if (options.id !== undefined) {

    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (app.data.city_location !== undefined) {
      this.data.array[0].list[5].value = app.data.city_location.name;
      this.setData({
        array: this.data.array
      })
    } else {
      this.data.array[0].list[5].value = app.data.globalCity.name;
      this.setData({
        array: this.data.array
      })
    }
    if (app.data.census_register !== undefined) {
      this.data.array[0].list[6].value = app.data.census_register.name;
      this.setData({
        array: this.data.array
      })
    } else {
      this.data.array[0].list[6].value = app.data.globalCity.name;
      this.setData({
        array: this.data.array
      })
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function() {

  // }
})