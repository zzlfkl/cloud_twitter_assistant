// pages/client/grap/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_search: false,
    list: [],
    page: 1,
    winHeight: "", // 窗口高度
    currentTab: 0, // 预设当前项的值
    scrollLeft: 0, // tab标题的滚动条位置
    rob_list: [{
      name: '可抢订单',
      value: '2',
      checked: true
    }],
    occupation: [{
        name: '企业主',
        value: '企业主',
        checked: false
      },
      {
        name: '个体户',
        value: '个体户',
        checked: false
      },
      {
        name: '公务员',
        value: '公务员',
        checked: false
      },
      {
        name: '企业员工',
        value: '企业员工',
        checked: false
      }
    ],
    condition: [{
        name: '房产',
        value: 'house',
        checked: false
      },
      {
        name: '车产',
        value: 'car',
        checked: false
      },
      {
        name: '社保公积',
        value: 'social_security',
        checked: false
      },
      {
        name: '信用卡',
        value: 'credit_card',
        checked: false
      },
      {
        name: '微粒贷',
        value: 'particulate_loan',
        checked: false
      },
      {
        name: '保单',
        value: 'insurance_policy',
        checked: false
      }
    ],
    tabs: [{
      title: '全部',
      currentTab: 0,
      data: []
    }, {
      title: '房产',
      currentTab: 1,
      data: []
    }, {
      title: '车产',
      currentTab: 2,
      data: []
    }, {
      title: '薪金',
      currentTab: 3,
      data: []
    }, {
      title: '经营',
      currentTab: 4,
      data: []
    }, {
      title: '其他',
      currentTab: 5,
      data: []
    }]
  },

  condition: function(e) {
    var condition = this.data.condition;
    var checkArr = e.detail.value;
    console.log(e.detail.value)
    for (var i = 0; i < condition.length; i++) {
      if (checkArr.indexOf(condition[i].value) != -1) {
        condition[i].checked = true;
      } else {
        condition[i].checked = false;
      }
    }
    this.setData({
      condition: condition
    })
  },

  occupation: function(e) {
    var occupation = this.data.occupation;
    var checkArr = e.detail.value;
    for (var i = 0; i < occupation.length; i++) {
      if (checkArr.indexOf(occupation[i].value) != -1) {
        occupation[i].checked = true;
      } else {
        occupation[i].checked = false;
      }
    }
    this.setData({
      occupation: occupation
    })
  },

  is_rob: function() {
    var rob_list = this.data.rob_list;
    rob_list[0].checked = !rob_list[0].checked;
    this.setData({
      rob_list: rob_list
    })
  },

  /**
   * 表单重置
   */
  clear: function(e) {
    var occupation = this.data.occupation;
    var condition = this.data.condition;
    var rob_list = this.data.rob_list;

    for (var i = 0; i < occupation.length; i++) {
      occupation[i].checked = false;
    }
    for (var i = 0; i < condition.length; i++) {
      condition[i].checked = false;
    }
    rob_list[0].checked = false;
    this.setData({
      occupation: occupation,
      condition: condition,
      rob_list: rob_list
    })

  },

  // 表单提交
  formSubmit: function(e) {
    var this_ = this;
    var params = e.detail.value;
    wx.navigateTo({
      url: '/pages/client/search/list?params=' + JSON.stringify(params)
    })
  },

  // 筛选栏出现
  translate: function() {
    var this_ = this;
    this_.setData({
      is_search: true
    });
    setTimeout(function() {
      this_.animation.translate(-(this_.data.clientWidth * 0.8)).step()
      this_.setData({
        animation: this_.animation.export()
      })
    }, 100)
  },

  // 隐藏搜索
  hide_all: function () {
    var this_ = this;
    this_.animation.translate(this_.data.clientWidth * 0.8).step()
    this_.setData({
      animation: this_.animation.export()
    })
    setTimeout(function () {
      this_.setData({
        is_search: false
      });
    }, 200)
  },

  test:function(){

  },

  // 滚动切换标签样式
  switchTab: function(e) {
    // wx.showLoading({
    //   title: '加载中...',
    // })
    this.setData({
      currentTab: e.detail.current
    });
    if (!this.data.tabs[e.detail.current].data.length) {
      this.getClientList(e.detail.current, 1)
    }
    this.checkCor();
  },

  // 点击标题切换当前页时改变样式
  swichNav: function(e) {
    var cur = e.target.dataset.current;
    if (this.data.currentTaB == cur) {
      return false;
    } else {
      this.setData({
        currentTab: cur
      })
    }
  },

  // 判断当前滚动超过一屏时，设置tab标题滚动条。
  checkCor: function() {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollLeft: 300
      })
    } else {
      this.setData({
        scrollLeft: 0
      })
    }
  },

  // 计算内容高度
  calContentHeight: function() {
    var that = this;
    //  高度自适应
    wx.getSystemInfo({
      success: function(res) {
        var clientHeight = res.windowHeight,
          clientWidth = res.windowWidth,
          rpxR = 750 / clientWidth;
        var calc = clientHeight * rpxR - 80;
        that.setData({
          clientWidth: res.windowWidth,
          clientHeight: res.windowHeight,
          winHeight: calc
        })
      }
    })
  },

  // 到达顶部
  upper: function() {
    console.log('saasa')
    this.onPullDownRefresh();
  },

  // 到达顶部
  lower: function() {
    console.log('到达底部');
    this.setData({
      page: this.data.page + 1
    })
    this.getClientList(this.data.currentTab, this.data.page)
  },

  // 客户详情
  clientDetail: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/client/detail/detail?id=' + id,
    })
  },

  // 筛选
  filter: function() {
    console.log('shaixuan')
  },

  // 获取客户列表 
  getClientList: function(kind, page) {
    wx.showLoading({
      title: '加载中...',
    })
    var this_ = this;
    var params = {
      'type': kind,
      'page': page
    }
    app.util.api("client/client_list", 'GET', params, function(data) {
      if (data.data.length) {
        for (var i = 0; i < this_.data.tabs.length; i++) {
          if (this_.data.currentTab == i) {
            this_.data.tabs[i].data = this_.data.tabs[i].data.concat(data.data)
          }
        }
        this_.setData({
          tabs: this_.data.tabs
        })
      }
      wx.hideLoading();
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if(options.des !== undefined){
      wx.setNavigationBarTitle({
        title: options.des,
      })
    }
    this.animation = wx.createAnimation({
      duration: 200,
    });
    this.calContentHeight();
    this.getClientList(this.data.currentTab, this.data.page);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // this.setData({
    //   is_search:false
    // })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.data.tabs[this.data.currentTab].data = [];
    this.setData({
      page: 1,
      tabs: this.data.tabs
    })
    this.getClientList(this.data.currentTab, this.data.page);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function() {
    
  // }
})