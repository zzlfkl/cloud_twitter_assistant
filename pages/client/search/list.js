// pages/client/search/list.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    page:1,
    next:true
  },

  getList:function(params,page){
    var this_ = this;
    params.page = page;
    app.util.api("client/client_search", 'GET', params, function (data) {
      if(data.data.length){
        this_.setData({
          list:this_.data.list.concat(data.data)
        })
      }else{
        this_.setData({
          next:false
        })
      }
    });
  },

  // 客户详情
  clientDetail: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/client/detail/detail?id=' + id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var param = JSON.parse(options.params);
    this.setData({
      param:param
    })
    this.getList(param, this.data.page);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      page:1
    });
    this.getList(this.data.param,this.data.page);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.next){
      this.setData({
        page: this.data.page + 1
      });
      this.getList(this.data.param, this.data.page);
    }
  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // }
})