// pages/user/wallet/change/inc.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cash:100
  },

  //充值金额输入
  incInput:function(e){
    console.log(e.detail.value)
    this.setData({
      cash:e.detail.value
    })
  },

  //充值
  Recharge:function(){
    var that = this;
    if(that.data.cash == ''){
      wx.showToast({
        title: '请输入充值金额',
        icon:'none'
      })
      return;
    }
    
    var params = {
      'cash': that.data.cash,
      'body': '充值',
    };
    app.util.api('account/pay', 'POST', params, function (data) {
      if (data.success == 1) {
        wx.requestPayment({
          'timeStamp': data.data.timeStamp,
          'nonceStr': data.data.nonceStr,
          'package': data.data.package,
          'signType': 'MD5',
          'paySign': data.data.paySign,
          success: function (res) {
            wx.showToast({
              title: '充值成功',
            })
            setTimeout(function(){
              wx.navigateBack();
            },1500);
          },
          fail: function (res) {
            wx.showToast({
              title: '您取消了支付',
              icon: 'none'
            })
          },
        });
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },  


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})