// pages/user/wallet/change/dec.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cashStatus: 0
  },

  // 金额输入
  limitInput: function(e) {
    if (app.data.userinfo.balance == 0 || !e.detail.value || e.detail.value == 0) {
      this.setData({
        cashStatus: 0
      })
    } else if (parseFloat(e.detail.value) > parseFloat(this.data.balance)) {
      this.setData({
        cashStatus: 2
      })
    } else if (e.detail.value && app.data.userinfo.balance !== 0 && parseFloat(e.detail.value) < parseFloat(this.data.balance)) {
      // 手续费计算
      var cash = parseFloat(e.detail.value);
      var tax = Math.ceil((Math.ceil(cash) * this.data.rate) / 10) / 10;
      // 手续费综合大于账户余额
      if (cash + tax > this.data.balance) {
        var cash_val = this.data.balance - tax;
        var taxs = Math.ceil((Math.ceil(cash_val) * this.data.rate) / 10) / 10;
        var cash_value = (this.data.balance - taxs).toFixed(2);
        console.log(taxs);
        this.setData({
          cashStatus: 1,
          cashValue: cash_value,
          tax: taxs
        })
      } else {
        this.setData({
          cashStatus: 1,
          cashValue: e.detail.value,
          tax: tax
        })
      }
    }
  },

  // 全部提现
  cashTotal: function() {
    // 手续费计算
    var cash = parseFloat(this.data.balance);
    var tax = Math.ceil((Math.ceil(cash) * this.data.rate) / 10) / 10;
    // 手续费综合大于账户余额
    var cash_val = this.data.balance - tax;
    var taxs = Math.ceil((Math.ceil(cash_val) * this.data.rate) / 10) / 10;
    var cash_value = (this.data.balance - taxs).toFixed(2);
    console.log(taxs);
    this.setData({
      cashStatus: 1,
      cashValue: cash_value,
      tax: taxs
    })
  },

  // 确认提现
  weiXinCash: function() {
    var this_ = this;
    if (this_.data.cashValue == '' || this_.data.cashValue == undefined) {
      wx.showToast({
        title: '请输入提现金额',
        icon: 'none'
      })
      return;
    }
    var cashToatal = Number(this_.data.cashValue) + Number(this_.data.tax)
    var params = {
      cash: this_.data.cashValue,
      cash_total: cashToatal.toFixed(2)
    };

    this_.setData({
      cashStatus: 0
    })
    app.util.api("wxcash/cash", 'post', params, function(data) {
      wx.showLoading({
        title: '提现中...',
      })
      if (data.success == 1) {
        wx.hideLoading();
        wx.showToast({
          title: data.msg,
          // icon:'none'
        })
        setTimeout(function() {
          wx.navigateBack()
        }, 1500);
      } else {
        this_.setData({
          cashStatus: 1
        })
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  //获取提现配置
  getCashSetting: function() {
    var this_ = this;
    var params = {};
    app.util.api("wxcash/get_setting", "GET", params, function(data) {
      console.log(data)
      this_.setData({
        rate: data.data.rate,
        notice: [
          '1.请确保您的实名认证信息真实有效；',
          '2.每日提现限' + data.data.cashTimesDay + '次，' + data.data.cashLimit + '元内实时到账；',
          '3.提现超过' + data.data.cashLimit + '元需3-5工作日审核；',
          '4.提现费率' + data.data.rate + '%（0.1元起）从账户余额中扣除。'
        ]
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (app.data.userinfo.balance == 0) {
      this.setData({
        cashStatus: 0
      })
    }
    this.getCashSetting();
    this.setData({
      balance: app.data.userinfo.balance
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          height: res.screenWidth * 0.45 + 'px',
          bagHeight: res.windowHeight,
          navH: app.globalData.navHeight
        })
      },
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})