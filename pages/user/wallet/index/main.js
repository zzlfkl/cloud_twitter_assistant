// pages/user/wallet/index/main.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_ios:false,
    itemArray: [{
        name: '推广记录',
        line: true,
        url: '/pages/user/extension/his'
      },
      {
        name: '返佣明细',
        line: true,
        url: '/pages/user/extension/detail'
      },
      {
        name: '账单明细',
        line: true,
        url: '/pages/user/wallet/bill/detail'
      }
    ]
  },

  //充值
  inc: function(e) {
    wx.navigateTo({
      url: '/pages/user/wallet/change/inc',
    })
  },

  //提现
  dec: function(e) {
    if(app.data.userinfo.user_status !== 1){
      wx.showModal({
        title: '温馨提示',
        content: '完成实名认证后可进行提现',
        confirmText:'去认证',
        cancelText:'算了',
        success:function(res){
          if(res.confirm){
            wx.navigateTo({
              url: '/pages/user/personal_authentication/personal_authentication',
            })
          }
        }
      })
    }else{
      wx.navigateTo({
        url: '/pages/user/wallet/change/dec',
      })
    }
  },

  // 列表点击事件
  itemClick: function(e) {
    var url = e.currentTarget.dataset.url;
    if (url == '/pages/user/wallet/change/dec' && app.data.userinfo.user_status !== 1) {
      wx.showModal({
        title: '未实名',
        content: '请先进行实名认证',
        success: function(res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/user/personal_authentication/personal_authentication',
            })
          }
        }
      })
    } else {
      wx.navigateTo({
        url: url,
      })
    }
  },

  // 刷新账户余额
  freshBalance: function(e) {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo.balance = data.data.balance;
        that.setData({
          balance: app.data.userinfo.balance.toFixed(2)
        })

      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (app.globalData.userSys == 'iOS') {
      this.setData({
        is_ios: true
      })
    }
  },  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.freshBalance();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})