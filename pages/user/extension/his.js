// pages/user/self/extension/his.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    loading: true,
    complete: true,
    show: false,
    page: 1,
    mine_list: []
  },
  /**
   * 网络请求
   */
  api: function() {
    var that = this;
    var params = {
      user_id: app.data.userinfo.id,
      page: that.data.page
    };
    app.util.api('userinfo/extension_mine', 'GET', params, function(data) {
      console.log(data)
      if (data.status == 200) {
        if (data.data.length <= 20) {
          that.setData({
            loading: true,
            complete: false,
          })
        } else {
          that.setData({
            loading: false,
            complete: true,
          })
        }
        that.setData({
          mine_list: that.data.mine_list.concat(data.data),
          status: true,
        })
      } else {
        if (that.data.mine_list.length == 0) {
          that.setData({
            status: false
          })
        }
        that.setData({
          loading: true,
          complete: false,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.api();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      mine_list: [],
      page:1
    })
    this.api();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.setData({
      page: this.data.page + 1
    })
    this.api();
  }
})