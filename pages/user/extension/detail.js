// pages/user/self/extension/detail.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    page: 1,
    mine_list: []
  },

  /**
   * 获取返佣总额
   */
  get_coin: function () {
    var that = this;
    var params = {
      'type': 'order_list',
    };
    app.util.api('account/user_coin', 'GET', params, function (data) {
      that.setData({
        total_coin: data.data.total_coin.toFixed(2),
        month_coin: data.data.month_coin.toFixed(2)
      })
    })
  },

  /**
   * 返利明细
   */
  api: function() {
    var that = this;
    var params = {
      user_id: app.data.userinfo.id,
      page: that.data.page
    };
    app.util.api('userinfo/cash_list', 'GET', params, function(data) {
      console.log(data)
      if (data.status == 200) {
        if (data.data.length == 0) {
          that.setData({
            mine_list: [],
            his: false,
          })
        } else {
          that.setData({
            mine_list: that.data.mine_list.concat(data.data),
            his: true,
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.api();
    this.get_coin();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      mine_list: []
    })
    this.api();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.setData({
      page: this.data.page + 1
    })
    this.api();
  }
})