var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    status: true
  },
  /**
   * 用户实名认证
   */
  auth: function() {
    wx.navigateTo({
      url: 'personal_authentication/personal_authentication',
    })
  },
  /**
   * 公司认领
   */
  auth_company: function() {
    wx.navigateTo({
      url: 'auth_company/company',
    })
  },
  /**
   * 绑定微信
   */
  bindGetUserInfo: function() {
    var that = this;
    wx.getUserInfo({
      success: function(info) {
        var rawData = info['rawData'];
        var signature = info['signature'];
        var encryptedData = info['encryptedData'];
        var iv = info['iv'];
        var params = {
          "rawData": rawData,
          "signature": signature,
          'iv': iv,
          'encryptedData': encryptedData,
        };
        app.util.api('login/bind_wx', 'POST', params, function(data) {

          if (data.success == 1) {
            wx.showToast({
              title: data.msg,
            })
            that.loading();
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        })
      }
    })
  },
  /**
   * 绑定手机号
   */
  getPhoneNumber: function(e) {
    var that = this;
    var params = {
      'status': e.detail.errMsg,
      'iv': e.detail.iv,
      'encryptedData': e.detail.encryptedData,
    };
    app.util.api('userinfo/bind_mobile', 'POST', params, function(data) {
     
      if (data.success == 1) {
        // 判断是否为推广人二维码扫描
        if (that.data.extension_id !== undefined) {
          var params = {
            user_id: that.data.extension_id,
          };
          app.util.api('userinfo/qr_post', 'POST', params, function(data) {
            if (data.success == 1) {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
              setTimeout(function() {
                wx.reLaunch({
                  url: '/pages/index/index'
                })
              }, 1500)
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          })
        }

        //  判断是否返回了TOKEN
        if (data.data !== 1) {
          wx.setStorageSync('token', data.data)
        }

        that.loading();
        that.setData({
          phone: true
        })
        wx.showToast({
          title: data.msg,
        })
      } else {
       
        wx.showModal({
          title: '获取号码失败',
          content: '通过验证码绑定手机号',
          confirmText: '去获取',
          success: function(res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/user/userinfo/bindPhone/bind_phone?extension_id=' + that.data.extension_id,
              })
            }
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if (app.data.showStatus) {
      that.setData({
        hideStatus: true
      })
    }
    // 若推荐人ID存在，则设置变量推荐人ID
    if (options.extension_id !== undefined) {
      that.setData({
        extension_id: options.extension_id
      })
    }

    if (app.data.userinfo == undefined) {
      that.loading();
    } else {
      that.setData({
        userinfo: app.data.userinfo
      })
      if (app.data.userinfo.mobile == '') {
        that.setData({
          phone: false
        })
      } else {
        that.setData({
          phone: true
        })
      }
    }
  },
  /**
   * 请求用户信息
   */
  loading: function(e) {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo = data.data;
        that.setData({
          userinfo: data.data,
        })
        if (data.data.mobile == '') {
          that.setData({
            phone: false
          })
        } else {
          that.setData({
            phone: true
          })
        }
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },
})