// pages/info/feedback/send_feedback.js
var app = getApp();
var upliad = require("../../../../utils/upload.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    item_images: ['/img/customer/upload_image.png'],
  },

  // 上传图片
  uploadImage: function () {
    var that = this;
    var params = {
      app:'userAdvice'
    }
    upliad.uploadImage("userinfo/advice_image", params, function (data) {
      if (data.success) {
        that.data.item_images.unshift(data.data);
        that.setData({
          item_images:that.data.item_images
        })
      }
    })
  },
  
  // 修改图片
  modifyImage: function (e) {
    var temp_image = e.currentTarget.dataset.item
    var temp_index = e.currentTarget.dataset.index
    var that = this 
    if (temp_index == this.data.item_images.length - 1) {
      that.uploadImage();
    }else {
      wx.showModal({
        title: '温馨提示',
        content: '确认删除该图片？',
        success: function (res) {
          if (res.confirm) {
            var temp_array = that.data.item_images
            var images = temp_array.splice(temp_index, 1)
            that.setData({
              item_images: temp_array
            })
          } else if (res.cancel) {
            console.log('删除取消')
          }
        }
      })
    }
  },

  addAdvice:function(e){
    // 表单ID搜集
    var that = this;
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) {
      if (data.success == 0) {
        console.log(data)
      }
    })

    //反馈内容检测
    if (e.detail.value.content == '') {
      wx.showToast({
        title: '请输入反馈内容',
        icon:'none'
      })
      return;
    }
    var arr = this.data.item_images.slice(0, this.data.item_images.length-1);
    var params = e.detail.value;
    params.images = arr;
    app.util.api("userinfo/advice_post",'POST',params,function(data){
      if (data.success == 1) {
        wx.showToast({
          title: data.msg,
        });
        setTimeout(function () {
          wx.navigateBack({
            delta: 1
          })
        }, 1500)
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  // 提交反馈
  formsubmit: function (e) {
    var that = this;
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) {
      if (data.success == 0) {
        console.log(data)
      }
    })
    var params = e.detail.value;
    app.util.api('userinfo/user_advice', 'POST', params, function (data) {
      if (data.success == 1) {
        wx.showToast({
          title: data.msg,
        });
        setTimeout(function () {
          wx.navigateBack({
            delta: 1
          })
        }, 1500)
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    });
  },

  //我的反馈
  checkFeedback:function(){
    wx.navigateTo({
      url: 'feedback_list?list=',
    })
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(app.data.userinfo.feedback_status)
    if (app.data.userinfo.feedback_status !== undefined) {
      this.setData({
        read: app.data.userinfo.feedback_status
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})