var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  /**
   *点击事件
   */
  advice: function(e) {
    wx.navigateTo({
      url: 'feedback/feedback',
    })
  },
  // 支付管理 
  restPayPwd: function (e) {
    if(app.data.userinfo.pay_pass == null){
      wx.navigateTo({
        url: '/pages/pay/pwd/index',
      })
    }else{
      wx.navigateTo({
        url: '/pages/user/userinfo/bindPhone/bind_phone?send_type=reset_pay_pass',
      }) 
    }
  },
  // 关于我们
  about: function(e) {
    wx.navigateTo({
      url: 'about',
    })
  },
  // 使用昵称发布
  switchChange: function(e) {
    var that = this;
    if (e.detail.value) {
      that.setData({
        pub_setting: 0
      })
    } else {
      that.setData({
        pub_setting: 1
      })
    }

    // 请求网络开关
    var params = {
      'pub_setting': that.data.pub_setting
    };
    app.util.api('userinfo/use_nickname', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo.pub_setting = that.data.pub_setting;
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (app.data.userinfo.pub_setting == 1) {
      this.setData({
        pub_settings: 0
      })
    } else {
      this.setData({
        pub_settings: 1
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  }
})