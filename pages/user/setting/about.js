// pages/user/about.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  showAddress: function () {
    wx.openLocation({
      latitude: 31.8795300884,
      longitude: 117.2456645966,
      name: '推客网络',
      address: '合肥市庐阳区万科中心16楼'
    })
  },

  callTel: function () {
    wx.makePhoneCall({
      phoneNumber: '0551-65868826',
    })
  },

  callPhone: function () {
    wx.makePhoneCall({
      phoneNumber: '18134532252',
    })
  },

  openUrl: function () {
    var url = 'https://www.ytk18.com'
    wx.navigateTo({
      url: '/pages/webview/index?url=' + url,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})