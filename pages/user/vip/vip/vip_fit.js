// pages/user/vip/vip/vip_fit.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fitList: [{
        title: '浏览特权',
        src: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_fit_extra_read.png',
        style:'width:',
        introduce: '平台内容随心浏览，产品信息随心查看，联系方式随心获取'
      },
      {
        title: '活动特权',
        src: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_fit_extra_act.png',
        introduce: '平台内容随心浏览，产品信息随心查看，联系方式随心获取'
      },
      {
        title: '尊贵标识',
        src: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_fit_extra_vip.png',
        introduce: '平台内容随心浏览，产品信息随心查看，联系方式随心获取'
      },
      {
        title: '产品特权',
        src: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_fit_extra_pro.png',
        introduce: '平台内容随心浏览，产品信息随心查看，联系方式随心获取'
      },
      {
        title: '管理特权',
        src: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_fit_extra_manage.png',
        introduce: '平台内容随心浏览，产品信息随心查看，联系方式随心获取'
      },
      {
        title: '招聘特权',
        src: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_vip_fit_extra_emp.png',
        introduce: '平台内容随心浏览，产品信息随心查看，联系方式随心获取'
      }
    ]
  },


  back:function(){
    wx.navigateBack()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})