//获取应用实例
var md5 = require('../../../../utils/md5.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    price_array: [],
    is_ios:false,
    is_pay:false,
    enoughBalance:true
  },
  
  hiddenPopup: function () {},

  showPopup: function () {},

  // vip权益介绍
  vipDetail:function(){
    wx.navigateTo({
      url: '/pages/user/vip/vip/vip_fit',
    })
  },

  /**
   * 购买某种vip
   */
  pay: function(e) {
    var this_ = this;
    if (!app.util.checkauth(app)) {
      return;
    }
    if (app.data.userinfo.user_type == 4) {
      wx.showModal({
        title: '绑定微信',
        content: '新用户绑定微信即可获赠三日会员',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/user/authlist',
            })
          }
        }
      })
      return;
    }
    var id = parseInt(e.currentTarget.dataset.id);
    var discount = e.currentTarget.dataset.discount.toFixed(2);
    if (discount >= this_.data.balance){
      this_.setData({
        enoughBalance:false
      })
    }else{
      this_.setData({
        enoughBalance: true
      })
    }
    this_.setData({
      cash: discount,
      fee_id:id
    })
    this_.popViewPay.showPopup();
    return;
  },

  //meau的点击事件  
  meau_item_tap: function(e) {
    var value = e.currentTarget.dataset.identify;
  },
  /**
   * 数据加载(收费列表)
   */
  loading: function(e) {
    var that = this;
    var params = {
      'id': 1
    };
    app.util.api('fee/fee_list', 'GET', params, function(data) {
      if (data.success == 1) {
        that.setData({
          list: data.data,
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  // 开始支付
  payActivity: function (e) {
    var this_ = this;
    var pwd = e.detail.pwd;
    wx.showLoading({
      title: '支付中...',
    });
    var params = {
      'body': '开通会员',
      'fee_id': this_.data.fee_id,
      'pay_pass': pwd
    };
    app.util.api('pay/balance_pay', 'POST', params, function (data) {
      wx.hideLoading();
      if (data.success) {
        wx.showToast({
          title: '购买成功',
        });
        setTimeout(function () {
          this_.popViewPay.hidePopup();
          this_.refresh();
        }, 800);
      } else if (data.status == 4005) {
        wx.showModal({
          title: '密码错误',
          content: '支付密码不正确',
          confirmText: '找回密码',
          cancelText: '重新输入',
          success: function (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/user/userinfo/bindPhone/bind_phone?send_type=reset_pay_pass',
              })
            } else {
              setTimeout(function () {
                this_.popViewPay.setData({
                  pwd: ''
                })
              }, 500)
            }
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        });
        this_.setData({
          buyIng: true
        });
        this_.popViewPay.setData({
          pwd: ''
        });
        setTimeout(function () {
          wx.hideLoading();
          this_.popViewPay.hidePopup();
        }, 800);
      }
    });
  },

  /**
  * 获取用户信息
  */
  refresh: function (e) {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function (data) {
      if (data.success == 1) {
        app.data.userinfo = data.data;
        that.setData({
          userinfo: data.data,
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  showPay: function () {
    this.popViewPay.showPopup();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if (app.globalData.userSys == 'iOS') {
      that.setData({
        is_ios: true
      })
    }
    if (app.data.userinfo.user_type == 2) {
      that.setData({
        isVip: true,
      })
    } else {
      that.setData({
        isVip: false,
      })
    }
    that.setData({
      data_array: app.config.vip_items(),
      meau_data: {
        sysWidth: app.globalData.sysWidth,
        itemHeight: app.globalData.sysWidth / 3,
        itemImageWidth: app.globalData.sysWidth / 9 * 0.9,
      },
      vip_buy_items: app.config.vip_buy_items(),
      userinfo: app.data.userinfo
    });
    that.loading()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.popViewPay = this.selectComponent("#popview_pay");
  },

/**
 * 获取用户信息
 */
  refresh: function (e) {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function (data) {
      if (data.success == 1) {
        app.data.userinfo = data.data;
        that.setData({
          userinfo: data.data,
          balance: data.data.balance
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.refresh();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})