var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    auth: false,
    auth_info: null,
    authStatus: false
  },

  // 显示营业执照
  showLicense: function () {
    var that = this
    wx.previewImage({
      urls: [
        that.data.auth_info.license,
        that.data.auth_info.door
      ],
      current: that.data.auth_info.license
    })
  },

  // 显示门头照
  showSignboard: function () {
    var that = this
    wx.previewImage({
      urls: [
        that.data.auth_info.license,
        that.data.auth_info.door
      ],
      current: that.data.auth_info.door
    })
  },

  /**
   * 进行实名认证跳页
   */
  auth: function() {
    wx.navigateTo({
      url: '../personal_authentication/personal_authentication',
    })
  },

  /**
   * 选择公司
   */
  ChooseCompany: function() {
    wx.navigateTo({
      url: '/pages/product/company/company_search?isAuth=true',
    })
  },

  /**
   * 上传图片
   */
  upload: function(e) {
    var that = this;
    app.util.UploadImage('userinfo/upload', 'AuthCompany', function(data) {
      that.setData({
        url: data.data
      })
    }, function(data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },

  upload1: function(e) {
    var that = this;
    app.util.UploadImage('userinfo/upload', 'AuthCompany', function(data) {
      that.setData({
        url1: data.data
      })
    }, function(data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },

  /**
   * 认证信息提交
   */
  CompanyAuthPost: function(e) {
    var that = this;
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) {
      if (data.success == 0) {
        console.log(data)
      }
    })

    // 判断个人实名状态
    if (!app.util.checkIdentify(app.data.userinfo)) {
      return;
    }
    if (app.data.company_choose == undefined) {
      wx.showToast({
        title: '请选择公司',
        icon: 'none'
      })
      return;
    }
    if (that.data.url == undefined || that.data.url1 == undefined) {
      wx.showToast({
        title: '请上传指定图片',
        icon: 'none'
      })
      return;
    }
    // 传递参数
    var params = {
      company_id: app.data.company_choose.companyId,
      license: that.data.url,
      door: that.data.url1
    }
    //  提交服务器
    app.util.api('company_auth/auth_post', 'POST', params, function(data) {
      if (data.success == 1) {
        wx.showModal({
          title: '提交成功',
          content: data.msg,
          showCancel: false,
          success: function(res) {
            if (res.confirm) {
              wx.navigateBack({
                delta: 1
              })
            }
          }
        })
      } else {
        wx.showModal({
          title: '重复认领',
          content: data.msg,
          showCancel: false,
          success: function(res) {
            if (res.confirm) {
              wx.navigateBack({
                delta: 1
              })
            }
          }
        })
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    var that = this;
    var params = {
      'type': 'auth_info'
    };
    app.util.api('company_auth/auth_info', 'POST', params, function(data) {
      if (data.status == 200) {
        that.setData({
          auth_info: data.data,
          authStatus: true
        })
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // 判断是否已选择要认领的公司
    if (app.data.company_choose !== undefined) {
      this.setData({
        company_name: app.data.company_choose.name,
        company_id: app.data.company_choose.companyId
      })
    }
    // 判断用户实名状态
    if (app.data.userinfo.user_status == 2) {
      this.setData({
        auth: false
      })
    } else {
      this.setData({
        auth: true
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})