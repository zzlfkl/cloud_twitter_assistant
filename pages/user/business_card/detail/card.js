// pages/user/business_card/detail/card.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    company_status: true
  },
  /**
   * 回到首页
   */
  home: function() {
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
  /**
   * 获取名片信息
   */
  get_info: function(id) {
    var that = this;
    var params = {
      user_id: id
    }
    app.util.api('qr/business_card', 'GET', params, function(data) {

      if (data.data.company_id == null) {
        that.setData({
          company_status: false,
        })
      }
      that.setData({
        userinfo: data.data,
        itemDataArray: [{
            title: data.data.mobile,
            image: "/img/card/phone.png",
            hint: '',
            bottomLine: true,
            topLine: false,
            rightArrow: true,
            identify: "0",
            show: true
          }, {
            title: data.data.company.formatted_address,
            image: "/img/company_location.png",
            hint: '',
            bottomLine: true,
            topLine: false,
            rightArrow: true,
            identify: "1",
            show: that.data.company_status
          },
          {
            title: data.data.signature,
            image: "/img/card/introduce.png",
            hint: '',
            bottomLine: true,
            topLine: false,
            rightArrow: false,
            identify: "2",
            show: true
          },
        ],
      })
    });
  },
  /**
   * 
   */
  addFriend: function() {
    wx.addPhoneContact({
      firstName: this.data.userinfo.user_nickname,
      mobilePhoneNumber: this.data.userinfo.mobile,
      remark: '金融经纪人'
    })
  },
  /**
   * 列表点击
   */
  itemTap: function(e) {
    var identify = e.currentTarget.dataset.
    identify
    if (identify == 0) {
      wx.makePhoneCall({
        phoneNumber: this.data.userinfo.mobile,
      })
    } else if (identify == 1) {
      wx.openLocation({
        latitude: parseFloat(this.data.userinfo.company.lat),
        longitude: parseFloat(this.data.userinfo.company.lng),
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.q !== undefined) {
      var scan_url = decodeURIComponent(options.q); //字符分割console.log(scan_url); //打印出url//分割网址，提取出参数
      var surls = scan_url.split("qrcode?user_id=")
      var vid = surls[1];
      this.get_info(vid);
    }
    if (options.user_id !== undefined) {
      this.get_info(options.user_id)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})