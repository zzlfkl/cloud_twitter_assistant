// pages/tools/business_card/detail/detail.js
var app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    company_status: true,
    status: false
  },

  /**
   * 显示个人二维码
   */
  showQrCode: function() {
    var that = this;
    var params = {
      user_id: app.data.userinfo.id
    }
    app.util.api('index/qr_code', 'GET', params, function(data) {
      if (data.success == 1) {
        wx.previewImage({
          current: data.data, // 当前显示图片的http链接
          urls: [
            data.data
          ]
        })
      }
    })
  },

  /**
   * 编辑个人资料
   */
  edit: function() {
    wx.navigateTo({
      url: '/pages/user/userinfo/user_detail',
    })
  },

  /**
   * 列表点击
   */
  itemTap: function(e) {
    var identify = e.currentTarget.dataset.
    identify
    if (identify == 0) {
      wx.makePhoneCall({
        phoneNumber: app.data.userinfo.mobile,
      })
    } else if (identify == 1) {
      wx.openLocation({
        latitude: parseFloat(app.data.userinfo.company_info.lat),
        longitude: parseFloat(app.data.userinfo.company_info.lng),
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (app.data.userinfo.company_id == null) {
      this.setData({
        company_status: false
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      itemDataArray: [{
          title: app.data.userinfo.mobile,
          image: "/img/card/phone.png",
          hint: '',
          bottomLine: true,
          topLine: false,
          rightArrow: true,
          identify: "0",
          show: true
        }, {
          title: this.data.company_status ? app.data.userinfo.company_info.formatted_address : '',
          image: "/img/company_location.png",
          hint: '',
          bottomLine: true,
          topLine: false,
          rightArrow: true,
          identify: "1",
          show: this.data.company_status
        },
        {
          title: app.data.userinfo.signature,
          image: "/img/card/introduce.png",
          hint: '',
          bottomLine: true,
          topLine: false,
          rightArrow: false,
          identify: "2",
          show: true
        },
      ],
      userinfo: app.data.userinfo
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  shareCard: function() {
    this.setData({
      status: true
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var that = this;
    return {
      title: '云推客-让贷款更简单',
      path: '/pages/user/business_card/detail/card?user_id=' + app.data.userinfo.id,
      // imageUrl:'/img/logo.png',
      complete: function(res) {
        that.setData({
          status: false
        })
      }

    }
  }
})