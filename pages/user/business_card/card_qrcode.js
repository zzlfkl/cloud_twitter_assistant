// pages/user/business_card/card_qrcode.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  /**
   * 二维码
   */
  qr_code: function(user_id) {
    var that = this;

    //请求网络图片资源
    var params = {
      user_id: user_id
    }
    app.util.api('index/qr_code', 'POST', params, function(data) {
      if (data.success == 1) {
        that.setData({
          url: data.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          window_width: res.windowWidth,
          window_height: res.windowHeight,
        })
      }
    })
    if (app.data.userinfo == undefined) {
      that.setData({
        userinfo: options
      })
      that.qr_code(options.user_id)
    } else {
      that.setData({
        userinfo: app.data.userinfo
      })
      that.qr_code(app.data.userinfo.id)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: '云推客-让贷款更简单',
      path: '/pages/user/business_card/card_qrcode?user_id=' + app.data.userinfo.id + '&&user_nickname=' + app.data.userinfo.user_nickname + '&&signature=' + app.data.userinfo.signature + '&&avatar=' + app.data.userinfo.avatar,
    }
  }
})