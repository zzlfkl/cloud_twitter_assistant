var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    homeHide:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if(options.type !== undefined){
      if (app.data.userinfo == undefined) {
        app.util.login(function (data) {
          if (data.success == 1) {
            app.data.userinfo = data.data;
            wx.setStorageSync('token', data.other);
            that.qrcode(options.id);
            that.setData({
              code: options.key
            })
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        })
      } else {
        that.qrcode(options.id);
        that.setData({
          code: app.data.userinfo.user_activation_key
        })
      }
      that.setData({
        homeHide:false
      })
    }else{
      that.qrcode();
      that.setData({
        code: app.data.userinfo.user_activation_key
      })
    }
    
  },

  /** 
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },
  /**
   * 获取二维码URL
   */
  qrcode: function(user_id) {
    var that = this;
    var params = {
      'type': 'qrcode',
      'user_id': user_id
    };
    app.util.api('userproduct/get_qr', 'GET', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        that.setData({
          'url': data.data
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
        that.setData({
          'url': ''
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  command: function() {
    wx.navigateTo({
      url: '/pages/webview/index?url=https://wx.ytk18.com/portal/index/command',
    })
  },
  rule: function() {
    wx.navigateTo({
      url: '/pages/webview/index?url=https://wx.ytk18.com/portal/index/partner',
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

   onShareAppMessage: function () { 
    return {
      title: '云推客-让贷款更简单',
      path: '/pages/logs/logs?scene=' + app.data.userinfo.id,
    }
  }
})