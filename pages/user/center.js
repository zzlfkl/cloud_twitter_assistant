var app = getApp();
Page({

  data: {
    toView: 'red',
    scrollTop: 100,
    flag: true,
    bind: false,
    is_ios: false,
    itemArray: [{
        name: '我的钱包',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_center_wallet.png',
        line: true,
        block: false,
        notice: false,
        hide: false,
        url: '/pages/user/wallet/index/main'
      },
      {
        name: '会员中心',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_center_hat.png',
        line: true,
        block: false,
        notice: false,
        hide: false,
        url: '/pages/user/vip/vip/vip_center'
      },
      {
        name: '公司认领',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_center_auth.png',
        line: false,
        block: true,
        notice: false,
        hide: false,
        url: 'auth_company/company'
      },
      {
        name: '意见反馈',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_center_advice.png',
        line: true,
        block: false,
        notice: false,
        hide: false,
        url: '/pages/user/setting/feedback/send_feedback'
      },
      {
        name: '用户设置',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/user_center_set.png',
        line: false,
        notice: false,
        block: true,
        hide: false,
        url: 'setting/setting'
      },
    ]
  },

  local: function(e) {
    wx.getLocation({
      type: 'wgs84',
      success: function(res) {
        var latitude = res.latitude
        var longitude = res.longitude
        var speed = res.speed
        var accuracy = res.accuracy
      }
    })
  },

  chat: function() {
    wx.navigateTo({
      url: '/pages/WebSocket/index',
    })
  },


  // 列表点击事件
  itemClick: function(e) {
    var url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },

  /**
   * 编辑个人资料
   */
  edit_userinfo: function() {
    wx.navigateTo({
      url: 'userinfo/user_detail',
    })
  },

  /**
   * 开通VIP
   */
  vip: function(e) {
    wx.navigateTo({
      url: '/pages/user/vip/vip/vip_center',
    })
  },

  /**
   * 推广赚拥
   */
  extension: function(e) {
    wx.navigateTo({
      url: 'extension/extension',
    })
  },


  authlist: function(e) {
    wx.navigateTo({
      url: 'auth_company/company',
    })
  },
  order: function(e) {
    wx.navigateTo({
      url: 'order/order',
    })
  },
  /**
   * 我的推广码
   */
  qrcode: function(e) {
    wx.navigateTo({
      url: 'qrcode/qrcode',
    })
  },

  hide: function() {
    this.setData({
      flag: true
    })
  },
  over: function() {

  },

  /**
   * 绑定微信
   */
  bindGetUserInfo: function() {
    var that = this;
    wx.getUserInfo({
      success: function(info) {
        var rawData = info['rawData'];
        var signature = info['signature'];
        var encryptedData = info['encryptedData'];
        var iv = info['iv'];
        var params = {
          "rawData": rawData,
          "signature": signature,
          'iv': iv,
          'encryptedData': encryptedData,
        };
        app.util.api('login/bind_wx', 'POST', params, function(data) {

          if (data.success == 1) {
            wx.showToast({
              title: data.msg,
            })
            that.setData({
              wxBind: true
            })
            that.loading();
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        })
      }
    })
  },

  onLoad: function() {
    if (app.globalData.userSys == 'iOS') {
      this.data.itemArray[1].hide = true;
      this.setData({
        itemArray: this.data.itemArray
      })
    }
    if (app.data.userinfo == undefined) {
      this.loading();
    } else {
      //验证是否为游客
      if (app.data.userinfo.user_nickname == '游客' && app.data.userinfo.avatar == '') {
        this.setData({
          wxBind: false
        })
      } else {
        this.setData({
          wxBind: true
        })
      }
      this.setData({
        userinfo: app.data.userinfo
      })
    }
  },
  /**
   * 获取用户信息
   */
  loading: function(e) {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo = data.data;
        that.setData({
          userinfo: data.data,
        })
        //若无公司信息则获取
        if (app.data.userinfo.company_info == undefined) {
          app.util.set_company_info();
        }
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  tap: function(e) {
    for (var i = 0; i < order.length; ++i) {
      if (order[i] === this.data.toView) {
        this.setData({
          toView: order[i + 1]
        })
        break;
      }
    }
  },

  /**
   * 下拉刷新
   */
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading();
    this.loading();
    this.getAdvice();
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
  },

  /**
   * 获取反馈信息提示
   */
  getAdvice: function() {
    var that = this;
    var params = {
      type: 'getAdvice'
    }
    app.util.api("userinfo/get_advice_info", 'GET', params, function(data) {
      app.data.userinfo.feedback_status = data.data.feedback_status;
      that.data.itemArray[3].notice = data.data.feedback_status;
      that.setData({
        itemArray: that.data.itemArray
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.getAdvice();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    if (app.data.userinfo !== undefined) {

      //验证游客身份
      if (app.data.userinfo.user_nickname == '游客' && app.data.userinfo.avatar == '') {
        this.setData({
          wxBind: false
        })
      } else {
        this.setData({
          wxBind: true
        })
      }
      if (app.data.userinfo.extension_id !== null) {
        that.setData({
          bind: true,
        })
      }

      if (app.data.userinfo.feedback_status !== undefined) {
        that.data.itemArray[3].notice = app.data.userinfo.feedback_status;
        that.setData({
          itemArray: that.data.itemArray
        })
      }
    };


    that.setData({
      userinfo: app.data.userinfo
    })
  },
})