var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    user_avatar: null,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      user_avatar: app.data.userinfo.avatar,
      // showStatus: app.data.showStatus,
    })
    if (app.data.userinfo.user_status == 3 || app.data.userinfo.user_status == 1) {
      var params = {
        'type': 'auth_info'
      };
      app.util.api('userinfo/auth_info', 'POST', params, function(data) {
        console.log(data)
        if (data.success == 1) {
          that.setData({
            authinfo: data.data
          })
        }
      })
      that.setData({
        status: true,
        userstatus: app.data.userinfo.user_status
      })
    } else {
      that.setData({
        userstatus: app.data.userinfo.user_status,
        status: false
      })
    }
  },

  formSubmit: function(e) {
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
    // 验证真实姓名
    if (e.detail.value.real_name == '') {
      wx.showToast({
        title: '请输入真实姓名',
        icon: 'none'
      })
      return;
    }
    if (e.detail.value.real_name.length > 4 || !app.util.checkname(e.detail.value.real_name) || e.detail.value.real_name.length < 2) {
      wx.showToast({
        title: '用户名格式错误',
        icon: 'none'
      })
      return;
    }
    if (e.detail.value.real_name.length > 4) {
      wx.showToast({
        title: '用户名不得大于四位',
        icon: 'none'
      })
      return;
    }
    //验证身份证号码
    if (!app.util.checkidcard(e.detail.value.id_num)) {
      wx.showToast({
        title: '身份证号码格式错误',
        icon: 'none'
      })
      return;
    }

    var that = this;
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) {
      if (data.success == 0) {
        console.log(data)
      }
    })

    var params = e.detail.value;
    params.city_code = app.data.globalCity.val;
    app.util.api('userinfo/auth_post', 'POST', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        wx.showToast({
          title: data.msg,
        })
        app.data.userinfo.user_status = 1;
        setTimeout(function() {
          wx.navigateBack({
            delta: 1
          })
        }, 2000)
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  upload1: function(e) {
    var that = this;
    app.util.UploadImage('userinfo/upload', 'idcard', function(data) {
      that.setData({
        url2: data.data
      })
    }, function(data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },
  /**
   * 图片上传
   */
  upload: function(e) {
    var that = this;
    app.util.UploadImage('userinfo/upload', 'idcard', function(data) {
      that.setData({
        url1: data.data
      })
    }, function(data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
})