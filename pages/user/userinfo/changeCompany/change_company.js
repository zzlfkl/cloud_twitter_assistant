// pages/info/detail/changeCompnay/change_company.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    keyword: ''
  },

  /**
   * 确定公司名称
   */
  confirm: function(e) {

  },
  /**
   * 选择公司事件
   */
  change_company: function(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    if (app.data.userinfo.company_info !== undefined) {
      // 确认修改公司
      wx.showModal({
        title: '更换公司',
        content: '确定要更换公司码？',
        confirmText: '我要换',
        cancelText: '不换了',
        success: function(res) {
          if (res.confirm) {
            var params = {
              'company_id': id
            };
            app.util.api('map/change_company', 'POST', params, function(data) {
              console.log(data)
              if (data.success == 1) {
                app.data.userinfo.company_info.name = data.data.name;
                app.data.userinfo.company_info.formatted_address = data.data.b_address;

                //是否提交认证申请
                wx.showModal({
                  title: '绑定成功',
                  content: '是否提交认证申请加入该公司？',
                  confirmText: '我要加入',
                  cancelText: '再想想',
                  success: function(res) {
                    if (res.confirm) {
                      app.util.api('elites/apply_join', 'POST', params, function(data) {
                        console.log(data);
                        if (data.success == 1) {
                          wx.showModal({
                            title: '申请成功',
                            content: data.msg,
                            showCancel: false,

                            // 成功返回
                            success: function(res) {
                              wx.navigateBack()
                            }
                          })
                          // 错误提示
                        } else {
                          wx.showToast({
                            title: '申请失败，请稍后重试',
                            icon: 'none'
                          })
                        }
                      })
                    }
                  }
                })
                // 返回上级页面

              }
            })
          }
        }
      })
    } else {
      var params = {
        company_id: id
      }

      //加入公司提示
      wx.showModal({
        title: '绑定公司',
        content: '确认绑定该公司吗？',
        confirmText: '确定',
        cancelText: '再想想',
        success: function(res) {
          if (res.confirm) {
            app.util.api('elites/add_company', 'POST', params, function(data) {
              console.log(data);
              if (data.success == 1) {
                //设置本地公司信息
                app.util.set_company_info();
                //是否提交认证申请
                wx.showModal({
                  title: '绑定成功',
                  content: '是否提交认证申请加入该公司？',
                  confirmText: '我要加入',
                  cancelText: '再想想',
                  success: function(res) {
                    if (res.confirm) {
                      app.util.api('elites/apply_join', 'POST', params, function(data) {
                        console.log(data);
                        if (data.success == 1) {
                          wx.showModal({
                            title: '申请成功',
                            content: data.msg,
                            showCancel: false,

                            // 成功返回
                            success: function(res) {
                              if (res.confirm) {
                                wx.navigateBack({
                                  delta: 1
                                })
                              }
                            }
                          })

                          // 错误提示
                        } else {
                          wx.showToast({
                            title: '申请失败，请稍后重试',
                            icon: 'none'
                          })
                        }
                      })
                    }
                  }
                })

                // 错误提示
              } else {
                console.log(data)
                // wx.showToast({
                //   title: data.msg,
                //   icon: 'none'
                // })
              }
            });
          }
        }
      })
    }

  },
  /**
   * 监听输入字数
   */
  bindTextInput: function(e) {
    var keyword = e.detail.value
    this.setData({
      list: [],
      keyword: keyword,
      page: 1
    })
    this.company_search(keyword);
  },
  /**
   * 公司搜索
   */
  company_search(keyword) {
    var that = this;
    var params = {
      'keyword': keyword,
      'page': that.data.page
    }
    app.util.api('map/company_search', 'POST', params, function(data) {
      if (data.success == 1) {
        that.setData({
          list: that.data.list.concat(data.data)
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    this.setData({
      company: options.info,
      list: [],
      page: 1
    })
    this.company_search(options.info);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.setData({
      page: this.data.page + 1
    })
    this.company_search(this.data.keyword)
  }
})