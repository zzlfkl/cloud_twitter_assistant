// pages/info/detail/elite/edit_elite.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    company: null,
    itemDataArray: [{
        title: "所在公司",
        image: "",
        hint: "",
        hintColor: "#303030",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "1",
        show: true
      },
      {
        title: "职位",
        image: "",
        hint: "",
        hintColor: "#303030",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "2",
        show: true
      }, {
        title: "精英优势",
        image: "",
        hint: "",
        hintColor: "#303030",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "3",
        show: true
      }
    ]
  },


  // 修改精英优势
  changeIntroduce: function(e) {

    if (app.data.userinfo !== undefined) {
      var array = this.data.itemDataArray
      array[2].hint = (app.data.userinfo.advantage ? app.data.userinfo.advantage : '未填写');
      this.setData({
        itemDataArray: array
      })
    }
  },

  // 修改职位
  changeJob: function(e) {
    if (app.data.userinfo.company_info !== undefined) {
      var array = this.data.itemDataArray
      array[1].hint = (app.data.userinfo.company_info.position ? app.data.userinfo.company_info.position : '未填写');
      this.setData({
        itemDataArray: array
      })
    }
  },

  // 修改公司
  changeCompany: function() {
    if (app.data.userinfo.company_info !== undefined) {
      var array = this.data.itemDataArray
      array[0].hint = app.data.userinfo.company_info.name;
      this.setData({
        itemDataArray: array
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.changeCompany();
    this.changeJob();
    this.changeIntroduce();

    //若用户认领公司审核通过，绑定公司与认领公司不一致，则修改
    if (app.data.userinfo.managerCompany !== undefined && app.data.userinfo.managerCompany.id !== app.data.userinfo.company_info.id) {
      var params = {
        'rightCid': app.data.userinfo.managerCompany.id
      };
      app.util.api('elites/correcting_company', 'POST', params, function(data) {
        console.log(data)
      });
    }
  },

  /**
   * 列表点击
   */
  itemTap: function(e) {
    var identify = e.currentTarget.dataset.identify;

    if (identify == 1) {

      //验证是否认领过公司
      if (app.data.userinfo.managerCompany !== undefined) {
        wx.showModal({
          title: '公司已存在',
          content: '您已认领过公司，解除后可加入其他公司',
        })
        return;
      } else {
        wx.navigateTo({
          url: "/pages/user/userinfo/changeCompany/change_company?info=",
        })
      }
    } else if (identify == 2) {
      if (app.data.userinfo.company_info !== undefined) {
        var job = (app.data.userinfo.company_info.position ? app.data.userinfo.company_info.position : '')
        wx.navigateTo({
          url: '/pages/user/userinfo/changeJob/change_job?info=' + job,
        })
      } else {
        wx.showModal({
          title: '未绑定公司',
          content: '请先完善公司信息',
          showCancel: false
        })
      }

    } else if (identify == 3) {
      if (app.data.userinfo !== undefined) {
        var advantage = app.data.userinfo.advantage;
        wx.navigateTo({
          url: '/pages/user/userinfo/elite/introduce/introduce?info=' + advantage,
        })
      } else {
        wx.showToast({
          title: '好像出问题了，重新打开试试吧',
          icon: 'none'
        })
      }

    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})