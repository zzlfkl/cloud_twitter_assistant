// pages/info/detail/introduce/introduce.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    textContent: '',
    textLength: 0,
    maxLength: 240,
  },

  /**
   * 表单提交
   */
  formsubmit: function(e) {
    // 网络请求...
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
    var that = this;
    var params = {
      'advantage': e.detail.value.content
    }
    app.util.api('elites/change_advantage', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo.advantage = data.data;
        wx.showToast({
          title: data.msg,

          duration: 1500
        });
        setTimeout(function() {
          wx.navigateBack()
        }, 1500)
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    });
  },

  /**
   * 监听输入字数
   */
  bindTextAreaInput: function(e) {
    var value = e.detail.value
    console.log(value.length)
    console.log(this.data.maxLength)
    if (value.length > this.data.maxLength) {
      value = value.substr(0, this.data.maxLength)
    }
    this.setData({
      textContent: value,
      textLength: value.length,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.info !== undefined) {
      this.setData({
        textContent: (options.info == 'null' ? '' : options.info)
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})