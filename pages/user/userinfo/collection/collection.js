// pages/tools/activity/manage/manage.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    bar_item_width: 0,
    swiper_current: 0,
    show: true,
    bar_item_titles: [{
      "title": "精英收藏",
      "selectedTitle": "精英收藏",
      "matchingId": "0",
      "isSelected": true,
      'page': 1,
      item_array: [],
    }, {
      "title": "公司收藏",
      "selectedTitle": "公司收藏",
      "matchingId": "1",
      "isSelected": false,
      'page': 1,
      item_array: [],
    }, {
      "title": "产品收藏",
      "selectedTitle": "产品收藏",
      "matchingId": "2",
      "isSelected": false,
      'page': 1,
      item_array: [],
    }],
    item_array: [],
  },

  // tab绑定点击事件
  barItemTap: function(e) {
    var temp_array = this.data.bar_item_titles;
    for (var idx = 0; idx < temp_array.length; idx++) {
      temp_array[idx].isSelected = false;
      if (temp_array[idx].matchingId == e.currentTarget.dataset.identify) {
        temp_array[idx].isSelected = true;
      }
    }
    // 重置swiper选项
    this.setData({
      swiper_current: e.currentTarget.dataset.identify,
      item_array: [],
      page: 1
    })
    this.getContent(this.data.page);
    this.reloadData(temp_array);
  },

  // 刷新顶部tab数据
  reloadData: function(array) {
    this.setData({
      bar_item_titles: array,
      bar_item_width: app.globalData.sysWidth / array.length
    })
  },

  /**
   * 获取服务端数据
   */
  getContent: function(page) {
    var that = this;

    switch (that.data.swiper_current) {
      case 0:
        var url = 'elites/elite_collection_list'
        break;
      case 1:
        var url = 'elites/company_collection_list'
        break;
      case 2:
        var url = 'userinfo/pro_me'
        break;
    }

    var params = {
      'user_id': app.data.userinfo.id,
      'page': page
    }
    if (that.data.swiper_current == 2) {
      params.value = that.data.swiper_current;
    }
    app.util.api(url, 'GET', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        if (that.data.swiper_current == 2) {
          for (var k = 0, length = data.data.length; k < length; k++) {
            if (data.data[k].limit_months < 12) {
              data.data[k].limit_months = data.data[k].limit_months + '月';
            } else {
              data.data[k].limit_months = data.data[k].limit_months / 12 + '年';
            }
            if (data.data[k].open_days == 1) {
              data.data[k].open_days = '当日';
            } else if (data.data[k].open_days == 2) {
              data.data[k].open_days = '次日';
            } else if (data.data[k].open_days == 3) {
              data.data[k].open_days = '3天';
            } else if (data.data[k].open_days == 7) {
              data.data[k].open_days = '1周';
            } else if (data.data[k].open_days == 14) {
              data.data[k].open_days = '2周';
            } else if (data.data[k].open_days == 21) {
              data.data[k].open_days = '3周';
            } else if (data.data[k].open_days == 30) {
              data.data[k].open_days = '1月';
            } else {
              data.data[k].open_days = data.data[k].open_days + '日';
            }
          }
        }
        that.setData({
          item_array: that.data.item_array.concat(data.data),
        })
        if (data.data.length == 0 && that.data.page == 1) {
          that.setData({
            show: false,
          })
        } else {
          that.setData({
            show: true,
          })
        }
      }
    });
  },

  productTap: function(e) {
    var id = parseInt(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/index/detail?id=' + id,
    })
  },

  /**
   *用户详情点击 
   */
  user_click: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/product/cream/detail/elite_detail?id=' + id,
    })
  },

  /**
   * 选择公司事件
   */
  change_company: function(e) {
    var companyId = e.currentTarget.dataset.id;
    if (this.data.isAuth) {
      var company = e.currentTarget.dataset.name;
      var companyId = e.currentTarget.dataset.id;
      app.data.company_choose = {
        name: company,
        companyId: companyId
      };
      wx.navigateBack({
        delta: 1
      })
    } else {
      wx.navigateTo({
        url: '/pages/product/company/detail/company_detail?id=' + companyId,
      })
    }

  },

  /**
   * 主营产品详情
   */
  productDetail: function(e) {
    if (!app.util.checkauth(app)) {
      return;
    }
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/detail?id=' + id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getContent(this.data.page);
    this.reloadData(this.data.bar_item_titles)
    this.forData();
  },

  /**
   * 请求数据
   */
  forData: function() {
    var title_items = this.data.bar_item_titles[this.data.swiper_current]
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      page: 1,
      item_array: []
    })
    this.getContent(this.data.page);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.swiper_current !== 2) {
      if (this.data.item_array.length !== 0) {
        this.setData({
          page: this.data.page + 1
        })
        this.getContent(this.data.page)
      }
    }

  },
})