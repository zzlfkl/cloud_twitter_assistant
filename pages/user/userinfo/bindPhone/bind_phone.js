// pages/info/detail/bindPhone/bind_phone.js
var timer; // 计时器
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sendeCodeBtnTitle: '发送验证码',
    // 等时 ()
    waitTime: 60,
    countTime: 6,
  },

  /**
   * 读秒
   */
  readTime: function() {
    // 减时间
    this.data.countTime--

      // 判断是否结束
      if (this.data.countTime < 1) {
        // 读秒结束 重新赋值
        this.setData({
          countTime: this.data.waitTime,
          sendeCodeBtnTitle: '发送验证码',
        })
        // 取消定时器
        clearTimeout(timer)
        return;
      }

    // 读秒钟的时间显示
    var time_code = this.data.countTime + '秒后可重发'
    this.setData({
      sendeCodeBtnTitle: time_code,
    })

    // 重复执行读秒
    var that = this
    timer = setTimeout(function() {
      that.readTime()
    }, 1000);
  },

  /**
   * 发送验证码
   */
  sendCode: function(dic) {
    var that = this;
    if (that.data.send_type !== undefined && that.data.send_type == 'reset_pay_pass') {
      var type = 'resetpaypass'
    }else{
      var type = 'new_mobile'
    }
    var reg = /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/;
    if (!reg.test(dic['phone'])) {
      // 不合法数据
      wx.showToast({
        title: '手机号格式错误',
        icon: 'none'
      })
      return;
    }
    var params = {
      'mobile': dic['phone'],
      'type': type,
      'user_id': app.data.userinfo.id
    }
    app.util.api('index/mobile_code', 'POST', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        if (that.data.countTime != that.data.waitTime) {
          // 还在等时
          wx.showToast({
            title: '请勿重复发送',
            icon: 'none'
          })
          return;
        }
        that.readTime();
        wx.showToast({
          title: data.msg,
          duration: 1500
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 表单提交
   */
  formsubmit: function(e) {
    var that = this;
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) {
      if (data.success == 0) {
        console.log(data)
      }
    })
    if (e.detail.target.dataset.type == 1) {
      this.sendCode(e.detail.value)
      return;
    } else {
      var params = e.detail.value;
      if (that.data.send_type !== undefined && that.data.send_type == 'reset_pay_pass'){
        app.util.api('userinfo/reset_pay_pass','POST',params,function(data){
          if(data.success){
            wx.navigateTo({
              url: '/pages/pay/pwd/index?origin=code',
            })
          }else{
            wx.showToast({
              title: data.msg,
              icon:'none'
            })
          }
        })
      }else{
        app.util.api('userinfo/bind_mobile_bycode', 'POST', params, function (data) {
          if (data.success == 1) {
            console.log(params);
            // 判断是否为推广人二维码扫描
            if (that.data.extension_id !== undefined) {
              var params = {
                user_id: that.data.extension_id,
              };
              app.util.api('userinfo/qr_post', 'POST', params, function (data) {
                if (data.success == 1) {
                  wx.showToast({
                    title: data.msg,
                    icon: 'none'
                  })
                }
              })
            }
            //存储token
            wx.setStorageSync('token', data.data)
            app.data.userinfo.mobile = data.other
            wx.showToast({
              title: data.msg,
              duration: 500,
              success: function () {
                setTimeout(function () {
                  wx.reLaunch({
                    url: '/pages/index/index',
                  })
                }, 1000)
              }
            })
          } else {
            console.log(data);
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        });
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.data.countTime = this.data.waitTime;
    if (options.extension_id !== undefined) {
      this.setData({
        extension_id: options.extension_id
      })
    };
    if(options.send_type !== undefined && options.send_type == 'reset_pay_pass'){
      this.setData({
        send_type:'reset_pay_pass',
        fixd_mobile:app.data.userinfo.mobile
      });
      wx.setNavigationBarTitle({
        title: '修改支付密码',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  }
})