// pages/info/detail/changeJob/change_job.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 表单提交
   */
  formsubmit: function(e) {
    var that = this;
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) {})

    //提交
    var params = e.detail.value;
    app.util.api('userinfo/update_company_info', 'POST', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        app.data.userinfo.company_info.position = data.data;
        wx.showToast({
          title: data.msg,
          duration: 500,
          success: function() {
            setTimeout(function() {
              wx,
              wx.navigateBack({
                delta: 1,
              })
            }, 1000)
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          duration: 1000,
          icon: 'none',
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      position: options.info
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})