// pages/info/detail/changeName/change_name.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  /**
   * 表单提交
   */
  formsubmit: function(e) {
    var that = this;

    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) {
      if (data.success == 0) {
        console.log(data)
      }
    })

    var params = e.detail.value;
    app.util.api('userinfo/change_userinfo', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo.user_nickname = data.data.user_nickname
        wx.showToast({
          title: data.msg,
          duration: 500,
          success: function() {
            setTimeout(function() {
              wx,
              wx.navigateBack({
                delta: 1,
              })
            }, 1000)
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          duration: 1000,
          icon: 'none',
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      info: options.info
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  }
})