// pages/user/userinfo/company/staff/manager.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: [],
    startX: 0, //开始坐标
    startY: 0,
    tab: true
  },
  //手指触摸动作开始 记录起点X坐标
  touchstart: function(e) {
    //开始触摸时 重置所有删除
    this.data.items.forEach(function(v, i) {
      if (v.isTouchMove) //只操作为true的
        v.isTouchMove = false;
    })

    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      items: this.data.items
    })
  },

  //滑动事件处理
  touchmove: function(e) {
    var that = this,
      index = e.currentTarget.dataset.index, //当前索引
      startX = that.data.startX, //开始X坐标
      startY = that.data.startY, //开始Y坐标
      touchMoveX = e.changedTouches[0].clientX, //滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY, //滑动变化坐标

      //获取滑动角度
      angle = that.angle({
        X: startX,
        Y: startY
      }, {
        X: touchMoveX,
        Y: touchMoveY
      });
    that.data.items.forEach(function(v, i) {
      v.isTouchMove = false

      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {

        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })

    //更新数据
    that.setData({
      items: that.data.items
    })
  },

  //删除事件
  del: function(e) {
    var that = this;
    var staffId = e.currentTarget.dataset.id; //当前索引
    var params = {
      'staffId': staffId
    }

    wx.showModal({
      title: '移出员工',
      content: '确定要将该员工移出公司吗？',
      success: function(e) {
        if (e.confirm) {
          app.util.api('elites/staff_del', 'POST', params, function(data) {
            if (data.success == 1) {
              wx.showToast({
                title: data.msg,
              })
              that.getList('on_job');
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          });
        }
      }
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function(start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y

    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  getList: function(types) {
    var that = this;
    var params = {
      'companyId': app.data.userinfo.managerCompany.id,
      'type': types
    }
    app.util.api('elites/staffs_onJob', 'GET', params, function(data) {
      console.log(data)
      that.setData({
        items: data.data
      });
    })
  },

  /**
   * 员工详情
   */
  staffDetail: function(e) {
    var staffId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/product/cream/detail/elite_detail?id=' + staffId,
    })
  },

  //tab点击事件
  chouse: function(e) {
    var tab = e.currentTarget.dataset.tab;
    if (tab == 0) {
      this.getList('apply')
    } else if (tab == 1) {
      this.getList('on_job')
    }
    this.setData({
      tab: parseInt(tab)
    })
  },

  //按钮点击
  apply: function(e) {
    var that = this;
    var staffId = e.currentTarget.dataset.id,
      status = parseInt(e.currentTarget.dataset.status);
    var params = {
      staffId: staffId,
      status: status
    }
    app.util.api('elites/staff_auth', 'POST', params, function(data) {
      that.getList('apply');
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getList('on_job');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})