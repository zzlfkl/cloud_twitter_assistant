// pages/info/detail/company/edit_company.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemDataArray: [{
        title: "公司全称",
        image: "",
        hintColor: "#303030",
        hint: "安徽推客网络科技服务有限公司",
        bottomLine: true,
        topLine: true,
        rightArrow: true,
        identify: "1",
        show: true
      }, {
        title: "公司简称",
        image: "",
        hintColor: "#303030",
        hint: "推客网络",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "2",
        show: true
      }, {
        title: "所在商圈",
        image: "",
        hintColor: "#303030",
        hint: "东煌大厦",
        bottomLine: true,
        topLine: true,
        rightArrow: true,
        identify: "3",
        show: true
      }, {
        title: "详细地址",
        image: "",
        hintColor: "#303030",
        hint: "栋/座，室",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "4",
        show: true
      }, {
        title: "公司性质",
        image: "",
        hintColor: "#303030",
        hint: "信用贷",
        bottomLine: true,
        topLine: true,
        rightArrow: true,
        identify: "5",
        show: true
      }, {
        title: "公司优势",
        image: "",
        hintColor: "#303030",
        hint: "德玛西亚万岁~~",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "6",
        show: true
      },
      {
        title: "员工管理",
        image: "",
        hintColor: "#303030",
        hint: "",
        bottomLine: true,
        topLine: true,
        rightArrow: true,
        identify: "7",
        show: true
      },
    ]
  },
  /**
   * 图片预览
   */
  showLogoImage: function() {
    wx.previewImage({
      current: this.data.logoImage, // 当前显示图片的http链接
      urls: [this.data.logoImage] // 需要预览的图片http链接列表
    })
  },
  /**
   * 图片预览
   */
  showDoorImage: function() {
    wx.previewImage({
      current: this.data.doorImage, // 当前显示图片的http链接
      urls: [this.data.doorImage] // 需要预览的图片http链接列表
    })
  },
  /**
   * 列表点击
   */
  itemTap: function(e) {
    var identify = e.currentTarget.dataset.identify;

    if (identify == 1) {
      var model = {
        title: "公司全称",
        hintTitle: '公司全称',
        hint: this.data.itemDataArray[0].hint,
        value: "",
        btnTitle: '修改',
        url: "elites/change_text",
      }
      wx.navigateTo({
        url: "/pages/user/userinfo/changeSingleText/index?name=total_name&&model=" + JSON.stringify(model),
      })
    } else if (identify == 2) {
      var model = {
        title: "公司简称",
        hintTitle: '公司简称',
        hint: this.data.itemDataArray[1].hint,
        value: "",
        btnTitle: '修改',
        url: "elites/change_text",
      }
      wx.navigateTo({
        url: "/pages/user/userinfo/changeSingleText/index?name=name&&model=" + JSON.stringify(model),
      })
    } else if (identify == 3) {
      wx.navigateTo({
        url: '/pages/customer/map/chouse?type=CompanyManager',
      })
    } else if (identify == 4) {
      var model = {
        title: "详细地址",
        hintTitle: '详细地址',
        hint: this.data.itemDataArray[3].hint,
        value: "",
        btnTitle: '修改',
        url: "elites/change_text",
      }
      wx.navigateTo({
        url: "/pages/user/userinfo/changeSingleText/index?name=address&&model=" + JSON.stringify(model),
      })
    } else if (identify == 5) {
      // 公司性质
      var model = {
        title: "公司性质",
        hintTitle: '公司性质',
        hint: this.data.itemDataArray[4].hint,
        value: "",
        btnTitle: '修改',
        url: "elites/change_text",
      }
      wx.navigateTo({
        url: "/pages/user/userinfo/changeSingleText/index?name=product&&model=" + JSON.stringify(model),
      })
    } else if (identify == 6) {
      // 公司优势
      var advantage = this.data.itemDataArray[5].hint
      wx.navigateTo({
        url: '/pages/user/userinfo/company/introduce/introduce?advantage=' + advantage,
      })
    } else if (identify == 7) {

      wx.navigateTo({
        url: 'staff/manager'
      })
    }
  },

  /**
   * 更换头像
   */
  changeLogo: function(e) {
    var that = this;
    app.util.UploadImage('elites/upload', 'companyLogo', function(data) {
      app.data.userinfo.managerCompany.picture = data.data;
      that.setData({
        logoImage: data.data
      })
    }, function(data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },
  /**
   * 更换门头照
   */
  changeCompanyPhoto: function(e) {
    var that = this;
    app.util.UploadImage('elites/upload', 'companyDoor', function(data) {
      app.data.userinfo.managerCompany.door = data.data;
      that.setData({
        doorImage: data.data
      })
    }, function(data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },
  /**
   * 重载数据
   */
  loadData: function(array) {
    var data = this.data.itemDataArray
    data[0].hint = (array.total_name ? array.total_name : '');
    data[1].hint = array.name;
    data[2].hint = array.b_name;
    data[3].hint = array.address;
    data[4].hint = array.product;
    data[5].hint = (array.advantage ? array.advantage : '');
    this.setData({
      itemDataArray: data
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (app.data.userinfo.managerCompany !== undefined) {
      var companyInfo = app.data.userinfo.managerCompany;
      this.setData({
        logoImage: companyInfo.picture,
        doorImage: companyInfo.door
      })
      this.loadData(companyInfo);
    } else {
      wx.navigateBack();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})