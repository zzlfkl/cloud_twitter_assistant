// pages/info/detail/introduce/introduce.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    textContent: '',
    textLength: 0,
    maxLength: 240,
  },

  /**
   * 表单提交
   */
  formsubmit: function(e) {
    // 网络请求...
    var params = {
      changeType: 'advantage',
      value: e.detail.value.advantage
    };
    app.util.api('elites/change_text', 'POST', params, function(data) {
      app.data.userinfo.managerCompany['advantage'] = data.data;
      wx.showToast({
        title: data.msg,
      })
      setTimeout(function() {
        wx.navigateBack()
      }, 1000)
    });
  },

  /**
   * 监听输入字数
   */
  bindTextAreaInput: function(e) {
    var value = e.detail.value

    if (value.length > this.data.maxLength) {
      value = value.substr(0, this.data.maxLength)
    }
    this.setData({
      textContent: value,
      textLength: value.length,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      textContent: options.advantage,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})