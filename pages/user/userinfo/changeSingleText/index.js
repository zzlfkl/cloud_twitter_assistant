// pages/normal/changeSingleText/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page_data: {},
    model: {
      title: "标题",
      hintTitle: '修改名',
      hint: "提示文字",
      value: "默认值",
      btnTitle: '修改',
      url: "",
    }
  },

  /**
   * 表单提交
   */
  formsubmit: function(e) {
    // 网络请求...
    var that = this;

    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) {
      if (data.success == 0) {
        console.log(data)
      }
    })

    if (that.data.page_data.model.url) {
      var params = {
        value: e.detail.value.text,
        changeType: that.data.name
      }
      app.util.api(this.data.page_data.model.url, 'POST', params, function(data) {
        app.data.userinfo.managerCompany[that.data.name] = data.data;
        wx.showToast({
          title: data.msg,
        })
        setTimeout(function() {
          wx.navigateBack()
        }, 1000)
      })
    } else {
      wx.navigateBack()
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    if (options.name == undefined) {
      wx.navigateBack()
    }
    this.setData({
      name: options.name
    })
    if (options != null && options.model != null) {
      var that = this;
      var model = JSON.parse(options.model)
      this.setData({
        model: model
      })
      this.data.page_data.model = model

      if (this.data.page_data.model.title != null) {
        wx.setNavigationBarTitle({
          title: that.data.page_data.model.title //页面标题为路由参数
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})