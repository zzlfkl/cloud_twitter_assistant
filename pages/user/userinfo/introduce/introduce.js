// pages/info/detail/introduce/introduce.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    textContent: '',
    textLength: 0,
    maxLength: 240,
  },

  /**
   * 监听输入字数
   */
  bindTextAreaInput: function(e) {
    var value = e.detail.value
    if (value.length > this.data.maxLength) {
      value = value.substr(0, this.data.maxLength)
    }
    this.setData({
      textContent: value,
      textLength: value.length,
    })
  },
  /**
   * 表单提交
   */
  formsubmit: function(e) {
    var that = this;
    var params = e.detail.value;
    app.util.api('userinfo/change_userinfo', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo.signature = data.data.signature
        wx.showToast({
          title: data.msg,
          duration: 500,
          success: function() {
            setTimeout(function() {
              wx,
              wx.navigateBack({
                delta: 1,
              })
            }, 1000)
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none',
          duration: 1000,
        })
      }

    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      signature: options.info
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})