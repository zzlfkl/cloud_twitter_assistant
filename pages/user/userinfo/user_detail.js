// pages/info/detail/user_detail.js
//获取应用实例
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    company_status: true,
    itemDataArray: [{
        title: "昵称",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "1",
        show: true
      },
      {
        title: "手机号码",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "2",
        show: true
      },
      {
        title: "所在公司",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "3",
        show: false
      },
      {
        title: "职位",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "4",
        show: false
      }, {
        title: "工作地址",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "5",
        show: false
      },
      {
        title: "个人介绍",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "6",
        show: true
      },
      {
        title: "名片二维码",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "7",
        show: true
      },
      {
        title: "我的名片",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "8",
        show: true
      },
      {
        title: "精英管理",
        image: "",
        hint: "",
        bottomLine: true,
        topLine: true,
        rightArrow: true,
        identify: "9",
        show: true
      },
      {
        title: "公司管理",
        image: "",
        hint: "",
        bottomLine: false,
        topLine: false,
        rightArrow: true,
        identify: "10",
        show: true
      }
    ],
  },

  /**
   * 更换数据
   */
  loadData: function(array) {
    this.setData({
      itemDataArray: array,
    })
  },

  /**
   * 更换数据
   */
  changeData: function(identify, value) {
    // 遍历数组赋值
    if (this.data.itemDataArray !== undefined) {
      this.data.itemDataArray.every(function(item) {
        if (identify == item.identify) {
          item.hint = value;
          return false
        }
        return true;
      })
      this.loadData(this.data.itemDataArray)
    }
  },

  /**
   * 更换显示状态
   */
  changeShow: function(identify, value) {
    // 遍历数组赋值
    if (this.data.itemDataArray !== undefined) {
      this.data.itemDataArray.every(function(item) {
        if (identify == item.identify) {
          item.show = value;
          return false
        }
        return true;
      })
      this.loadData(this.data.itemDataArray)
    }
  },

  /**
   * 显示头像大图
   */
  showAvatar: function(e) {
    wx.previewImage({
      current: this.data.url ? this.data.url : app.data.userinfo.avatar, // 当前显示图片的http链接 
      urls: [
        getApp().data.userinfo.avatar,
        this.data.url
      ] // 需要预览的图片http链接列表
    })
  },

  /**
   * 上传头像
   */
  upload: function(e) {
    var that = this;
    app.util.UploadImage('userinfo/upload', 'avatar', function(data) {
      app.data.userinfo.avatar = data.data
      that.setData({
        url: data.data
      })
    }, function(data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },

  /**
   * 列表点击
   */
  itemTap: function(e) {
    var identify = e.currentTarget.dataset.identify;
    var info = e.currentTarget.dataset.info;
    if (identify == 1) {
      wx.navigateTo({
        url: '/pages/user/userinfo/changeName/change_name?info=' + info,
      })
    } else if (identify == 2) {
      wx.navigateTo({
        url: '/pages/user/userinfo/bindPhone/bind_phone?info=' + info,
      })
    } else if (identify == 3) {
      wx.navigateTo({
        url: '/pages/user/userinfo/changeCompany/change_company?info=' + info,
      })
    } else if (identify == 4) {
      wx.navigateTo({
        url: '/pages/user/userinfo/changeJob/change_job?info=' + info,
      })
    } else if (identify == 5) {
      wx.openLocation({
        latitude: parseFloat(app.data.userinfo.company_info.lat),
        longitude: parseFloat(app.data.userinfo.company_info.lng),
      })
    } else if (identify == 6) {
      wx.navigateTo({
        url: '/pages/user/userinfo/introduce/introduce?info=' + info,
      })
    } else if (identify == 7) {
      wx.navigateTo({
        url: '/pages/user/business_card/card_qrcode',
      })
    } else if (identify == 8) {
      wx.navigateTo({
        url: '/pages/user/business_card/detail/detail',
      })
    } else if (identify == 9) { // 精英管理
      if (app.data.userinfo.user_status == 1) {
        wx.navigateTo({
          url: '/pages/user/userinfo/elite/edit_elite',
        })
      } else if (app.data.userinfo.user_status == 2) {
        wx.navigateTo({
          url: '/pages/user/pre_personal_authentication/index',
        })
      } else {
        wx.navigateTo({
          url: '/pages/user/personal_authentication/personal_authentication',
        })
      }
    } else if (identify == 10) { // 公司管理
      if (app.data.userinfo.managerCompany == undefined) {
        var params = {
          'type': 'get_auth_company'
        };
        app.util.api('elites/get_auth_company', 'GET', params, function (data) {
          if (data.success == 1) {
            app.data.userinfo.managerCompany = data.data;
            wx.navigateTo({
              url: '/pages/user/userinfo/company/edit_company',
            })
          } else {
            wx.showModal({
              title: '未认领公司',
              content: '您尚未认领公司，现在去认领吗？',
              success: function (e) {
                if (e.confirm) {
                  wx.navigateTo({
                    url: '/pages/user/auth_company/company',
                  })
                }
              }
            })
          }
        });
      } else {
        wx.navigateTo({
          url: '/pages/user/userinfo/company/edit_company',
        })
      }
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },
  /**
   * 验证用户是否有认领公司
   */
  checkAuthCompany: function() {


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    that.checkAuthCompany();
    that.changeData("1", app.data.userinfo.user_nickname)
    that.changeData("2", app.data.userinfo.mobile)
    that.changeData("6", app.data.userinfo.signature)

    that.setData({
      userinfo: app.data.userinfo
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})