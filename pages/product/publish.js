// pages/product/publish.js
var app = getApp();
var upload = require("../../utils/upload.js")
Page({
  /**
   * 页面的初始数据
   */
  data: {
    disabled:false,
    addList:[],
    status: true,
    nstatus: false,
    check_after:false,
    check_before:false,
    before_data:{},
    progress:[
      {
        src: '/img/progress/one.png',
        content: '贷款申请',
        lei:'luc',
        choose_img: false,
        choose_src:''
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        choose_img: false,
        choose_src: '',
      },
      {
        src: '/img/progress/two.png',
        content: '贷前调查',
        lei: 'luc',
        opacity: 0.4,
        is_selected:false,
        identify:'check_before',
        choose_img: true
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        choose_img: false,
        choose_src: '',
      },
      {
        src: '/img/progress/three.png',
        content: '资质审核',
        lei: 'luc',
        choose_img: false,
        choose_src: ''
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        choose_img: false,
        choose_src: '',
      },
      {
        src: '/img/progress/four.png',
        content: '实地调查',
        lei: 'luc',
        choose_img: true,
        is_selected: false,
        identify: 'check_after',
        opacity: 0.4
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        choose_img: false,
        choose_src: '',
      },
      {
        src: '/img/progress/five.png',
        content: '放款结算',
        lei: 'luc',
        choose_img: false,
        choose_src: ''
      }
    ],
    credit: [{
        name: '抵押贷 ',
        value: '0',
        checked: false
      },
      {
        name: '信用贷 ',
        value: '1',
        checked: false
      },
      {
        name: '大数据 ',
        value: '2',
        checked: false
      },
      {
        name: '信用卡 ',
        value: '3',
        checked: false
      },
      {
        name: '过桥类 ',
        value: '4',
        checked: false
      },
      {
        name: '服务类 ',
        value: '5',
        checked: false
      },
      {
        name: '其他  ',
        value: '6',
        checked: false
      },
    ],
    condition: [{
        name: ' 房产 ',
        value: '0',
        checked: false
      },
      {
        name: '车产',
        value: '1',
        checked: false
      },
      {
        name: '保单',
        value: '2',
        checked: false
      },
      {
        name: '营业执照',
        value: '3',
        checked: false
      },
      {
        name: '薪金流水',
        value: '4',
        checked: false
      },
      {
        name: '学历',
        value: '5',
        checked: false
      },
      {
        name: '其他',
        value: '6',
        checked: false
      },
    ],
    mechanism: [{
        name: '正规银行',
        value: '0',
        checked: false
      },
      {
        name: '消费金融',
        value: '1',
        checked: false
      },
      {
        name: '小贷公司',
        value: '2',
        checked: false
      },
      {
        name: '个人服务',
        value: '3',
        checked: false
      },
    ],
    array: ['等本等息', '等额本息', '等额本金', '先息后本', '后息后本', '复合还款'],
    day: ['当日', '次日', '3天', '1周', '2周', '3周', '1月'],
    days: [{
        name: '当日',
        value: '1'
      },
      {
        name: '次日',
        value: '2'
      },
      {
        name: '3天',
        value: '3'
      },
      {
        name: '1周',
        value: '7'
      },
      {
        name: '2周',
        value: '14'
      },
      {
        name: '3周',
        value: '21'
      },
      {
        name: '1月',
        value: '30'
      }
    ],
    months: [{
        name: '1个月',
        value: '1'
      },
      {
        name: '2个月',
        value: '2'
      },
      {
        name: '3个月',
        value: '3'
      },
      {
        name: '半年',
        value: '6'
      },
      {
        name: '1年',
        value: '12'
      },
      {
        name: '2年',
        value: '24'
      },
      {
        name: '3年',
        value: '36'
      },
      {
        name: '4年',
        value: '48'
      },
      {
        name: '5年',
        value: '60'
      },
      {
        name: '10年',
        value: '120'
      },
      {
        name: '20年',
        value: '240'
      },
      {
        name: '30年',
        value: '360'
      },
      {
        name: '其他',
        value: '0'
      },
    ],
    time: ['1个月', '2个月', '3个月', '半年', '1年', '2年', '3年', '4年', '5年', '10年', '20年', '30年', '其他'],
    yj: ['%', '元'],
    yjindex: 0,
    fx: ['%', '元'],
    fxindex: 0,
    dayindex: 0,
    index: 0,
    position: 6,
    winWidth: 0,
    winHeight: 0,
    currentTab: 0
  },

  /**
   * 下一步
   */
  next: function(e) {
    var that = this;
    if (typeof(that.data.pro_name) == 'undefined' || that.data.pro_name == '') {
      wx.showToast({
        title: '请输入产品名称',
        icon: 'none',
      })
      return;
    }
    if (that.data.pro_name.length > 8) {
      wx.showToast({
        title: '名称不可超过8位',
        icon: 'none'
      })
      return;
    }
    if (!that.data.credit[0].checked && !that.data.credit[1].checked && !that.data.credit[2].checked && !that.data.credit[3].checked && !that.data.credit[4].checked && !that.data.credit[5].checked && !that.data.credit[6].checked) {
      wx.showToast({
        title: '请选择授信方式',
        icon: 'none'
      })
      return;
    }
    if (!that.data.condition[0].checked && !that.data.condition[1].checked && !that.data.condition[2].checked && !that.data.condition[3].checked && !that.data.condition[4].checked && !that.data.condition[5].checked && !that.data.condition[6].checked) {
      wx.showToast({
        title: '请选择进件条件',
        icon: 'none'
      })
      return;
    }
    if (!that.data.mechanism[0].checked && !that.data.mechanism[1].checked && !that.data.mechanism[2].checked && !that.data.mechanism[3].checked) {
      wx.showToast({
        title: '请选择放款机构',
        icon: 'none'
      })
      return;
    }
    that.setData({
      status: false,
      nstatus: true,
      before_data: e.detail.value
    })
  },
  /**
   * 验证产品名称
   */
  catename: function(e) {
    this.setData({
      pro_name: e.detail.value
    })
  },
  /**
   * 验证四位数字
   */
  verification: function(e) {
    if (e.detail.value.length > 4) {
      wx.showToast({
        title: '不可超过四位',
        icon: 'none'
      })
      return;
    }
  },
  /**
   * 提交
   */
  formSubmit: function(e) {
    if (e.detail.target.dataset.identify == 'next') {
      this.next(e)
      return
    }
    
    // 禁止点击
    this.setData({
      disabled: true
    })
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) {})

    if (!app.util.checkdata(e.detail.value.max_amount) || !app.util.checkdata(e.detail.value.mon_interest)) {
      wx.showToast({
        title: '数据格式不正确',
        icon: 'none'
      })
      // 禁止重复点击
      this.setData({
        disabled: false
      })
    } else {
      var that = this;
      var params = e.detail.value;
      for (var key in this.data.before_data) {
        params[key] = this.data.before_data[key]
      }
      if (that.data.pro_id !== undefined) {
        params['pro_id'] = that.data.pro_id;
        var pro_url = 'userproduct/edit_pro';
      } else {
        var pro_url = 'userproduct/publish_pro';
      }
      params['city_code'] = app.data.globalCity.val;
      params['user_id'] = app.data.userinfo.id;
      params['img']     = that.data.addList;
      params['check_before'] = that.data.check_before;
      params['check_after'] = that.data.check_after;
     
      app.util.api(pro_url, 'POST', params, function(data) {
        console.log(data)
        if (data.success == 1) {
          wx.showToast({
            title: data.msg,
          })
          setTimeout(function() {
            wx.navigateBack()
          }, 1000)

        } else {
          // that.setData({
          //   disabled:false
          // })
          wx.showModal({
            showCancel: false,
            title: '发布失败',
            content: data.msg,
          })
        }
      })
    }
  },
  /**
   * 多选框点击事件
   */
  credit: function(e) {
    var credit = this.data.credit;
    var checkArr = e.detail.value;
    for (var i = 0; i < credit.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        credit[i].checked = true;
      } else {
        credit[i].checked = false;
      }
    }
    this.setData({
      credit: credit
    })
  },
  /**
   * 多选框点击事件
   */
  condition: function(e) {
    var condition = this.data.condition;
    var checkArr = e.detail.value;
    for (var i = 0; i < condition.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        condition[i].checked = true;
      } else {
        condition[i].checked = false;
      }
    }
    this.setData({
      condition: condition
    })
  },

  // 办理流程
  choose:function(e){
    var info = e.currentTarget.dataset.item;
    var index = e.currentTarget.dataset.index;
    info.is_selected = !info.is_selected;
    info.opacity == 1 ? (info.opacity = 0.4) : (info.opacity = 1);

    this.data.progress[index] = info;
    this.setData({
      progress: this.data.progress,
      [info.identify]:info.is_selected
    })
  },

  /**
   * 多选框点击事件
   */
  mechanism: function(e) {

    var mechanism = this.data.mechanism;
    var checkArr = e.detail.value;
    for (var i = 0; i < mechanism.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        mechanism[i].checked = true;
      } else {
        mechanism[i].checked = false;
      }
    }
    this.setData({
      mechanism: mechanism
    })
  },

  bindPickerChange1: function(e) {
    this.setData({
      position: e.detail.value
    })

  },

  bindPickerChange2: function(e) {
    this.setData({
      index: e.detail.value
    })

  },

  bindPickerChange3: function(e) {
    this.setData({
      yjindex: e.detail.value
    })

  },

  bindPickerChange4: function(e) {
    this.setData({
      fxindex: e.detail.value
    })
  },

  bindPickerChange5: function(e) {
    this.setData({
      dayindex: e.detail.value
    })

  },

  radioChange1: function(e) {
    this.setData({
      credit: e.detail.value
    })

  },
  radioChange2: function(e) {
    this.setData({
      condition: e.detail.value
    })

  },
  radioChange3: function(e) {
    this.setData({
      mechanism: e.detail.value
    })
  },
  swichNav: function(e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        })
      }
    });
    if (options.type !== undefined) {
      var params = {
        id: app.data.userinfo.id,
        pro_id: options.id
      };
      //  判断是模板发布还是编辑产品
      if (options.type == 'temp') {
        var req_url = 'userproduct/template_detail';
      } else {
        var req_url = 'userproduct/pro_detail';
        that.setData({
          isUpdate: true,
          pro_id: options.id
        })
      }
      app.util.api(req_url, 'GET', params, function(data) {
        if (data.success == 1) {
          var credit = that.data.credit;
          var condition = that.data.condition;
          var mechanism = that.data.mechanism;
          var months = that.data.months;
          var days = that.data.days;

          for (var k = 0, length = months.length; k < length; k++) {
            if (months[k].value == data.data.limit_months) {
              that.setData({
                position: k
              })
            }
          }
          for (var k = 0, length = days.length; k < length; k++) {
            if (days[k].value == data.data.open_days) {
              that.setData({
                dayindex: k
              })
            }
          }
          data.data.credit = data.data.credit.split(',');
          data.data.pro_condition = data.data.pro_condition.split(',');
          for (var k = 0, length = data.data.credit.length; k < length; k++) {
            credit[data.data.credit[k] - 1].checked = true
          }
          for (var k = 0, length = data.data.pro_condition.length; k < length; k++) {
            condition[data.data.pro_condition[k] - 1].checked = true
          }
          mechanism[data.data.mechanism - 1].checked = true

          if (data.data.cash_type == 0) {
            that.setData({
              fxindex: 0
            })
          } else {
            that.setData({
              fxindex: 1
            })
          }
          if (data.data.commission_type == 0) {
            that.setData({
              yjindex: 0
            })
          } else {
            that.setData({
              yjindex: 1
            })
          }

          // 办理流程选中状态
          if (data.data.check_before){
            that.data.progress[2].is_selected = true;
            that.data.progress[2].opacity = 1;
           
            that.setData({
              progress: that.data.progress
            })
          }else{
            that.data.progress[2].is_selected = false;
            that.data.progress[2].opacity = 0.4;

            that.setData({
              progress: that.data.progress
            })
          }

          if (data.data.check_after) {
            that.data.progress[6].is_selected = true;
            that.data.progress[6].opacity = 1;

            that.setData({
              progress: that.data.progress
            })
          } else {
            that.data.progress[6].is_selected = false;
            that.data.progress[6].opacity = 0.4;

            that.setData({
              progress: that.data.progress
            })
          }

          that.setData({
            pro_name: data.data.pro_name,
            product: data.data,
            addList: data.data.img ? data.data.img:[],
            check_before: data.data.check_before,
            check_after: data.data.check_after,
            credit: credit,
            condition: condition,
            mechanism: mechanism,
            index: data.data.pay_kind - 1
          })
        } else {
          wx.showToast({
            title: data.msg,
          })
        }
      })
    } 
  },

  // 上传图片
  upload : function (e) {
    var that = this;
    var params = {
      app: 'proImage',
      userId: app.data.userinfo.id
    }
    upload.uploadImage("product/pro_upload", params, function (data) {
      if (data.success) {
        that.setData({
          addList: that.data.addList.concat({'url':data.data})
        })
      }
    });
    
  },

  delete_pro_image:function(e){
    var index = e.currentTarget.dataset.index;
    this.data.addList.splice(index,1);
    this.setData({
      addList: this.data.addList
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})