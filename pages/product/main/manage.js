var config = require('../../../utils/config.js');
//获取应用实例
var app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    background: ['https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_banner_sign.png'],
    list_array: [{
        title: '产品中心',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_product_my.png',
        identify: '000'
      }, {
        title: '抢单中心',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_client_share.png',
        identify: '102'
      }, {
        title: '客户管理',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_client_manage.png',
        identify: '001'
      }, {
        title: '活动交流',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_activity_manage.png',
        identify: '100'
      }, {
        title: '人才市场',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_recruit_manage.png',
        identify: '101'
      }, {
        title: '我的收藏',
        image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_product_collect.png',
        identify: '002'
      }, {
        title: '',
        image: '',
        identify: '200'
      },
      {
        title: '',
        image: '',
        identify: '201'
      },
      {
        title: '',
        image: '',
        identify: '202'
      }
    ]
  },

  bannerTap: function() {
    wx.navigateTo({
      url: '/pages/webview/index?url=https://wx.ytk18.com/portal/index/command',
    })
  },

  //meau的点击事件  
  meau_item_tap: function(e) {
    var that = this;
    var value = e.currentTarget.dataset.identify;
    // 审核版本及手机号检测
    if (app.data.showStatus !== 1) {
      if (!app.util.checkauth(app)) {
        return;
      }
    }
    //管理页功能
    switch (value) {
      case '000':
        wx.navigateTo({
          url: '/pages/product/list?value=' + '1',
        })
        break;
      case '001':
        wx.navigateTo({
          url: '/pages/product/customer/catalog/catalog',
        })
        break;
      case '002':
        wx.navigateTo({
          url: '/pages/user/userinfo/collection/collection',
        })
        break;
      case '100':
        wx.navigateTo({
          url: '/pages/index/activity/manage/manage',
        })
        break;
      case '101':
        wx.navigateTo({
          url: '/pages/index/recruit/recruit/manage/manage',
        })
        break;
      case '102':
        wx.navigateTo({
          url: '/pages/client/index/index',
        })
        break;
      case '200':

        break;
      case '201':

        break;
      case '202':

        break;
    }
  },

  /**
   * 获取用户信息
   */
  loading: function(e) {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function(data) {
      if (data.success == 1) {
        app.data.userinfo = data.data;
        that.setData({
          userinfo: data.data,
        })
        //若无公司信息则获取
        if (app.data.userinfo.company_info == undefined) {
          app.util.set_company_info();
        }
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (app.data.userinfo == undefined) {
      this.loading();
    }
    // 审核版本及手机号检测
    if (app.data.showStatus !== 1) {
      this.setData({
        list_array: [{
          title: '产品中心',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_product_my.png',
          identify: '000'
        }, {
          title: '抢单中心',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_client_share.png',
          identify: '102'
        }, {
          title: '客户管理',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_client_manage.png',
          identify: '001'
        }, {
          title: '活动交流',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_activity_manage.png',
          identify: '100'
        }, {
          title: '人才市场',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_recruit_manage.png',
          identify: '101'
        }, {
          title: '我的收藏',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_product_collect.png',
          identify: '002'
        }, {
          title: '',
          image: '',
          identify: '200'
        },
        {
          title: '',
          image: '',
          identify: '201'
        },
        {
          title: '',
          image: '',
          identify: '202'
        }
        ]
      })
    }else{
      this.setData({
        list_array: [{
          title: '活动交流',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_activity_manage.png',
          identify: '100'
        }, {
          title: '人才市场',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_recruit_manage.png',
          identify: '101'
        }, {
          title: '我的收藏',
          image: 'https://lg-0nsng10k-1256175049.cos.ap-shanghai.myqcloud.com/manage_home_product_collect.png',
          identify: '002'
        }]
      })
    }
    this.setData({
      data_array: config.mange_items(),
      meau_data: {
        sysWidth: app.globalData.sysWidth * 0.94,
        itemHeight: app.globalData.sysWidth / 3,
        itemImageWidth: app.globalData.sysWidth / 12,
      },
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})