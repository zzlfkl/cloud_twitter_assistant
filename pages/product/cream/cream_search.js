// pages/product/cream/cream_search.js
var app = getApp(); 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    star:5,
    keyword: '',
    page: 1,
    enablePullup: true,
    placeholder:'搜精英',
    list: []
  },
  /**
   * 获取用户列表
   */
  user_list:function(keyword,page = 1){
    var that = this;
    var params = {
      keyword:keyword,
      page:page
    }
    app.util.api('elites/user_search','GET',params,function(data){
      console.log(data)
      that.setData({
        list: that.data.list.concat(data.data)
      })
    });
  },

  productDetail:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/detail?id='+id
    })
  },

  //失去焦点事件
  bindBlurInput:function(){

  },

  //获取焦点事件
  bindfocusInput:function(){

  },

  //确定事件
  confirm:function(){
    
  },

  /**
   *用户详情点击 
   */
  user_click:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: 'detail/elite_detail?id=' + id,
    })
  },
  /**
   * 用户删除关键词
   */
  back:function(e){
    if(e.detail.value == ''){
      this.setData({
        list: []
      })
      this.user_list('');
    }else{
      this.setData({
        keyword: e.detail.value
      })
    }
  },

  /**
   * 输入框输入事件
   */
  bindTextInput :function(e){
    this.setData({
      keyword:e.detail.value,
      list:[]
    })
    this.user_list(this.data.keyword);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
    this.user_list(this.data.keyword);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      page:this.data.page+1
    })
    this.user_list(this.data.keyword,this.data.page)
  }
})