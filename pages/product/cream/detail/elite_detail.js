// pages/tools/elite/detail/elite_detail.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    homeHide:true,
    score_stars: 5, 
    // 假的产品列表 
    pro_itemDataArray: null, 
  },
  /**
   * 主营产品详情
   */
  productTap : function (e) {
    if (!app.util.checkauth(app)) {
      return;
    }
    var id = parseInt(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/index/detail?id=' + id,
    })
  },
  /**
   * 拨打电话
   */
  phoneCall:function(){
    if (this.data.userInfo.user_type == 2) {
      wx.makePhoneCall({
        phoneNumber: this.data.eliteInfo.mobile
      })
    } else {
      wx.showModal({
        title: '温馨提示',
        content: '会员专属功能，请先充值成为会员',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/user/vip/vip/vip_center',
            })
          }
        }
      })
    }
  },
  /**
   * 公司详情网络请求
   */
  company_detail:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/product/company/detail/company_detail?id='+id,
    })
  },

  /**
   * 精英详情
   */
  elitesDetail: function (userId){
    var that = this;
    var params = {
      'user_id': userId
    };
    app.util.api('elites/elites_detail', 'GET', params, function (data) {
      console.log(data)
      data.data.user_nickname = data.data.user_nickname.substring(0, 12);
      for (var k = 0, length = data.data.product.length; k < length; k++) {
        if (data.data.product[k].limit_months < 12) {
          data.data.product[k].limit_months = data.data.product[k].limit_months + '月';
        } else {
          data.data.product[k].limit_months = data.data.product[k].limit_months / 12 + '年';
        }
        if (data.data.product[k].open_days == 1) {
          data.data.product[k].open_days = '当日';
        } else if (data.data.product[k].open_days == 2) {
          data.data.product[k].open_days = '次日';
        } else if (data.data.product[k].open_days == 3) {
          data.data.product[k].open_days = '3天';
        } else if (data.data.product[k].open_days == 7) {
          data.data.product[k].open_days = '1周';
        } else if (data.data.product[k].open_days == 14) {
          data.data.product[k].open_days = '2周';
        } else if (data.data.product[k].open_days == 21) {
          data.data.product[k].open_days = '3周';
        } else if (data.data.product[k].open_days == 30) {
          data.data.product[k].open_days = '1月';
        } else {
          data.data.product[k].open_days = data.data.product[k].open_days + '日';
        }
      }
      that.setData({
        eliteInfo: data.data,
        star:data.data.star,
        pro_itemDataArray: data.data.product,
      })
    })
  },

  /**
   * 用户收藏/取消收藏操作
   */
  star:function(e){
    var that = this;
    var eliteId = e.currentTarget.dataset.id;
    var params = {
      eliteId: eliteId
    };
    app.util.api('elites/elite_collection','POST',params,function(data){
      console.log(data)
      that.setData({
        star:data.data
      })
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // 来源分享
    if(options.type !== undefined){
      that.setData({
        homeHide: false
      })
    }
    if (app.data.userinfo == undefined) {
      app.util.login(function (data) {
        if (data.success == 1) {
          wx.setStorageSync('token', data.other);
          app.data.userinfo = data.data;
          that.setData({
            userInfo: data.data
          })
          that.elitesDetail(options.id);
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
        }
      })
    } else {
      that.setData({
        userInfo: app.data.userinfo
      })
      that.elitesDetail(options.id);   
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.elitesDetail(this.data.eliteInfo.user_id); 
    wx.stopPullDownRefresh();  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '精英-' + this.data.eliteInfo.user_nickname,
      path: '/pages/product/cream/detail/elite_detail?type=share&id=' + this.data.eliteInfo.user_id,
    }
  }
})