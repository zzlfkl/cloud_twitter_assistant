var app = getApp();
var md5 = require('../../../utils/md5.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bk: 0,
    enoughBalance: true
  },

  showPopup: function () { },

  hiddenPopup: function () { },

  /**
   * 确认支付
   */
  confirm: function(e) {
    var this_ = this;
    var discount = this_.data.discount;
    if (discount >= this_.data.balance) {
      this_.setData({
        enoughBalance: false
      })
    } else {
      this_.setData({
        enoughBalance: true
      })
    }
    this_.setData({
      cash: discount,
      fee_id: this_.data.post,
      pro_id:this_.data.pro_id
    })
    this_.popViewPay.showPopup();
  },

  // 开始支付
  payActivity: function (e) {
    var this_ = this;
    var pwd = e.detail.pwd;
    wx.showLoading({
      title: '支付中...',
    });
    var params = {
      body: this_.data.name,
      cash: this_.data.discount,
      fee_id: this_.data.post,
      pro_id: this_.data.pro_id,
      pay_pass:pwd
    };
    app.util.api('pay/balance_pay', 'POST', params, function (data) {
      wx.hideLoading();
      if (data.success) {
        wx.showToast({
          title: '购买成功',
        });
        setTimeout(function () {
          this_.popViewPay.hidePopup();
          wx.navigateBack({})
        }, 800);
      } else if (data.status == 4005) {
        wx.showModal({
          title: '密码错误',
          content: '支付密码不正确',
          confirmText: '找回密码',
          cancelText: '重新输入',
          success: function (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/user/userinfo/bindPhone/bind_phone?send_type=reset_pay_pass',
              })
            } else {
              setTimeout(function () {
                this_.popViewPay.setData({
                  pwd: ''
                })
              }, 500)
            }
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        });
        this_.setData({
          buyIng: true
        });
        this_.popViewPay.setData({
          pwd: ''
        });
        setTimeout(function () {
          wx.hideLoading();
          this_.popViewPay.hidePopup();
        }, 800);
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      pro_id: options.id
    })
    var params = {
      'type': 'list'
    };
    app.util.api('fee/fee_type_list', 'GET', params, function(data) {
      if (data.success == 1) {
        that.setData({
          list: data.data,
          name: data.data['0'].type_name
        })
        that.api(data.data['0'].id);
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 收费明细
   */
  change: function(e) {
    var id = parseInt(e.currentTarget.dataset.index);
    var name = e.currentTarget.dataset.name;
    console.log(name);
    this.setData({
      name:name
    })
    this.api(id);
  },

  /**
   * 选择开通时间
   */
  go: function(e) {
    var that = this;
    var id = parseInt(e.currentTarget.dataset.id);
    var count = parseInt(e.currentTarget.dataset.count);
    var price = parseInt(e.currentTarget.dataset.price);
    that.choose(id, count, price)
  },
  /**
   * 选择开通时间
   */
  choose: function(id, count, price) {
    var that = this;
    that.setData({
      post: id,
      discount: count,
      price: price - count,
    })
  },
  /**
   * 网络请求收费明细
   */
  api: function(id) {
    var that = this;
    var id = id;
    that.setData({
      bk: id
    })

    var params = {
      'id': id
    };
    params['user_id'] = getApp().data.userinfo.id

    app.util.api('fee/fee_list', 'GET', params, function(data) {
      if (data.success == 1) {
        that.setData({
          fee_list: data.data
        })
        that.choose(data.data['0'].id, data.data['0'].fee_discount, data.data['0'].price);
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    });
  },

/**
 * 获取用户信息
 */
  refresh: function (e) {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function (data) {
      if (data.success == 1) {
        app.data.userinfo = data.data;
        that.setData({
          userinfo: data.data,
          balance: data.data.balance
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.popViewPay = this.selectComponent("#popview_pay");
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.refresh();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})