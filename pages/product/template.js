// pages/product/template.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.temp();
  },
  /**
   * 网络请求
   */
  temp: function (e) {
    var that = this;
    var params = {
      'type': 'temp'
    };
    app.util.api('userproduct/pro_template','GET',params,function(data){
      if (data.success == 1) {
        that.setData({
          temp: data.data
        })
      } else {
        that.setData({
          temp: ''
        })
        wx.showToast({
          title: '暂无模板',
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  /**
   * 模板选择
   */
  template: function (e) {
    var index = parseInt(e.currentTarget.dataset.index);
    wx.navigateTo({
      url: 'publish?id=' + index + '&type=temp',
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})