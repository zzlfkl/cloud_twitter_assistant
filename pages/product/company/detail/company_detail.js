var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    homeHide:true,
    // 0 产品列表 1 员工列表
    showType: 0,
    score_stars: 4,
    // 假的产品列表 
    pro_itemDataArray: null,
    // 员工列表
    staff_itemDataArray: null,
  },

  /**
   * 公司报错
   */
  wrong: function (e) {
    var bid = e.currentTarget.dataset.bid;
    var cid = e.currentTarget.dataset.cid;
    var name = e.currentTarget.dataset.name;
    var address = e.currentTarget.dataset.address;
    var bname = e.currentTarget.dataset.bname;
    wx.navigateTo({
      url: '/pages/customer/map/wrong?bid=' + bid + '&&cid=' + cid + '&&name=' + name + '&&address=' + address + '&&b_name=' + bname,
    })
  },

  /**
   * 选择产品列表
   */
  chooseProductList: function() {
    this.setData({
      showType: 0,
    })
  },

  /**
   * 选择员工列表
   */
  chooseStaffList: function() {
    this.setData({
      showType: 1,
    })
  },

  /**
   * 精英详情
   */
  staffDetail: function(e) {
    var id = parseInt(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/product/cream/detail/elite_detail?id=' + id,
    })
  },

  /**
   * 获取公司详情
   */
  companyDetail: function(companyId) {
    var that = this;
    var params = {
      company_id: companyId
    }
    app.util.api('company_auth/company_detail', 'GET', params, function(data) {
      console.log(data)
      if (data.status == 200) {
        if (data.data.auth_status) {
          for (var k = 0, length = data.data.product.length; k < length; k++) {
            if (data.data.product[k].limit_months < 12) {
              data.data.product[k].limit_months = data.data.product[k].limit_months + '月';
            } else {
              data.data.product[k].limit_months = data.data.product[k].limit_months / 12 + '年';
            }
            if (data.data.product[k].open_days == 1) {
              data.data.product[k].open_days = '当日';
            } else if (data.data.product[k].open_days == 2) {
              data.data.product[k].open_days = '次日';
            } else if (data.data.product[k].open_days == 3) {
              data.data.product[k].open_days = '3天';
            } else if (data.data.product[k].open_days == 7) {
              data.data.product[k].open_days = '1周';
            } else if (data.data.product[k].open_days == 14) {
              data.data.product[k].open_days = '2周';
            } else if (data.data.product[k].open_days == 21) {
              data.data.product[k].open_days = '3周';
            } else if (data.data.product[k].open_days == 30) {
              data.data.product[k].open_days = '1月';
            } else {
              data.data.product[k].open_days = data.data.product[k].open_days + '日';
            }
          }
          that.setData({
            companyInfo: data.data,
            star: data.data.star,
            pro_itemDataArray: data.data.product,
            staff_itemDataArray: data.data.staff
          })
        } else {
          that.setData({
            companyInfo: data.data,
            star: data.data.star,
          })
        }
      }
    })
  },
  /**
   * 用户收藏/取消收藏操作 
   */
  star: function(e) {
    var that = this;
    var companyId = e.currentTarget.dataset.id;
    var params = {
      companyId: companyId
    };
    app.util.api('elites/company_collection', 'POST', params, function(data) {
      that.setData({
        star: data.data
      })
    });
  },

  /**
   * 主营产品详情
   */
  productTap: function(e) {
    if (!app.util.checkauth(app)) {
      return;
    }
    var id = parseInt(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/index/detail?id=' + id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if(options.type !== undefined){
      that.setData({
        homeHide: false
      })
    }
    if (app.data.userinfo == undefined) {
      app.util.login(function(data) {
        if (data.success == 1) {
          wx.setStorageSync('token', data.other);
          app.data.userinfo = data.data;
          that.setData({
            userInfo: data.data,
          })
          that.companyDetail(options.id);
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none',
          })
        }
      })
    } else {
      wx.getSystemInfo({
        success: function(res) {
          that.setData({
            winWidth: res.windowWidth,
            winHeight: res.windowHeight, 
            userInfo: app.data.userinfo
          })
          that.companyDetail(options.id);
        }
      });
    }
  },
  /**
   * 拨打电话
   */
  phoneCall: function() {
    if (this.data.userInfo.user_type == 2) {
      if (this.data.companyInfo.mobile !== undefined) {
        wx.makePhoneCall({
          phoneNumber: this.data.companyInfo.mobile,
        })
      } else {
        wx.showToast({
          title: '该公司暂未被认领',
          icon: 'none'
        })
      }
    } else {
      wx.showModal({
        title: '温馨提示',
        content: '会员专属功能，请先充值成为会员',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/user/vip/vip/vip_center',
            })
          }
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.companyDetail(this.data.companyInfo.company_id);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: '公司-' + this.data.companyInfo.name,
      path: '/pages/product/company/detail/company_detail?type=share&id=' + this.data.companyInfo.id,
      success: function(res) {
        console.log(res)
      },
      fail: function(res) {
        console.log(res + 'error')
        // 转发失败
      }
    }
  }
})