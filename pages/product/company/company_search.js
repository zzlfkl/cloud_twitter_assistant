// pages/product/company/company_search.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    keyword:'',
    placeholder: '搜公司',
  },
  /**
   * 确定公司名称
   */
  confirm: function (e) {
    this.setData({
      list:[]
    })
    this.company_search(this.data.keyword);
  },
  /**
   * 选择公司事件
   */
  change_company: function (e) {
    var companyId = e.currentTarget.dataset.id;
    if(this.data.isAuth){
      var company = e.currentTarget.dataset.name;
      var companyId = e.currentTarget.dataset.id;
      app.data.company_choose = {
        name: company,
        companyId: companyId
      };
      wx.navigateBack({
        delta: 1
      })
    }else{
      wx.navigateTo({
        url: 'detail/company_detail?id='+companyId,
      })
    }
    
  },
  /**
   * 监听输入字数
   */
  bindTextInput: function (e) {
    var keyword = e.detail.value;
    if(keyword == ''){
      this.setData({
        list:[]
      })
      this.company_search('');
    }
    this.setData({
      keyword: keyword,
      page:1
    })
  },
  /**
   * 公司搜索
   */
  company_search(keyword) {
    var that = this;
    var params = {
      'keyword': keyword,
      'page': that.data.page
    }
    app.util.api('map/company_search', 'GET', params, function (data) {
      console.log(data)
      if (data.success == 1) { 
        that.setData({
          list: that.data.list.concat(data.data)
        })
      } 
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.isAuth !== undefined){
      this.setData({
        isAuth: options.isAuth
      })
    }
    this.setData({
      list: [],
    })
    this.company_search(this.data.keyword);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      page: this.data.page + 1
    })
    this.company_search(this.data.keyword)
  }
})