var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swichIndex:0,
    tab_list:[
      {
        name:'线上展示',
        index:0
      },
      {
        name: '等待发布',
        index:1
      },
      {
        name: '产品收藏',
        index:2
      }
    ]
  },

  tab_click:function(e){
    var index = e.currentTarget.dataset.index;
    switch (index){
      case 0:
        var value = 1;
      break;
      case 1:
        var value = 0;
        break;
      case 2:
        var value = 2;
        break;
    }
    this.setData({
      swichIndex: index,
      value:value
    });
    this.list(value);
  },

  /**
   * 产品发布
   */
  publish: function() {
    wx.showActionSheet({
      itemList: ['自定义发布', '使用模板发布'],
      success: function(res) {
        if (res.tapIndex == 0) {
          wx.navigateTo({
            url: '/pages/product/publish',
          })
        } else {
          wx.navigateTo({
            url: '/pages/product/template',
          })
        }

      },
      fail: function(res) {

      }
    })
  },

  /**
   * 列表获取
   */
  list: function(value) {
    var that = this;
    var params = {
      'value': value
    };
    params.page = that.data.page;
    app.util.api('userinfo/pro_me', 'GET', params, function(data) {
      if (data.success == 1 && data.data.length !== 0) {
        for (var k = 0, length = data.data.length; k < length; k++) {
          if (data.data[k].limit_months < 12) {
            data.data[k].limit_months = data.data[k].limit_months + '月';
          } else {
            data.data[k].limit_months = data.data[k].limit_months / 12 + '年';
          }
          if (data.data[k].open_days == 1) {
            data.data[k].open_days = '当日';
          } else if (data.data[k].open_days == 2) {
            data.data[k].open_days = '次日';
          } else if (data.data[k].open_days == 3) {
            data.data[k].open_days = '3天';
          } else if (data.data[k].open_days == 7) {
            data.data[k].open_days = '1周';
          } else if (data.data[k].open_days == 14) {
            data.data[k].open_days = '2周';
          } else if (data.data[k].open_days == 21) {
            data.data[k].open_days = '3周';
          } else if (data.data[k].open_days == 30) {
            data.data[k].open_days = '1月';
          } else {
            data.data[k].open_days = data.data[k].open_days + '日';
          }
        }
        that.setData({
          pro_list: data.data,
          show: true
        })
      } else {
        that.setData({
          pro_list: [],
          show: false
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      value: options.value
    })
  },
  /**
   * 详情
   */
  productTap: function(e) {
    var status = e.currentTarget.dataset.status;
    var pro_id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/detail?id=' + pro_id + '&&status=' + status,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.list(this.data.value);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.list(this.data.value);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})