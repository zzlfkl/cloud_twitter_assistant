// pages/tools/record/record/record.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    subType: false,
    commission_type: 0,
    repeat: false,
    house: true, 
    car: true,
    main: true,
    gold: true,
    card: true,
    mortgage: true,
    credit: true,
    credit_card: true,
    net: true,
    param: {
      name: {
        need: true,
        title: '真实姓名：',
        placeholder: '请输入不超过4个汉字',
        unit: '',
        bottom: true,
        name: "name",
        show: true,
        genre: 'text',
        length: 4
      },
      phone: {
        need: false,
        title: '手机号码：',
        placeholder: '请输入11位数字',
        unit: '',
        bottom: true,
        name: "phone",
        show: false,
        genre: 'number',
        length: 11
      },
      money: {
        need: true,
        title: '需求金额：',
        placeholder: '请输入3位以内数字',
        unit: '万',
        bottom: true,
        name: "money",
        show: false,
        genre: 'number',
        length: 3
      },
      commission: {
        need: false,
        title: '服务佣金：',
        placeholder: '请输入5位以内数字',
        unit: '%',
        bottom: true,
        name: "commission",
        show: false,
        genre: 'digit',
        length: 6
      },
      birthday: {
        need: false,
        title: '出生日期：',
        placeholder: '请选择客户的出生日期',
        unit: '',
        bottom: true,
        name: "birthday",
        show: false,
        value: ''
      },
      marriage: {
        need: false,
        name: 'marriage',
        title: '婚姻状况：',
        bottom: true,
        multiSelect: false,
        show: false,
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '未婚',
          tag_identify: 'marriage',
          selected: false,
          tag_number: 0
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '已婚',
          tag_identify: 'marriage',
          selected: false,
          tag_number: 1
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '离异',
          tag_identify: 'marriage',
          selected: false,
          tag_number: 2
        }],
      },

      business_license: {
        need: false,
        title: '营业执照：',
        bottom: false,
        multiSelect: false,
        name: "business_license",
        show: false,
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '无',
          tag_identify: 'business_license',
          tag_number: 0,
          selected: false
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '有',
          tag_identify: 'business_license',
          tag_number: 1,
          selected: false
        }],
      },
      company_name: {
        need: false,
        title: '',
        sub_title: '公司名称：',
        placeholder: '请输入公司名称',
        unit: '有限公司',
        bottom: true,
        name: "company_name",
        show: false,
        genre: 'text',
        changeWidth: '40rpx;',
        length: 18
      },
      business_license_name: {
        need: false,
        title: '',
        sub_title: '执照名称：',
        placeholder: '请输入执照名称',
        unit: '有限公司',
        bottom: true,
        name: "business_license_name",
        show: false,
        genre: 'text',
        changeWidth: '40rpx;',
        length: 18
      },

      // 社保公积
      protect: {
        need: false,
        title: '社保公积：',
        multiSelect: false,
        bottom: true,
        show: false,
        name: 'protect',
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '无',
          tag_identify: 'protect',
          tag_number: 0,
          selected: false
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '有',
          tag_identify: 'protect',
          tag_number: 1,
          selected: false
        }],
      },
      cash_base: {
        need: false,
        title: '',
        sub_title: '缴存基数：',
        placeholder: '请输入5位以内数字',
        unit: '元',
        bottom: false,
        name: "cash_base",
        show: false,
        genre: 'number',
        changeWidth: '40rpx;',
        length: 5
      },
      month_total: {
        need: false,
        title: '',
        sub_title: '月总缴存：',
        placeholder: '请输入5位以内数字',
        unit: '元',
        bottom: true,
        name: "month_total",
        show: false,
        genre: 'number',
        changeWidth: '40rpx;',
        length: 5
      },
      //有无房产
      house: {
        need: false,
        title: '有无房产：',
        multiSelect: false,
        bottom: true,
        show: false,
        name: 'house',
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '无',
          tag_identify: 'house',
          tag_number: 0,
          selected: false
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '有',
          tag_identify: 'house',
          tag_number: 1,
          selected: false
        }],
      },
      monthly_supply: {
        need: false,
        title: '',
        sub_title: '月供金额：',
        placeholder: '请输入5位以内数字',
        unit: '元',
        bottom: false,
        name: "monthly_supply",
        show: false,
        genre: 'number',
        changeWidth: '40rpx;',
        length: 5
      },
      bank: {
        need: false,
        title: '',
        sub_title: '按揭银行：',
        placeholder: '请输入12个以内汉字',
        unit: '支行',
        bottom: false,
        name: "bank",
        show: false,
        changeWidth: '40rpx;',
        length: 12
      },
      use_kind: {
        need: false,
        title: '',
        sub_title: '规划用途：',
        bottom: false,
        multiSelect: false,
        name: 'use_kind',
        show: false,
        changeWidth: '40rpx;',
        choose_array: [{
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '住宅',
            tag_identify: 'use_kind',
            tag_number: 0,
            selected: false
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '商业',
            tag_identify: 'use_kind',
            tag_number: 1,
            selected: false
          },
          {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '办公',
            tag_identify: 'use_kind',
            tag_number: 2,
            selected: false
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '其它',
            tag_identify: 'use_kind',
            tag_number: 3,
            selected: false
          }
        ],
      },
      land_nature: {
        need: false,
        title: '',
        sub_title: '土地性质：',
        bottom: true,
        multiSelect: false,
        name: 'land_nature',
        show: false,
        changeWidth: '40rpx;',
        choose_array: [{
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '出让',
            tag_identify: 'land_nature',
            tag_number: 0,
            selected: false
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '划拨',
            tag_identify: 'land_nature',
            tag_number: 1,
            selected: false
          },
          {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '私有',
            tag_identify: 'land_nature',
            tag_number: 2,
            selected: false
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '其它',
            tag_identify: 'land_nature',
            tag_number: 3,
            selected: false
          }
        ],
      },

      //有无车产
      car: {
        need: false,
        title: '有无车产：',
        multiSelect: false,
        bottom: false,
        show: false,
        name: 'car',
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '无',
          tag_identify: 'car',
          tag_number: 0,
          selected: false
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '有',
          tag_identify: 'car',
          tag_number: 1,
          selected: false
        }],
      },
      car_status: {
        need: false,
        title: '',
        sub_title: '',
        bottom: true,
        multiSelect: false,
        name: 'car_status',
        changeWidth: '40rpx;',
        show: false,
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '全款车',
          tag_identify: 'car_status',
          tag_number: 0,
          selected: false
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '按揭车',
          tag_identify: 'car_status',
          tag_number: 1,
          selected: false
        }]
      },
      //其他资产
      other_property: {
        need: false,
        title: '其他资产：',
        bottom: true,
        multiSelect: true,
        name: 'other_property',
        show: false,
        choose_array: [{
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '房产',
            tag_identify: 'other_property',
            tag_number: 0,
            selected: false
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '车产',
            tag_identify: 'other_property',
            tag_number: 1,
            selected: false
          },
          {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '经营',
            tag_identify: 'other_property',
            tag_number: 2,
            selected: false
          },
          {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '保单',
            tag_identify: 'other_property',
            tag_number: 3,
            selected: false
          },
          {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '学历',
            tag_identify: 'other_property',
            tag_number: 4,
            selected: false
          },
          {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '其它',
            tag_identify: 'other_property',
            tag_number: 5,
            selected: false
          }
        ],
      },

      //个人负债
      personal_debt: {
        need: false,
        title: '负债详情：',
        bottom: false,
        multiSelect: true,
        name: 'personal_debt',
        show: false,
        choose_array: [{
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '按揭贷',
            tag_identify: 'personal_debt',
            child_name: 'mortgage',
            tag_number: 0,
            selected: false
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '信用贷',
            tag_identify: 'personal_debt',
            child_name: 'credit',
            tag_number: 1,
            selected: false
          }, {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '信用卡',
            child_name: 'credit_card',
            tag_identify: 'personal_debt',
            tag_number: 2,
            selected: false
          },
          {
            tag_image_path: '/img/customer/setup_unselected.png',
            tag_title: '小额网贷',
            child_name: 'net',
            tag_identify: 'personal_debt',
            tag_number: 3,
            selected: false
          }
        ],
      },
      //按揭贷
      mortgage: {
        need: false,
        title: '',
        sub_title: '按揭贷：',
        placeholder: '请输入4位以内数字',
        unit: '万',
        bottom: false,
        name: "mortgage",
        show: false,
        changeWidth: '40rpx;',
        genre: 'digit',
        length: 4
      },
      //信用贷
      credit: {
        need: false,
        title: '',
        sub_title: '信用贷：',
        placeholder: '请输入4位以内数字',
        unit: '万',
        bottom: false,
        name: "credit",
        show: false,
        changeWidth: '40rpx;',
        genre: 'digit',
        length: 4
      },
      //信用卡
      credit_card: {
        need: false,
        title: '',
        sub_title: '信用卡：',
        placeholder: '请输入4位以内数字',
        unit: '万',
        bottom: false,
        name: "credit_card",
        show: false,
        changeWidth: '40rpx;',
        genre: 'digit',
        length: 4
      },
      //小额网贷
      net: {
        need: false,
        title: '',
        sub_title: '小额网贷：',
        placeholder: '请输入4位以内数字',
        unit: '万',
        bottom: false,
        name: "net",
        show: false,
        changeWidth: '40rpx;',
        genre: 'digit',
        length: 4
      },

      // 是否共有
      is_common: {
        need: false,
        title: '是否共有：',
        bottom: false,
        multiSelect: false,
        name: 'is_common',
        show: false,
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '是',
          tag_identify: 'is_common',
          tag_number: 0,
          selected: false
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '否',
          tag_identify: 'is_common',
          tag_number: 1,
          selected: false
        }]
      },
      // 是否共签
      is_sign: {
        need: false,
        title: '是否共签：',
        bottom: false,
        multiSelect: false,
        name: 'is_sign',
        show: false,
        choose_array: [{
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '是',
          tag_identify: 'is_sign',
          tag_number: 0,
          selected: false
        }, {
          tag_image_path: '/img/customer/setup_unselected.png',
          tag_title: '否',
          tag_identify: 'is_sign',
          tag_number: 1,
          selected: false
        }]
      },
      // 信息反馈
      feedback: {
        need: false,
        title: '信息反馈：',
        sub_title: '',
        placeholder: '请输入信息反馈',
        unit: '',
        bottom: true,
        name: "feedback",
        show: false
      }
    }
  },

  /**
   * 输入框输入事件(输入检测验证)
   */
  inputClick: function(e) {
    var style = e.currentTarget.dataset.type;
    var value = e.detail.value;

    //2-4位汉字检测
    var nameReg = /^[\u4e00-\u9fa5]{2,4}$/;
    //手机号检测
    var phoneReg = /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/;

    if (style == 'name') {
      if (!nameReg.test(value)) {
        wx.showToast({
          title: '请输入2-4位汉字',
          icon: 'none'
        })
      }
    }

    if (style == 'phone' && value.length == 11) {
      if (!phoneReg.test(value)) {
        wx.showToast({
          title: '请输入正确的手机号',
          icon: 'none'
        })
      }
    }
  },

  inputBlur: function(e) {
    var style = e.currentTarget.dataset.type;
    var value = e.detail.value;
    //手机号检测
    var phoneReg = /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/;
    if (style == 'phone') {
      if (!phoneReg.test(value)) {
        wx.showToast({
          title: '请输入正确的手机号',
          icon: 'none'
        })
      }
    }
  },

  //佣金单位选择
  changeUnit: function() {
    if (this.data.param.commission.unit == '%') {
      this.data.param.commission.unit = '元';
    } else {
      this.data.param.commission.unit = '%';
    }
    this.setData({
      param: this.data.param,
      commission_type: (this.data.param.commission.unit == '%' ? 0 : 1)
    })
  },

  //日期选择
  date: function(e) {
    this.data.param.birthday.value = e.detail.value;
    this.setData({
      param: this.data.param
    })
  },

  //判断是否在数组中
  InArray: function(value, arr) {
    for (var i = 0; i < arr.length; i++) {
      if (value === arr[i]) {
        return true;
      }
    }
    return false;
  },

  // 表单提交
  formsubmit: function(e) {
    var that = this;
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
    //将json对象转为数组
    var param = this.data.param;
    var list = [];
    var box = [];
    for (var i in param) {
      list.push(param[i]);
    }
    //验证数据格式
    if (e.detail.value.name.length > 4) {
      wx.showToast({
        title: '真实姓名过长！',
        icon: 'none'
      })
      return;
    }
    if (e.detail.value.money > 999) {
      wx.showToast({
        title: '需求金额超过最大限制！',
        icon: 'none'
      })
      return;
    }
    var reg = /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/;
    if (!reg.test(e.detail.value.phone)) {
      // 不合法数据
      wx.showToast({
        title: '手机号格式错误',
        icon: 'none'
      })
      return;
    }

    if (e.detail.value.bank.length > 12) {
      wx.showToast({
        title: '按揭银行名称过长（限12字以内）',
        icon: 'none'
      })
      return;
    }

    //获取单选按钮表单值
    var arr = [];
    for (var i = 0; i < list.length; i++) {

      //必填提示
      if (list[i].need) {
        var itemName = list[i].name;
        if (e.detail.value[itemName] == '') {
          wx.showToast({
            title: '请输入' + list[i].title.substring(0, list[i].title.length - 1),
            icon: 'none'
          })
          return;
        }
      }

      // 单选
      if (list[i].choose_array !== undefined && list[i].show == true && !list[i].multiSelect) {
        for (var j = 0; j < list[i].choose_array.length; j++) {
          if (list[i].choose_array[j].selected) {
            arr.push({
              kind: list[i].choose_array[j].tag_number,
              name: list[i].choose_array[j].tag_identify,
            })
          }
        }
      }
      // 多选
      if (list[i].choose_array !== undefined && list[i].show == true && list[i].multiSelect) {
        for (var j = 0; j < list[i].choose_array.length; j++) {
          if (list[i].choose_array[j].selected) {
            box.push({
              kind: list[i].choose_array[j].tag_number,
              name: list[i].choose_array[j].tag_identify,
            })
          }
        }
      }
    }

    // 组合多选框数据
    var arrs = [],
      name = [],
      value = [];
    var vv = '';
    for (var k = 0; k < box.length; k++) {
      name[k] = box[k].name;
    }
    name = app.util.uniqueArr(name);
    for (var l = 0; l < name.length; l++) {
      value.push({
        name: name[l],
        kind: []
      })
    }
    for (var k = 0; k < box.length; k++) {
      for (var l = 0; l < value.length; l++) {
        if (box[k].name == value[l].name) {
          value[l].kind += box[k].kind + ',';
        }
      }
    }
    for (var l = 0; l < value.length; l++) {
      arr.push({
        name: value[l].name,
        kind: value[l].kind.substr(0, value[l].kind.length - 1)
      })
    }

    if (that.data.subType) {
      var url = "record/edit_post";
      //组合params，进行表单提交
      var params = {
        value: e.detail.value,
        checkBox: arr,
        proId: this.data.proId,
        customer_order_id: that.data.customer_order_id
      }
    } else {
      var url = "record/add_post"
      //组合params，进行表单提交
      var params = {
        value: e.detail.value,
        checkBox: arr,
        proId: this.data.proId
      }
    }
    params.value.commission_type = that.data.commission_type;

    //避免按钮重复提交
    that.setData({
      repeat: true
    })

    app.util.api(url, 'POST', params, function(data) {

      if (data.success == 1) {
        wx.showToast({
          title: data.msg,
          duration: 1000,
          success: function() {
            setTimeout(function() {
              wx.navigateBack();
            }, 1000)
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
        that.setData({
          repeat: false
        })
      }
    })
  },

  // 其它贷款选择
  chooseInputTag: function(e) {
    var item = e.currentTarget.dataset.item
    var items_strings = ['other_loan', 'other_loan_car', 'other_loan_bank', 'other_loan_petty']
    var item_string;
    var model;
    var data = this.data.param;

    for (var index in items_strings) {
      item_string = items_strings[index];
      model = this.data.param[item_string];

      if (!model.multiSelect) {
        if (item.tag_number != index) {
          model.item.tag_image_path = '/img/customer/setup_unselected.png'
          model.item.selected = false;
        }
      }
    }

    item_string = item.tag_identify
    model = this.data.param[item_string]
    model.item.selected = !model.item.selected
    if (model.item.selected) {
      model.item.tag_image_path = '/img/customer/choose_seleted.png'
    } else {
      model.item.tag_image_path = '/img/customer/setup_unselected.png'
    }

    data[item_string] = model;
    this.setData({
      param: data
    })
  },

  // 选择某项
  chooseItemTag: function(e) {
    var item = e.currentTarget.dataset.item
    var identify = item.tag_identify
    var temp_model = this.data.param[identify];
    var data = this.data.param;
    temp_model.choose_array = this.selectItemTag(temp_model.choose_array, item.tag_number, temp_model.multiSelect);
    data[identify] = temp_model;
    this.setData({
      param: data
    })

    //有无房产
    if (data.house.choose_array[0].selected) {
      this.setData({
        house: true
      })
    } else {
      this.setData({
        house: false
      })
    }
    if (data.car.choose_array[0].selected) {
      this.setData({
        car: true
      })
    } else {
      this.setData({
        car: false
      })
    }

    //营业执照（无）点击
    if (data.business_license.choose_array[0].selected) {
      this.setData({
        card: true
      })
    } else {
      this.setData({
        card: false
      })
    }

    //社保公积点击
    if (data.protect.choose_array[0].selected) {
      this.setData({
        gold: true
      })
    } else {
      this.setData({
        gold: false
      })
    }

    //个人负债点击
    for (var i = 0; i < data.personal_debt.choose_array.length; i++) {
      if (data.personal_debt.choose_array[i].selected) {
        this.setData({
          [data.personal_debt.choose_array[i].child_name]: false
        })
      } else {
        this.setData({
          [data.personal_debt.choose_array[i].child_name]: true
        })
      }
    }
  },

  // 选择某一类
  selectItemTag: function(array, idx, multiSelect) {
    var model;
    for (var index in array) {
      if (!multiSelect) {
        model = array[index]
        if (idx != index) {
          model.tag_image_path = '/img/customer/setup_unselected.png'
          model.selected = false;
        }
      }
    }
    var temp_model = array[idx]
    temp_model.selected = !temp_model.selected
    if (temp_model.selected) {
      temp_model.tag_image_path = '/img/customer/choose_seleted.png'
    } else {
      temp_model.tag_image_path = '/img/customer/setup_unselected.png'
    }
    return array
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    //编辑页
    if (options.kind !== undefined) {

      //佣金单位
      if (options.commission_type == 1) {
        that.data.param.commission.unit = '元';
      } else {
        that.data.param.commission.unit = '%'
      }
      that.setData({
        subType: true,
        param: that.data.param,
        commission_type: (this.data.param.commission.unit == '%' ? 0 : 1),
        customer_order_id: options.id
      })
      that.getDetail(options.id, options.kind);
    }
    that.setData({
      proId: options.proId
    })

    //获取编辑信息
    var params = {
      proId: options.proId
    }
    app.util.api('record/personal_setting', 'GET', params, function(data) {
      var settingDetail = data.data;
      var middle = that.data.param;
      for (var i = 0; i < settingDetail.length; i++) {
        var index = settingDetail[i].name;
        if (index == middle[index].name && settingDetail[i].item_none == false) {
          middle[index].show = true;
          middle[index].need = settingDetail[i].item_selected;
          //若无该参数则不提交
        } else if (settingDetail[i].item_none == true) {
          middle[index].name = '';
        }
        var info = {
          [index]: middle[index]
        }
      }
      that.setData({
        param: middle
      })
    })
  },

  /**
   * 编辑录单信息
   */
  getDetail: function(id, kind) {
    var that = this;
    var params = {
      custom_id: id
    }
    app.util.api('record/edit_info', 'GET', params, function(data) {
      var arr = [];
      for (var i in data.data) {
        arr.push(data.data[i])
      }

      //处理
      var middle = that.data.param;
      for (var i = 0; i < arr.length; i++) {
        var index = arr[i].name;

        //单选、多选处理
        if (middle[index] !== undefined && middle[index].name == index && middle[index].choose_array !== undefined && arr[i].value !== null) {
          var testPreg = arr[i].value.toString().split(",");
          for (var l = 0; l < middle[index].choose_array.length; l++) {
            if (that.InArray(middle[index].choose_array[l].tag_number.toString(), testPreg)) {
              middle[index].choose_array[l].selected = true;
              middle[index].choose_array[l].tag_image_path = '/img/customer/choose_seleted.png';
            }
          }

          //表单值处理
        } else if (middle[index] !== undefined && middle[index].name == index && middle[index].choose_array == undefined && arr[i].value !== null) {
          middle[index].value = arr[i].value;
        }
      }

      //选择框 有无房产
      var data = that.data.param;
      if (data.house.choose_array[0].selected) {
        that.setData({
          house: true
        })
      } else {
        that.setData({
          house: false
        })
      }
      if (data.car.choose_array[0].selected) {
        that.setData({
          car: true
        })
      } else {
        that.setData({
          car: false
        })
      }

      //营业执照（无）点击
      if (data.business_license.choose_array[0].selected) {
        that.setData({
          card: true
        })
      } else {
        that.setData({
          card: false
        })
      }

      //社保公积点击
      if (data.protect.choose_array[0].selected) {
        that.setData({
          gold: true
        })
      } else {
        that.setData({
          gold: false
        })
      }

      //个人负债点击
      for (var i = 0; i < data.personal_debt.choose_array.length; i++) {
        if (data.personal_debt.choose_array[i].selected) {
          that.setData({
            [data.personal_debt.choose_array[i].child_name]: false
          })
        } else {
          that.setData({
            [data.personal_debt.choose_array[i].child_name]: true
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function() {

  // }
})