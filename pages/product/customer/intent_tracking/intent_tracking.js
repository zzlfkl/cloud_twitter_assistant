
// pages/tools/record/intent_tracking/intent_tracking.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    has_read: false,
    bar_item_width: 0,
    swiper_current: 1,
    bar_item_titles: [
    //   {
    //   "title": "确认中",
    //   "selectedTitle": "确认中",
    //   "matchingId": "0",
    //   "isSelected": true,
    //   'page': 1
    // }, 
    {
      "title": "办理中",
      "selectedTitle": "办理中",
      "matchingId": "1",
        "isSelected": true,
      'page': 1,
    }, 
    {
      "title": "已完成",
      "selectedTitle": "已完成",
      "matchingId": "2",
      "isSelected": false,
      'page': 1
    }, {
      "title": "已拒绝",
      "selectedTitle": "已拒绝",
      "matchingId": "3",
      "isSelected": false,
      'page': 1
    }],
  },

  delte_users:function(e){
    var this_ = this;
    var params = {
      customer_order_id: this_.data.custom_id,
      ids:e.detail.ids
    }
    app.util.api('record/gd_group_cancel','POST',params,function(data){
      console.log(data);
      if(data.success){
        wx.showToast({
          title: data.msg,
          icon:'none'
        })
        setTimeout(function(){
          this_.recordUsers.hidePopup();
        },1000)
      }
    })
  },

  gd_list:function(e){
    var that = this;
    var custom_id = e.currentTarget.dataset.custom_id;
    that.setData({
      custom_id: custom_id
    })
    var params = {
      custom_id: custom_id
    }
    app.util.api('record/gd_list','POST',params,function(data){
      if (data.data.length <= 0) {
        wx.showToast({
          icon: 'none',
          title: '暂无跟单人员',
        })
      }else {
        that.setData({
          list: data.data
        })
        that.recordUsers.showPopup()
      }
    })
  },

  gd_cancel: function (e) {
    var this_ = this;
    var pid = e.currentTarget.dataset.custom_id
    var params = {
      custom_id: pid
    }
    console.log(params)
    wx.showModal({
      title: '温馨提示',
      content: '确定取消跟单吗？',
      success: function (res) {
        if (res.confirm) {
          app.util.api('record/gd_cancel', 'POST', params, function (data) {
            console.log(data);
            if(data.success){
              wx.showToast({
                title: '取消成功',
                icon:'none'
              })
              setTimeout(function(){
                this_.forData(this_.data.swiper_current);
              },500)
            }
          });
        }
      }
    })
  },

  // tab绑定点击事件
  barItemTap: function(e) {
    var temp_array = this.data.bar_item_titles;
    for (var idx = 0; idx < temp_array.length; idx++) {
      temp_array[idx].isSelected = false;
      if (temp_array[idx].matchingId == e.currentTarget.dataset.identify) {
        temp_array[idx].isSelected = true;
      }
    }
    // 重置swiper选项
    this.setData({
      swiper_current: e.currentTarget.dataset.identify,
    })
    this.reloadData(temp_array);

    // 刷新数据
    this.forData(this.data.swiper_current);
  },

  detail: function(e) {
    var customId = e.currentTarget.dataset.id;
    var rid = e.currentTarget.dataset.rid;
    wx.navigateTo({
      url: '/pages/product/customer/record_detail/detail?custom_id=' + customId + ' &&rid=' + rid,
    })
  },
  // 刷新顶部tab数据
  reloadData: function(array) {
    this.setData({
      bar_item_titles: array,
      bar_item_width: app.globalData.sysWidth / array.length
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.reloadData(this.data.bar_item_titles)
    this.forData(1);
  },

  /**
   * 请求数据
   */
  forData: function(status) {
    var that = this;
    var params = {
      "status": status
    };

    app.util.api("record/order_list", 'GET', params, function(data) {
      console.log(data)
      if (data.status == 200) {
        that.setData({
          orderList: data.data
        })
      } else {
        that.setData({
          orderList: []
        })
      }
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.recordUsers = this.selectComponent("#record_users");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.forData(this.data.swiper_current);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
})