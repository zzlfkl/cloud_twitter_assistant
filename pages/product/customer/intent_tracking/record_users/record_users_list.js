// pages/product/customer/intent_tracking/record_users/record_users_list.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list: {
      type: Array,
      value: [],
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    flag: true,
    // list: ['1', '2', '1', '2', '1', '2', '1', '2', '1', '2', '1', '2', '1', '2', '1', '2']
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 显示
    showPopup() {
      this.setData({
        flag: false
      })
    },
    // 隐藏弹窗
    hidePopup() {
      this.setData({
        flag: true
      })
      this.triggerEvent("hidePopup")
    },
    // 隐藏键盘
    hiddenKeyboard() {
      
    },
    // 改变选择
    changeSelect(e) { 
      var temp_list = this.data.list
      temp_list.forEach(item => {
        if (e.currentTarget.dataset.identity == item.user_id) {
          item.selected = !item.selected;
        }
      })
      this.setData({
        list: temp_list
      })
    },
    // 删除跟单
    delteRecordUsers(e) {
      var ids = [];
      this.data.list.forEach(item => {
        if (item.selected) {
          ids.push(item.user_id)
        }
      })
      console.log(ids);
      if (ids.length == 0){
        wx.showToast({
          title: '请至少选择一人',
          icon:'none'
        })
        return;
      }
      this.triggerEvent("delte_users", {ids })
    }
  }
})
