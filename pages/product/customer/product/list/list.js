// pages/tools/record/product/list/list.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },
  /**
   * 获取本公司产品列表
   */
  getList: function () {
    var that = this;
    var params = {
      kind: 'company_product'
    }
    app.util.api("record/get_company_product", 'GET', params, function (data) {
      that.setData({
        list: data.data
      })
    });
  },

  // 点击产品详情
  tapProductItem: function (e) {
    var productId = e.currentTarget.dataset.id;
    if (this.data.kind == 'record'){
      var url = '/pages/product/customer/record/record?proId='+productId
    } else if (this.data.kind == 'receipt'){
      var url = '/pages/product/customer/setup/setup?proId=' + productId
    }
    wx.navigateTo({
      url: url,
    })
  },

  // 添加产品
  addProduct: function (e) {
    wx.showActionSheet({
      itemList: ['自定义发布', '使用模板发布'],
      success: function (res) {
        if (res.tapIndex == 0) {
          wx.navigateTo({
            url: '/pages/product/publish',
          })
        } else {
          wx.navigateTo({
            url: '/pages/product/template',
          })
        }
      },
    })
  },

  /**
   * 获取我的产品
   */
  proMine:function(){
    var that = this;
    var params = {
      value:1
    };
    app.util.api('userinfo/pro_me','GET',params,function(data){

      that.setData({
        list:data.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList();
    this.setData({
      kind:options.type
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getList();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})