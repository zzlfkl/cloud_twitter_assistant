// pages/product/customer/product/add_record_product/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    condition: [{
      name: '等本等息',
      value: '1',
      checked: false
    },
    {
      name: '等额本息',
      value: '2',
      checked: false
    },
    {
      name: '等额本金',
      value: '3',
      checked: false
    },
    {
      name: '先息后本',
      value: '4',
      checked: false
    },
    {
      name: '后息后本',
      value: '5',
      checked: false
    },
    {
      name: '复合方式',
      value: '6',
      checked: false
    }
    ],
  },

  /**
  * 提交
  */
  formSubmit: function (e) { 
    var this_ = this;
    var params = e.detail.value;
    if (!params.pro_name || !params.mon_interest) {
      wx.showToast({
        title: '请输入产品名称/产品月息',
        icon: 'none'
      })
      return;
    }
    if(params.pay_kind.length == 0){
      wx.showToast({
        title: '请选择还款方式',
        icon:'none'
      })
      return;
    }
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { });
    app.util.api('record/self_product_add','POST',params,function(data){
      console.log(data);
      if(data.success){
        wx.showModal({
          title: '温馨提示',
          content: '添加成功',
          showCancel:false,
          success:function(res){
            if(res.confirm){
              wx.navigateBack({})
            }
          }
        })
      }
    })
  },

  // 
  condition: function (e) {
    var condition = this.data.condition;
    var checkArr = e.detail.value;
    console.log(e.detail.value)
    for (var i = 0; i < condition.length; i++) {
      if (checkArr.indexOf(condition[i].value) != -1) {
        condition[i].checked = true;
      } else {
        condition[i].checked = false;
      }
    }
    this.setData({
      condition: condition
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})