// pages/product/customer/product/change/change_list.js

var app = getApp();

Page({

  data: {
    list: [],
    startX: 0, //开始坐标
    startY: 0
  },

  // 添加产品
  addProduct: function (e) {
    wx.navigateTo({
      url: '/pages/product/customer/product/record_product/index',
    })
  },

  /**
   * 获取本公司产品列表
   */
  getList: function () {
    var that = this;
    var params = {
      kind: 'company_product'
    }
    app.util.api("record/get_self_product", 'GET', params, function (data) {
      var temp_array = data.data
      for (var temp in temp_array)
        temp_array[temp].isTouchMove = false
      that.setData({
        list: temp_array
      })
    })
  },


  /**
   * 修改录单产品
   */
  changeProduct: function (e) {
    if (this.data.customer_order_id == undefined) {
      return
    }
    var proId = e.currentTarget.dataset.id;
    var params = {
      proId: proId,
      customer_order_id: this.data.customer_order_id
    }
    app.util.api("record/change_product", 'POST', params, function (data) {
      if (data.success) {
        wx.showToast({
          title: data.msg,
          success: function () {
            setTimeout(function () {
              wx.navigateBack()
            }, 1000)
          }
        })
      }
    });
  },

  onLoad: function (options) {
    var that = this;
    if (options.customer_order_id) {
      // 更改具体产品
      this.setData({
        customer_order_id: options.customer_order_id
      })
    } else {
      // 管理收单设置
      wx.setNavigationBarTitle({
        title: '收单设置',
      })
    }
  },

  //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    //开始触摸时 重置所有删除
    this.data.list.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })

    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      list: this.data.list
    })

  },

  //滑动事件处理
  touchmove: function (e) {
    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.list.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })

    //更新数据
    that.setData({
      list: that.data.list
    })

  },

  /**
  * 计算滑动角度
  * @param {Object} start 起点坐标
  * @param {Object} end 终点坐标
  */

  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },

  //删除事件

  del: function (e) {
    var this_ = this;
    var id = e.currentTarget.dataset.id;
    var params = {
      id:id
    }
    wx.showModal({
      title: '温馨提示',
      content: '确认删除该产品吗？',
      success:function(res){
        if(res.confirm){
          app.util.api('record/self_product_del', 'POST', params, function (data) {
            console.log(data);
            if (data.success) {
              this_.data.list.splice(e.currentTarget.dataset.index, 1);
              this_.setData({
                list: this_.data.list
              })
            }
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
    wx.stopPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})