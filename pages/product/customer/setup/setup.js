// pages/tools/record/setup/setup.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  // 必填 
  needSetUpTap: function(e) {
    var num = e.currentTarget.dataset.item
    var item = this.data.item_array[num]
    if (item.item_none) { // 不显示
      item.item_none = false
      item.item_selected = true
    } else {
      if (item.item_selected) {
        item.item_selected = false
        item.item_none = true
      } else {
        item.item_selected = true
        item.item_none = false
      }
    }
    this.replceItem(num, item)
  },

  // 选填 
  unNeedSetUpTap: function(e) {
    var num = e.currentTarget.dataset.item
    var item = this.data.item_array[num]
    if (item.item_none) {
      item.item_none = false
      item.item_selected = false
    } else {
      if (item.item_selected) {
        item.item_selected = false
        item.item_none = false
      } else {
        item.item_selected = true
        item.item_none = true
      }
    }
    this.replceItem(num, item)
  },
  // 替换某一项
  replceItem: function(index, item) {
    var temp_array = this.data.item_array
    temp_array.splice(index, 1, item)
    this.setData({
      item_array: temp_array
    })
  },

  /**
   * 提交数据
   */
  confirm: function(e) {

    var params = {
      str: JSON.stringify(this.data.item_array),
      proId: this.data.proId
    }
   
    app.util.api('record/param_post', 'POST', params, function(data) {
      if (data.success == 1) {
        wx.showToast({
          title: data.msg,
        })
        setTimeout(function() {
          wx.navigateBack()
        }, 1000)
      } else {
        wx.showModal({
          title: '失败',
          content: data.msg,
          showCancel: false
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      proId: options.proId
    })

    //网络请求
    var params = {
      proId: options.proId
    }
    app.util.api('record/param', 'GET', params, function(data) {
      that.setData({
        item_array: data.data
      })
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
 
})