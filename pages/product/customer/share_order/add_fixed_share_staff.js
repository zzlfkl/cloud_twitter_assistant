// pages/tools/record/share_order/add_fixed_share_staff.js
var app = getApp(); 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    has:true,
    length:0,
    keyword: '',
    page: 1,
    enablePullup: true,
    fixed_share_selected: true,
    search: {
      placeholder: "搜精英",
      searchText: "",
      searchImage: "/img/search.png",
    },
    list: [],
    selecteds: []
  },

  /**
   * 确认搜索
   */
  confirm: function () {
    this.setData({
      page: 1,
      list: []
    })
    this.user_list(this.data.keyword)
  },

  /**
   * 搜索收入变化
   */
  bindSearchInput: function (e) {
    var that = this
    this.setData({
      page: 1,
      keyword: e.detail.value,
      list: []
    })
    this.user_list(e.detail.value)
  },

  /**
   * 获取用户列表
   */
  user_list: function (keyword) {
    var that = this
    var params = {
      keyword: keyword,
      page: that.data.page
    }
    app.util.api('elites/user_search', 'GET',params, function (data) {
      if (data.data.length >= 10) {
        that.data.enablePullup = true
      } else {
        that.data.enablePullup = false
      }
      var array = that.data.list
      array = array.concat(data.data)
      that.setData({
        list: array
      })
      that.data.page += 1
      wx.stopPullDownRefresh()
    });
  },

  open: function () {
    this.setData({
      has: this.data.has ? false : true
    })
  },

  /**
   * 输入框输入事件
   */
  search: function (e) {
    this.setData({
      keyword: e.detail.value,
      list: []
    })
    this.user_list(this.data.keyword); 
  },

  // 改变选中状态
  changeStaffSelect: function (e) {
    var item = e.currentTarget.dataset.item
    var temp_selecteds = this.data.selecteds
    var index = temp_selecteds.indexOf(item.id)
    if (index != -1) {
      temp_selecteds.splice(index, 1)
    } else {
      temp_selecteds.push(item.id)
    }
    this.setData({
      selecteds: temp_selecteds
    })
  },
  //添加固定收单人
  add:function(){
    var params = {
      ids:this.data.selecteds
    };

    app.util.api("record/edit_fixed_gd",'POST',params,function(data){
      if(data.success){
        wx.showToast({
          title: data.msg,
          success:function(){
            setTimeout(function(){
              wx.navigateBack()
            },1000)
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.throwGroup !== 'undefined'){
      var fixed_detail =  JSON.parse(options.fixed_detail);
      var arr = options.throwGroup.split(",");
      for (var i = 0; i < arr.length; i++) {
        arr[i] = parseInt(arr[i]);
      }
      this.setData({
        selecteds: arr,
        fixed: fixed_detail,
        length: arr.length
      })
    }
    this.user_list(this.data.keyword);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      page: 1,
      list: []
    })
    this.user_list(this.data.keyword);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.enablePullup) {
      this.user_list(this.data.keyword);
    }
  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})