// pages/tools/record/share_order/share_order.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    length: 0,
    isSearch: false,
    has: false,
    keyword: '',
    page: 1,
    enablePullup: true,
    fixed_share_selected: true,
    placeholder:'搜精英',
    list: [],
    selecteds: [],
    groups_detail: []
  },

  //展开
  open: function() {
    this.setData({
      has: this.data.has ? false : true
    })
  },

  //确认甩单
  throwOrder: function() {
    var params = {
      ids: this.data.selecteds,
      custom_id: this.data.custom_id
    };

    app.util.api("record/throw_order", "POST", params, function(data) {
      if (data.success) {
        wx.showToast({
          title: data.msg,
          success: function() {
            setTimeout(function() {
              wx.navigateBack()
            }, 1000)
          }
        })
      }
    });
  },

  // 改变选中状态
  changeStaffSelect: function(e) {
    var item = e.currentTarget.dataset.item
    var temp_selecteds = this.data.selecteds
    var index = temp_selecteds.indexOf(item.id)
    if (index != -1) {
      for (var i = 0; i < this.data.groups_detail.length; i++) {
        if (this.data.groups_detail[i].id == item.id) {
          this.data.groups_detail.splice(i, 1)
        }
      }
      temp_selecteds.splice(index, 1)
    } else {
      temp_selecteds.push(item.id);
      this.data.groups_detail.push(item)
    }
    this.setData({
      selecteds: temp_selecteds,
      groups_detail: this.data.groups_detail,
      length: temp_selecteds.length
    })
  },

  changeFixedSelected: function() {
    var temp_selected = this.data.fixed_share_selected
    this.setData({
      fixed_share_selected: !temp_selected
    })
    if (this.data.groups_detail !== undefined && this.data.throwGroup !== undefined) {
      if (this.data.fixed_share_selected) {
        for (var i = 0; i < this.data.groups.length; i++) {
          this.data.groups_detail.push(this.data.groups[i]);
        }
        // 去重
        var result = [];
        var obj = {};
        for (var i = 0; i < this.data.groups_detail.length; i++) {
          if (!obj[this.data.groups_detail[i].id]) {
            result.push(this.data.groups_detail[i]);
            obj[this.data.groups_detail[i].id] = true;
          }
        }

        // 选中状态
        var arr = this.data.selecteds;
        for (var i = 0; i < this.data.throwGroup.length; i++) {
          arr.push(this.data.throwGroup[i])
        }

        this.setData({
          selecteds: arr,
          length: result.length,
          groups_detail: result
        })
      } else {
        var arr = this.data.selecteds;
        for (var i = 0; i < this.data.throwGroup.length; i++) {
          if (arr.indexOf(this.data.throwGroup[i]) != -1) {
            for (var l = 0; l < arr.length; l++) {
              if (arr[l] == this.data.throwGroup[i]) {
                arr.splice(l, 1)
              }
            }
          }
        }
        // 去除已选接单人中未选择的固定甩单人
        for (var i = 0; i < this.data.groups_detail.length; i++) {
          if (arr.indexOf(this.data.groups_detail[i].id) == -1) {
            delete this.data.groups_detail[i];
          }
        }

        // 去除undifind
        var group = [];
        for (var i = 0; i < this.data.groups_detail.length; i++) {
          if (typeof(this.data.groups_detail[i]) != 'undefined') {
            group.push(this.data.groups_detail[i]);
          }
        }
        this.setData({
          selecteds: arr,
          groups_detail: group,
          length: arr.length
        })
      }
    }
  },

  /**
   * 确认搜索
   */
  confirm: function() {
    this.setData({
      page: 1,
      list: [],
      isSearch: true
    })
    this.user_list(this.data.keyword)
  },

  bindBlurInput: function() {

  },
  bindfocusInput: function() {

  },
  /**
   * 搜索收入输入变化
   */
  bindTextInput: function(e) {
    var that = this
    this.setData({
      page: 1,
      keyword: e.detail.value,
      list: []
    })
  },

  /**
   * 历史接单人
   */
  throwHis: function() {
    var that = this;
    var params = {
      kind: 'his'
    }
    app.util.api('record/throw_his', 'GET', params, function(data) {
      that.setData({
        hisThrow: data.data
      })
    });
  },

  /**
   * 获取用户列表
   */
  user_list: function(keyword) {
    var that = this
    var params = {
      keyword: keyword,
      page: that.data.page
    }
    app.util.api('elites/user_search', 'GET', params, function(data) {
      if (data.data.length >= 10) {
        that.data.enablePullup = true
      } else {
        that.data.enablePullup = false
      }
      var array = that.data.list
      array = array.concat(data.data)
      that.setData({
        list: array
      })
      that.data.page += 1
      wx.stopPullDownRefresh()
    });
  },

  // 添加固定甩单
  addFixedShare: function(e) {
    wx.navigateTo({
      url: '/pages/product/customer/share_order/add_fixed_share_staff?throwGroup=' + this.data.throwGroup + ' &&fixed_detail=' + JSON.stringify(this.data.groups),
    })
  },

  /**
   * 获取个人固定甩单人配置
   */
  getThrowList: function() {
    var that = this;
    var params = {
      userId: app.data.userinfo.id
    }
    app.util.api("record/throw_fixed", 'GET', params, function(data) {
      if (data.status == 200) {
        var arr = data.data.throw_group.split(",");
        for (var i = 0; i < arr.length; i++) {
          arr[i] = parseInt(arr[i]);
        }
        that.setData({
          throwGroup: arr,
          selecteds: arr,
          groups_detail: data.data.groups_detail,
          groups: data.data.groups_detail,
          length: arr.length
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.throwHis();
    this.setData({
      custom_id: options.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getThrowList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    if (this.data.isSearch) {
      this.setData({
        page: 1,
        list: []
      })
      this.user_list(this.data.keyword);
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.enablePullup && this.data.isSearch) {
      this.user_list(this.data.keyword);
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})