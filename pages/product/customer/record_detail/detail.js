// pages/tools/record/detail/detail.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    actionOpen: false,
    trackOpen: false,
    action: false,
    track: false,
    open: false,
    status: false,
    hiddenmodalput: true,
    commissionShow: true,
    unit: ['%', '元'],
    index: 0
  },

  create_happy:function(e){
    var id = e.currentTarget.dataset.id;
    var params = {
      id: id
    }
    app.util.api('record/happy_info', 'GET', params, function (data) {
      wx.navigateTo({
        url: '/pages/customer/tools/happy/create?info=' + JSON.stringify(data.data),
      })
    })
  },

  open: function() {
    this.setData({
      open: this.data.open ? false : true
    })
  },
  //佣金单位选择
  bindPickerChange: function(e) {
    this.setData({
      index: e.detail.value
    })
  },

  /**
   * 编辑客户信息
   */
  editInfo: function(e) {
    var that = this;
    if (app.data.userinfo.id !== this.data.detail.record_id) {
      return;
    } else {
      var customer_order_id = e.currentTarget.dataset.id;
      var commission_type = e.currentTarget.dataset.commission_type;
      wx.showModal({
        title: '修改',
        content: '确定要修改客户信息吗？',
        success: function(res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/product/customer/record/record?kind=edit&&proId=&&commission_type=' + commission_type + '&&id=' + customer_order_id + '&&rid=' + that.data.custom_id,
            })
          }
        }
      })
    }
  },

  /**
   * 拨打录单人电话
   */
  recordCall: function() {
    var that = this;
    if (that.data.system == 'iOS'){
      wx.makePhoneCall({
        phoneNumber: that.data.detail.mobile,
      })
    }else{
      wx.showModal({
        title: '拨打电话',
        content: that.data.detail.mobile,
        success: function (res) {
          if (res.confirm) {
            wx.makePhoneCall({
              phoneNumber: that.data.detail.mobile,
            })
          }
        }
      })
    }
  },

  /**
   * 拨打客户电话
   */
  customerCall: function() {
    var that = this;
    wx.showModal({
      title: '拨打电话',
      content: (that.data.detail.phone).toString(),
      success: function(res) {
        if (res.confirm) {
          wx.makePhoneCall({
            phoneNumber: (that.data.detail.phone).toString(),
          })
        }
      }
    })
  },

  /**
   * 金额输入
   */
  moneyNum: function(e) {
    this.setData({
      money: e.detail.value
    })
  },

  commissionNum: function(e) {
    this.setData({
      commission: e.detail.value
    })
  },
  //取消
  cancel: function() {
    this.setData({
      hiddenmodalput: true,
      commissionShow: true
    })
  },

  /**
   * 修改需求金额
   */
  changeMoney: function(e) {
    this.setData({
      hiddenmodalput: !this.data.hiddenmodalput
    })
  },

  //确认
  moneyConfirm: function(e) {
    var that = this;
    //非空验证
    if (that.data.money == undefined || that.data.money == '') {
      wx.showToast({
        title: '请输入需要修改的贷款金额！',
        icon: 'none'
      })
      return;
    }
    var params = {
      custom_order_id: that.data.custom_order_id,
      money: that.data.money,
      field: 'money'
    }
    app.util.api("record/change_order", 'POST', params, function(data) {
     
      if (data.success) {
        wx.showToast({
          title: data.msg,
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
      that.setData({
        hiddenmodalput: true
      })
      setTimeout(function() {
        that.detail(that.data.custom_order_id)
      }, 1000)
    });
  },

  /**
   * 修改服务佣金
   */
  changeCommission: function(e) {
    // if (app.data.userinfo.id == this.data.detail.record_id) {
    this.setData({
      commissionShow: !this.data.commissionShow
    })
    // }

  },

  //确认
  commissionConfirm: function(e) {
    var that = this;
    //非空验证
    if (that.data.commission == undefined || that.data.commission == '') {
      wx.showToast({
        title: '请输入需要修改的服务佣金！',
        icon: 'none'
      })
      return;
    }
    var params = {
      custom_order_id: that.data.custom_order_id,
      commission: that.data.commission,
      field: 'commission',
      commission_type: that.data.index
    }
    app.util.api("record/change_order", 'POST', params, function(data) {
   
      if (data.success) {
        wx.showToast({
          title: data.msg,
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
      that.setData({
        commissionShow: true
      })
      setTimeout(function() {
        that.detail(that.data.custom_order_id)
      }, 1000)
    });

  },

  /**
   * 修改录单状态
   */
  editStatus: function(e) {
    var that = this;
    wx.showActionSheet({
      itemList: ['办理中', '已完成', '已拒绝'],
      success: function(res) {
        var params = {
          status: res.tapIndex+1,
          id: e.currentTarget.dataset.id
        }
        app.util.api("record/change_status", 'POST', params, function(data) {
          if (data.success == 1) {
            wx.showToast({
              title: data.msg,
              duration: 1000,
              success: function() {
                that.detail(that.data.custom_order_id);

              }
            })
          }
        })
      }
    })
  },

  /**
   * 提交跟进
   */
  formSubmit: function(e) {
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
    var that = this;
    that.setData({
      status: true
    })
    var params = e.detail.value;
    params.customer_order_id = that.data.custom_order_id;
    app.util.api("record/track_add", "POST", params, function(data) {
      that.setData({
        status: false
      })
      if (data.success) {
        wx.showToast({
          title: data.msg,
        });
        that.setData({
          value: ''
        })
        setTimeout(function() {
          that.detail(that.data.custom_order_id)
        }, 1000)
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })

      }
    });
  },

  /**
   * 甩单
   */
  throwOrder: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/product/customer/share_order/share_order?id=' + id,
    })
  },

  /**
   * 修改产品
   */
  changeProduct: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/product/customer/product/change/change_list?customer_order_id=' + id,
    })
  },

  // 影像资料详情
  imageDetail: function(e) {
    var id = e.currentTarget.dataset.id;
    var app = e.currentTarget.dataset.app;
    wx.navigateTo({
      url: '/pages/product/customer/certify/certify?id=' + id + '&app=' + app,
    })
  },

  //获取录单详情
  detail: function(id) {
    var that = this;
    var params = {
      custom_id: id,
      rid:that.data.rid,
      kind: 'show'
    }
    app.util.api("record/order_detail", 'GET', params, function(data) {
      
      for (var i = 0; i < data.data.image.length; i++) {
        that.setData({
          [data.data.image[i].type]: data.data.image[i].num
        })
      }
      if (data.success == 1) {
        // 操作记录
        if (data.data.action_log.length > 3) {
          that.setData({
            actionOpen: true,
            action_log: data.data.action_log.slice(0, 3)
          })
        } else {
          that.setData({
            action_log: data.data.action_log
          })
        }
        // 跟进记录
        if (data.data.track_his.length > 3) {
          that.setData({
            trackOpen: true,
            track_his: data.data.track_his.slice(0, 3)
          })
        } else {
          that.setData({
            track_his: data.data.track_his
          })
        }
        that.setData({
          detail: data.data
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  //操作记录展开/折叠
  actionOpen: function() {
    this.setData({
      action: this.data.action ? false : true,
      action_log: this.data.action ? (this.data.detail.action_log.slice(0, 3)) : (this.data.detail.action_log)
    })
  },

  //跟进记录展开/折叠
  trackOpen: function() {
    this.setData({
      track: this.data.track ? false : true,
      track_his: this.data.track ? (this.data.detail.track_his.slice(0, 3)) : (this.data.detail.track_his)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
    this.setData({
      rid:options.rid,
      custom_order_id: options.custom_id
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    // 检测用户手机系统
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        if (res.system.indexOf('iOS') !== -1){
          var system = 'iOS'
        } else if (res.system.indexOf('Android') !== -1){
          var system = 'Android'
        }
        that.setData({
          system: system
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.detail(this.data.custom_order_id);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.detail(this.data.custom_order_id);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
})