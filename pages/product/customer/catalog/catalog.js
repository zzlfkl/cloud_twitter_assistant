// pages/tools/record/catalog/catalog.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemDataArray: [{
        title: "客户跟踪",
        image: "/img/customer/record_purpose.png",
        hint: "",
        bottomLine: true,
        topLine: true,
        rightArrow: true,
        identify: "0",
        show: true
      }, {
        title: "产品设置",
        image: "/img/customer/record_setup.png",
        hint: "",
        bottomLine: true,
        topLine: false,
        rightArrow: true,
        identify: "3",
        show: true
      }
    ],
  },

  // 分类点击
  itemTap: function(e) {
    var identify = e.currentTarget.dataset.identify
    if (app.data.userinfo.user_status !== 1) {
      wx.navigateTo({
        url: '/pages/user/pre_personal_authentication/index',
      })
      return;
    }
    if (identify == 0) {
      wx.navigateTo({
        url: '../intent_tracking/intent_tracking',
      })
    } else if (identify == 2) {
      
      wx.navigateTo({
        url: '/pages/product/customer/record/record?proId=',
      })
    } else if (identify == 3) {
      wx.navigateTo({
        url: '/pages/product/customer/product/change/change_list',
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})