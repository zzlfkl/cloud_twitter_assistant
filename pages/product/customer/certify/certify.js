// pages/tools/record/certify/certify.js
var apps = getApp();
var upload = require("../../../../utils/upload.js")
Page({
  /**
   * 页面的初始数据
   */
  data: {
    items:[]
  },

  // 上传图片
  uploadImage: function (e) {
    var that = this;
    var params = {
      app:that.data.app,
      customer_order_id: that.data.customer_order_id
    }
    upload.uploadImage("record/customer_picture",params,function(data){
      if(data.success){
        that.getImage(that.data.customer_order_id, that.data.app);
      }
    });
  },

  // 显示图片
  showImage: function (e) {
    var index = e.currentTarget.dataset.showindex,
    list = [];
    for(var i = 0;i < this.data.items.length;i++){
      list.push(this.data.items[i].url);
    }
    wx.previewImage({
      current: this.data.items[index].url, 
      urls: list
    })
  },

  // 删除图片
  deleteImage: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '温馨提示',
      content: '确定删除该照片吗？',
      success: function (res) {
        if (res.confirm) {
          var params = {
           id:id
          }
          apps.util.api("record/delete_custom_image",'post',params,function(data){
            if(data.success){
              wx.showToast({
                title: data.msg,
                success:function(){
                  setTimeout(function(){
                    that.getImage(that.data.customer_order_id, that.data.app);
                  },1000)
                }
              })
            }else{
              wx.showToast({
                title: data.msg,
                icon:'none'
              })
            }
          });
        }
      }  
    })
  },

  /**
   * 获取影像资料
   */
  getImage: function (customer_order_id, app){
    var that = this;
    var params = {
      customer_order_id: customer_order_id,
      app: app
    }
    apps.util.api("record/get_custom_image",'GET',params,function(data){     
      if(data.success){
        that.setData({
          items: data.data
        })
      }else{
        console.log(data)
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getImage(options.id, options.app);
    this.setData({
      customer_order_id:options.id,
      app:options.app,
      items:[]
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})