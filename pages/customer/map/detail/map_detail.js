// pages/tools/map/detail/map_detail.js
var app = getApp()
const mapMaxHeight = 350
const mapMinHeight = 150
var otherViewHeight = 44 + (100 / 750) * app.globalData.sysWidth
Page({

  /**
   * 页面的初始数据
   */
  data: {
    authStatus:true,
    list:[],
    placeholder:"展业搜索",
    page: 1,
    showAll: true,
    mapHeight: mapMinHeight,
    otherHeight: 0,
    location: null,
    mapCtx: null,
    activityImageUrl: "/img/copy.png",
    markers: [],

    controls: [{
      id: 1,
      iconPath: '/img/position.png',
      position: {
        left: app.globalData.sysWidth - 38 - 12,
        top: mapMaxHeight - 38 - 22,
        width: 38,
        height: 38
      },
      clickable: true
    }],
    company_areas: [],
    is_search:false,
    searchList: [],
    searchPage:1
  },

  /**
   * 输入级获取焦点
   */
  bindfocusInput :function(e){
    this.setData({
      is_search:true,
      searchList:[]
    })
    this.getInfo(e.detail.value)
  },

  //输入框输入事件
  bindTextInput :function(e){
    this.setData({
      keyword:e.detail.value,
    })
    if(e.detail.value.length == 0){
      this.getInfo('')
    }
  },
  
  //确认搜索事件
  confirm :function(){
    this.setData({
      searchList:[],
    })
    this.getInfo(this.data.keyword)
  },

  // 取消搜索
  cancel:function(){
    this.setData({
      is_search:false,
    })
  },

  //获取数据
  getInfo:function(keyword){
    var that = this;
    var params = {
      'keyword': keyword,
      'page': that.data.searchPage
    }
    app.util.api('map/map_search', 'GET', params, function (data) {
      if(data.success == 1 && data.data !== 0){
        that.setData({
          searchList: that.data.searchList.concat(data.data)
        })
      }
    });
  },

  //搜索列表点击事件
  change_company:function(e){
    var that = this;
    var clickType = e.currentTarget.dataset.type;
    var id = e.currentTarget.dataset.id;
    if(clickType == 'company'){
      wx.navigateTo({
        url: '/pages/product/company/detail/company_detail?id= '+id,
      })
    } else if (clickType == 'business'){
      var params = {
        'lng': that.data.longitude,
        'lat': that.data.latitude,
        'areaId': id
      };
      app.util.api('map/marker_company', 'GET', params, function (data) { 
        if(data.success == 1){
          wx.navigateTo({
            url: '/pages/customer/map/company?id=' + id + '&&name=' + data.data.name + '&&detail_address=' + data.data.formatted_address + '&&distance=' + data.data.distance + '&&lng=' + data.data.lng + '&&lat=' + data.data.lat,
          })
        }else{
          console.log(data)
        }
      });
    }
  },

  // 添加商圈
  addBusinessArea: function (e) {
    wx.navigateTo({
      url: '/pages/customer/map/badd',
    })
  },

  // 添加公司
  addCompany: function (e) {
    wx.navigateTo({
      url: '/pages/customer/map/cadd',
    })
  },

  /**
 * 搜索收入变化
 */
  search: function (e) {
    var that = this
    this.setData({
      page: 1,
      keyword: e.detail.value,
    })
  },
  /**
   * 导航
   */
  go: function (e) {
    if (!app.util.checkauth(app)) {
      return;
    }
    if (!app.util.checkvip(app.data.userinfo)) {
      return;
    }
    var that = this;
    var lng = parseFloat(e.currentTarget.dataset.lng);
    var lat = parseFloat(e.currentTarget.dataset.lat);
    wx.openLocation({
      latitude: lat,
      longitude: lng,
      scale: 15
    })
  },

  /**
 * 获取商圈内商家
 */
  list: function (e) {

    //权限检测
    if (!app.util.checkauth(app)) {
      return;
    }
    
    var id = parseInt(e.currentTarget.dataset.id);
    var name = e.currentTarget.dataset.name;
    var address = e.currentTarget.dataset.address;
    var distance = e.currentTarget.dataset.distance;
    var lng = e.currentTarget.dataset.lng;
    var lat = e.currentTarget.dataset.lat;
    wx.navigateTo({
      url: '/pages/customer/map/company?id=' + id + '&&name=' + name + '&&detail_address=' + address + '&&distance=' + distance + '&&lng=' + lng + '&&lat=' + lat,
    })
  },
  /**
   * 加载更多
   */
  load: function (e) {
    var that = this;
    that.setData({
      page: that.data.page + 1
    })
    that.getbusiness(that.data.longitude, that.data.latitude, that.data.page)
  },

  // 修改mapview大小
  modifyActivity: function (e) {
    // 修改mapview大小
    if (this.data.mapHeight == mapMaxHeight) {
      this.setData({
        mapHeight: mapMinHeight,
        otherHeight: wx.getSystemInfoSync().windowHeight - mapMinHeight - otherViewHeight,
        activityImageUrl: "/img/pull.png",
      })
    } else {
      this.setData({
        mapHeight: mapMaxHeight,
        otherHeight: wx.getSystemInfoSync().windowHeight - mapMaxHeight - otherViewHeight,
        activityImageUrl: "/img/copy.png"
      })
    }
    // 修改回到中心位置的定位
    var controls = this.data.controls
    controls[0].position.top = this.data.mapHeight - 38 - 22
    this.setData({
      controls: controls
    })
  },

  /**
   * 点击标记点时触发，会返回marker的id
   */
  markertap(e) {
    var that = this;
    var params = {
      'lng': that.data.longitude,
      'lat':that.data.latitude,
      'areaId': e.markerId
    };
    app.util.api('map/marker_company','GET',params,function(data){
      wx.navigateTo({
        url: '/pages/customer/map/company?id=' + e.markerId + '&&name=' + data.data.name + '&&detail_address=' + data.data.formatted_address + '&&distance=' + data.data.distance + '&&lng=' + data.data.lng + '&&lat=' + data.data.lat,
      })
    });
  },

  /**
   * 点击控件时触发，会返回control的id
   */
  controltap(e) {
    if (e.controlId == 1) {
      this.mapCtx.moveToLocation()
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.modifyActivity(null);
    that.mapCtx = wx.createMapContext('map')
  },

  /**
    * 获取当前位置
    */
  getlocaltion() {
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
        that.getbusiness(res.longitude, res.latitude, that.data.page)
      },
      fail: function (res) {
        console.log(res)
      }
    })
  },

  /**
 * 获取商圈
 */
  getbusiness(lon, lat, page) {
    var that = this;
    var params = {
      'lon': lon,
      'lat': lat,
      'page': page
    };
    app.util.api('map/index', 'GET', params, function (data) {
      if (data.success == 1) {
        var markes = [];
        that.setData({
          list: that.data.list.concat(data.data)
        })
        for (var i = 0; i < that.data.list.length; i++) {
          markes.push({
            id: that.data.list[i].id,
            latitude: that.data.list[i].lat,
            longitude: that.data.list[i].lng,
            iconPath: '/img/location.png',
            width: 20,
            height: 24,
            callout: {
              content: that.data.list[i].name,
              borderRadius: 10,
              color: '#FFFFFF',
              bgColor: '#111111',
              padding: 5,
              display: 'ALWAYS'
            },
          })
        }
        that.setData({
          markers: markes,
        })
      }
    })
  },

  /**
   * 权限检测
   */
  localtion: function (res) {
    var that = this;
    wx.getSetting({
      complete: function (res) {
        if (res.authSetting['scope.userLocation'] == undefined || res.authSetting['scope.userLocation']) {
          that.getlocaltion();
          that.setData({
            authStatus: true
          })
        } else {
          that.setData({
            authStatus:false
          })
        }
      }
    })
  },

  //重新授权
  getAuth:function(){
    this.localtion();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          height: res.screenWidth * 0.45 + 'px',
          bagHeight: res.windowHeight
        })
      },
    });
    this.localtion();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
     
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      searchPage:this.data.searchPage + 1
    })
    this.getInfo(this.data.keyword);
  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // }
})