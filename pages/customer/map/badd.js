var app  = getApp();
var QQMapWX = require('../../../common/qqmap-wx-jssdk.js');
var demo = new QQMapWX({
  key: '4FWBZ-C3U3G-N6IQW-IRZNQ-LLZ2S-2QBJM' // 必填
});
Page({
  /**
   * 页面的初始数据
   */
  data: {
    region: ['安徽省', '合肥市', '庐阳区'],
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      navH: app.globalData.navHeight
    })
  },
  map:function(){
    var that = this;
    // 地图选择
    wx.chooseLocation({
      success: function (res) {
        console.log(res);
        demo.reverseGeocoder({
          location: {
            latitude: res.latitude,
            longitude: res.longitude
          },
          success: function (res) {
       
            that.setData({
              'city_code': res.result.ad_info.adcode
            })
          }
        });
        that.setData({
          roomname: res.name,
          address:res.address,
          lng: res.longitude,
          lat: res.latitude
        })
      },
      fail: function () {
        wx.showToast({
          title: '未选择位置',
          icon:'none'
        })
        // fail
      },
      complete: function () {
        // complete
      }
    })
  },
  /**
   * 选择省市区
   */
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  /**
   * 表单提交
   */
  baddpost:function(e){
    var that   = this;
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id,  function (data) {})
    
    var params = e.detail.value;
    params.lng = that.data.lng;
    params.lat = that.data.lat;
    params.city_code = that.data.city_code;
    app.util.api('map/business_add','POST',params,function(data){
      if (data.success == 1) {
        wx.showModal({
          title: '您的添加申请已提交',
          content: data.msg,
          showCancel: false,
          success: function (e) {
            if (e.confirm) {
              wx.navigateBack({

              })
            }
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

})