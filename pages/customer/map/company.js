var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    page:1,
    list:[]
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      name:options.name,
      address:options.detail_address,
      distance:options.distance,
      id: options.id,
      lng:options.lng,
      lat:options.lat
    });
    this.api(this.data.id,this.data.page)
  },
  /**
   * 地图导航
   */
  go: function (e) {
    var that = this;
    wx.openLocation({
      latitude: parseFloat(that.data.lat),
      longitude: parseFloat(that.data.lng),
      scale: 28
    })
  },
  /**
   * 公司报错
   */
  wrong:function(e){
    var bid = e.currentTarget.dataset.bid;
    var cid = e.currentTarget.dataset.cid;
    var name = e.currentTarget.dataset.name;
    var address = e.currentTarget.dataset.address;
    var bname = e.currentTarget.dataset.bname;
    wx.navigateTo({
      url: 'wrong?bid=' + bid +'&&cid=' +cid+'&&name=' + name + '&&address=' + address+'&&b_name='+bname,
    })
  },
  /**
 * 商圈报错
 */
  bwrong: function (e) {
    var id = e.currentTarget.dataset.id;
    var name = e.currentTarget.dataset.name;
    var address = e.currentTarget.dataset.address;
    wx.navigateTo({
      url: 'bwrong?id=' + id+'&&name='+name+'&&address='+address,
    })
  },
  /**
   * 获取商家
   */
  api(id,page) {
    var that = this;
    var params = {
      'area_id': id,
      'page'   : page
    };
    app.util.api('map/company','GET',params,function(data){
      if (data.success == 1) {
        that.setData({
          list: that.data.list.concat(data.data)
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      page: this.data.page + 1
    })
    this.api(this.data.id, this.data.page)
  },


})