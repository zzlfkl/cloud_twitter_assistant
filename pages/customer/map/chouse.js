var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    page:1,
    keyword:'',
    isManager:false
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getbusiness();
    this.setData({
      navH: app.globalData.navHeight
    })
    if(options.type == 'CompanyManager'){
      this.setData({
        isManager:true,
      })
    }
  },
  suit:function(e){
    var that = this;
    that.setData({
      keyword:e.detail.value,
      page:1
    })
    that.getbusiness();
  },
  /**
   * 选择商圈
   */
  chouse:function(e){
    var cid = parseInt(e.currentTarget.dataset.id);
    var cname = e.currentTarget.dataset.name;
    app.data.business_info = {
      cid: cid,
      cname: cname
    }
    wx.navigateBack({
      delta:1
    })
  },
  ChangeBusiness:function(e){
    var bid = parseInt(e.currentTarget.dataset.id);
    var bname = e.currentTarget.dataset.name;
    wx.showModal({
      title: '更换商圈',
      content: '确认要更换吗？',
      confirmText:'确认更换',
      cancelText:'再想想',
      success:function(res){
        if(res.confirm){
          if (app.data.userinfo.managerCompany !== undefined) {
            var params = {
              bid: bid,
              cid: app.data.userinfo.managerCompany.id
            }
            app.util.api('elites/change_business', 'POST', params, function (data) {
              if (data.success == 1) {
                // 修改全局变量
                app.data.userinfo.managerCompany.b_name = bname;
                wx.showToast({
                  title: data.msg,
                })
                setTimeout(function () {
                  wx.navigateBack()
                }, 1000)
              } else {
                wx.showToast({
                  title: data.msg,
                  icon: 'none'
                })
              }
            });
          } else {
            wx.navigateBack()
          }
        }
      }
    })
    
    
  },
  /**
   * 获取商圈
   */
  getbusiness:function(){
    var that = this;
    var params = {
      'keyword': that.data.keyword,
      'page' :that.data.page
    }
    app.util.api('map/business','GET',params,function(data){
      if(data.success==1){
        if (that.data.page == 1) {
          that.setData({
            list: data.data
          })
        } else {
          that.setData({
            list: that.data.list.concat(data.data)
          })
        }
      }
    })
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    that.setData({
      page: that.data.page + 1,  //每次触发上拉事件，把Page+1  
    });
    that.getbusiness();
  },

})