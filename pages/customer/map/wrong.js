var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [],
    other_reason: '',
    array: [{
      "title": "不存在",
      "selectedTitle": "不存在",
      "matchingId": "2",
      "isSelected": false
    },
    {
      "title": "已搬迁",
      "selectedTitle": "已搬迁",
      "matchingId": "2",
      "isSelected": false
    },
    {
      "title": "地址有误",
      "selectedTitle": "地址有误",
      "matchingId": "2",
      "isSelected": false
    },
      {
        "title": "其他原因",
        "selectedTitle": "其他原因",
        "matchingId": "3",
        "isSelected": false
      },
    ]
  },
  /**
   * 清除所有选中
   */
  clearErrorReason: function() {
    var temp_array = this.data.array;
    for (var idx = 0; idx < temp_array.length; idx++) {
      temp_array[idx].isSelected = false;
    }
    return temp_array
  },
  /**
   * 选中某一个
   */
  simple_item_tap: function(e) {
    var temp_array = this.clearErrorReason()
    //改变属性值
    temp_array[e.currentTarget.dataset.idx].isSelected = true;
    //重新设置数据源
    this.setData({
      array: temp_array,
      other_reason: e.currentTarget.dataset.msg,
    })
  },

  /**
   * 输入其它
   */
  otherErrorsReason: function(e) {
    if (e.detail.value.length > 0) {
      var temp_array = this.clearErrorReason()
      this.data.other_reason = e.detail.value
      //重新设置数据源
      this.setData({
        array: temp_array,
      })
    }
  },

  chooseImageHead(e) {
    var this_ = this;
    app.util.UploadImage("userinfo/upload", 'company_wrong', function(data) {
      console.log(data);
      this_.setData({
        img_head: data.data
      })
    })
  },

  chooseImageBrand(e) {
    var this_ = this;
    app.util.UploadImage("userinfo/upload", 'company_wrong', function(data) {
      console.log(data);
      this_.setData({
        img_brand: data.data
      })
    })
  },

  post: function(e) {
    var that = this;
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) { })
    var params = e.detail.value;
    params.cause = that.data.other_reason;
    params.company_head = that.data.img_head;
    params.company_card = that.data.img_brand;
    params.bid = parseInt(this.data.info.bid);
    params.cid = parseInt(this.data.info.cid);
    params.msg = this.data.other_reason;
    params.c_name = that.data.info.name;
    params.b_name = that.data.info.b_name

    app.util.api('map/company_wrong', 'POST', params, function(data) {
      if (data.success == 1) {
        wx.showModal({
          title: '反馈成功',
          content: '我们将及时处理',
          showCancel: false,
          success: function(e) {
            if (e.confirm) {
              wx.navigateBack({
                delta: 1
              })
            }
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      info: options
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})