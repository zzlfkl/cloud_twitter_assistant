var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: ['房抵贷', '车抵贷', '信用贷', '薪金类', '过桥类', '租金类']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      navH: app.globalData.navHeight
    })
  },

  chooseImageHead(e) {
    var this_ = this;
    app.util.UploadImage("userinfo/upload", 'company_add', function(data) {
      console.log(data);
      this_.setData({
        img_head: data.data
      })
    })
  },

  chooseImageBrand(e) {
    var this_ = this;
    app.util.UploadImage("userinfo/upload", 'company_add', function(data) {
      console.log(data);
      this_.setData({
        img_brand: data.data
      })
    })
  },

  /**
   * 表单提交
   */
  caddpost: function(e) {
    var that = this;
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function(data) {})
    var params = e.detail.value;
    if (app.data.business_info == undefined) {
      wx.showToast({
        title: '请选择商圈！',
        icon: "none"
      })
      return;
    }
    params.company_head = that.data.img_head;
    params.company_card = that.data.img_brand;
    params.business_id = app.data.business_info.cid
    app.util.api('map/company_add', 'POST', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        wx.showModal({
          title: '您的添加申请已提交',
          content: data.msg,
          showCancel: false,
          success: function(e) {
            if (e.confirm) {
              wx.navigateBack({
                delta: 2
              })
            }
          }
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 选择商圈
   */
  business: function() {
    wx.navigateTo({
      url: 'chouse',
    })
  },
  /**
   * 选择公司性质
   */
  pro: function() {
    var that = this;
    wx.showActionSheet({
      itemList: that.data.list,
      success: function(res) {
        that.setData({
          com: that.data.list[res.tapIndex]
        })
      },
      fail: function(res) {
        console.log(res.errMsg)
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (app.data.business_info !== undefined) {
      this.setData({
        cid: app.data.business_info.cid,
        cname: app.data.business_info.cname
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
})