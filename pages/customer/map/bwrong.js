var app  = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [],
    other_reason: '',
  },
  /**
  * 清除所有选中
  */
  clearErrorReason: function () {
    var temp_array = this.data.array;
    for (var idx = 0; idx < temp_array.length; idx++) {
      temp_array[idx].isSelected = false;
    }
    return temp_array
  },
   /**
  * 选中某一个
  */
  simple_item_tap: function (e) {
    var temp_array = this.clearErrorReason()
    //改变属性值
    temp_array[e.currentTarget.dataset.idx].isSelected = true;
    //重新设置数据源
    this.setData({
      array: temp_array,
      other_reason: e.currentTarget.dataset.msg,
    })
  },
  /**
  * 输入其它
  */
  otherErrorsReason: function (e) {
    if (e.detail.value.length > 0) {
      var temp_array = this.clearErrorReason()
      this.data.other_reason = e.detail.value
      //重新设置数据源
      this.setData({
        array: temp_array,
      })
    }
  },
  /**
   * 提交
   */
  post:function(e){
    var that = this;
    
    // 搜集表单，推送消息
    app.util.CollectFormId(e.detail.formId,app.data.userinfo.id,function (data) {
      if (data.success == 0) {
        console.log(data)
      }
    })
    var params = e.detail.value;
    params.business_id = parseInt(that.data.info.id);
    params.msg = that.data.other_reason;
    params.b_name = that.data.info.name;
    app.util.api('map/business_wrong','POST',params,function(data){
      if(data.success == 1){
        wx.showModal({
          title: '反馈成功',
          content: '我们将及时处理',
          showCancel:false,
          success:function(e){
            if(e.confirm){
              wx.navigateBack({
                delta:1
              })
            }
          }
        })
      }else{
        wx.showToast({
          title: data.msg,
          icon:'none'
        })
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      info: options,
      navH: app.globalData.navHeight,
      array: [
        {
          "title": "商圈不存在",
          "selectedTitle": "商圈不存在",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "商圈已搬迁",
          "selectedTitle": "商圈已搬迁",
          "matchingId": "2",
          "isSelected": false
        },
        {
          "title": "商圈地址有误",
          "selectedTitle": "商圈地址有误",
          "matchingId": "2",
          "isSelected": false
        },
      ]
    })
  },
})