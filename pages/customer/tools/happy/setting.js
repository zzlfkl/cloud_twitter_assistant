var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemDataArray: [{
        name: "company_name",
        title: "公司简称：",
        hint: "",
        placeholder: '请输入公司简称',
        block: false
      },
      {
        name: "advantage",
        title: "宣传口号：",
        hint: "",
        placeholder: '请输入宣传口号',
        block: true
      },
      {
        name: "real_name",
        title: "人员姓名：",
        placeholder: '请输入人员姓名',
        hint: "",
        block: false
      },
      {
        name: "position",
        title: "岗位名称：",
        placeholder: '请输入岗位名称',
        hint: "",
        block: false
      },
      {
        name: "money",
        title: "放款金额：",
        placeholder: '请输入放款金额',
        hint: "万",
        block: false
      }, {
        name: "pro_name",
        title: "产品名称：",
        placeholder: '请输入产品名称',
        hint: "",
        block: false
      },
      {
        name: "mon_interest",
        title: "产品月息：",
        placeholder: '请输入产品月息',
        hint: "%",
        block: false
      }
    ],
  },

  // 喜报设置提交
  createSub: function(e) {
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
    var happySetting = e.detail.value;
    if (happySetting.company_name.length > 6) {
      wx.showToast({
        title: '公司简称不得大于6位！',
        icon: 'none'
      })
      return;
    }
    happySetting.logo = this.data.setting.logo;
    happySetting.create_time = this.data.setting.create_time;
    happySetting.mon_interest = happySetting.mon_interest;
    happySetting.money = happySetting.money;
    wx.setStorageSync('happySetting', happySetting)
    wx.navigateBack();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    var setting = JSON.parse(options.setting);
    console.log(setting)
    var data = this.data.itemDataArray;
    
    data[0].value = setting.company_name;
    data[1].value = setting.advantage;
    data[2].value = setting.real_name;
    data[3].value = setting.position;
    data[4].value = setting.money;
    data[5].value = setting.pro_name;
    data[6].value = setting.mon_interest;

    that.setData({
      setting: setting,
      itemDataArray: data
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})