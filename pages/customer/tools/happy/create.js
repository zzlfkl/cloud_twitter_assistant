var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    choose: 0,
    index: 0,
    style: [{
        name: '主题一',
        id: 'happyOne',
        image: '/img/customer/one.png',
      },
      {
        name: '主题二',
        id: 'happyTwo',
        image: '/img/customer/two.png',
      },
      {
        name: '主题三',
        id: 'happyThree',
        image: '/img/customer/three.png',
      },
      {
        name: '主题四',
        id: 'happyFour',
        image: '/img/customer/four.png',
      },
    ],
    url: []
  },

  // 改变样式
  changeStyle: function(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    that.setData({
      index: index,
      choose: index
    })
    setTimeout(function() {
      if (index == 1) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index, 0.7, 0.59, 'black')
      } else if (index == 2) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index, 0.8, 0.62, '#C4A93C')
      } else if (index == 0) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index)
      } else if (index == 3) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index, 0.8, 0.63, "white", "#3d5ebb", "white", "#00fbfb")
      }
    }, 50)
  },

  // 预览分享
  share: function() {
    var change = this.data.style[this.data.index].id;
    var path = this.data[change];
    wx.previewImage({
      current: path, // 当前显示图片的http链接
      urls: [path] // 需要预览的图片http链接列表
    })
  },

  // 保存图片
  save: function() {
    wx.showLoading({
      title: '保存中...'
    })
    var change = this.data.style[this.data.index].id;
    var path = this.data[change];
    wx.saveImageToPhotosAlbum({
      filePath: path,
      success: function(res) {
        console.log(res)
        wx.hideLoading();
        if (res.errMsg) {
          wx.showToast({
            title: '已保存至相册',
          })
        }
      },
      fail: function(res) {
        wx.hideLoading();
        console.log(res)
      }
    })
    console.log(change)
    console.log(this.data[change]);
  },

  // 配置项
  edit: function() {
    wx.navigateTo({
      url: '/pages/customer/tools/happy/setting?setting=' + JSON.stringify(this.data.info),
    })
  },

  //绘制图片
  drawImage: function(id, url, index, timeW = 0.8, timeH = 0.63, timeC = 'white', lineColor = 'red', positionColor = 'black', nameColor = 'red', companyColor = 'white') {
    var that = this;
    var happy = wx.createCanvasContext(id);
    happy.drawImage(url, 0, 0, that.data.Width * 0.93, that.data.Height * 0.8);

    if (that.data.qrcode) {
      wx.getImageInfo({
        src: that.data.qrcode,
        success: function(res) {
          happy.drawImage(res.path, that.data.Width * 0.38, that.data.Height * 0.65, 70, 70 * res.height / res.width);

          happy.draw(true, function() {
            setTimeout(function() {
              wx.canvasToTempFilePath({
                canvasId: id,
                success: function(res) {
                  that.setData({
                    [id]: res.tempFilePath
                  })
                }
              })
            });
          }, 50)
        }
      })
    }

    // 公司名称
    happy.setFontSize(18)
    happy.setFillStyle(companyColor);
    happy.setTextAlign('center');
    happy.fillText(that.data.info.company_name ? that.data.info.company_name : '', that.data.Width * 0.48, that.data.Height * 0.08)


    // 字体大小
    happy.setFontSize(15)
    happy.setLineWidth(0.2)
    happy.setLineCap('round')
    happy.setStrokeStyle(lineColor)

    // 底部线条
    happy.moveTo(that.data.Width * 0.19, that.data.Height * 0.41)
    happy.lineTo(that.data.Width * 0.74, that.data.Height * 0.41)
    happy.moveTo(that.data.Width * 0.19, that.data.Height * 0.46)
    happy.lineTo(that.data.Width * 0.74, that.data.Height * 0.46)
    happy.moveTo(that.data.Width * 0.19, that.data.Height * 0.51)
    happy.lineTo(that.data.Width * 0.74, that.data.Height * 0.51)
    happy.moveTo(that.data.Width * 0.19, that.data.Height * 0.56)
    happy.lineTo(that.data.Width * 0.74, that.data.Height * 0.56)
    happy.stroke()

    // 职位信息
    happy.setTextAlign('left');
    happy.setFillStyle(positionColor)
    happy.fillText(that.data.info.position ? that.data.info.position + '：' : '无（职位）', that.data.Width * 0.2, that.data.Height * 0.4)
    happy.fillText('放款金额：', that.data.Width * 0.2, that.data.Height * 0.45)
    happy.fillText('产品名称：', that.data.Width * 0.2, that.data.Height * 0.5)
    happy.fillText('产品月息：', that.data.Width * 0.2, that.data.Height * 0.55)

    // 贷款信息
    happy.setTextAlign('right');
    happy.setFillStyle(nameColor)
    happy.fillText(that.data.info.real_name, that.data.Width * 0.7, that.data.Height * 0.4)
    happy.fillText(that.data.info.money + '万', that.data.Width * 0.7, that.data.Height * 0.45)
    happy.fillText(that.data.info.pro_name ? that.data.info.pro_name : '无', that.data.Width * 0.7, that.data.Height * 0.5)
    happy.fillText(that.data.info.mon_interest ? that.data.info.mon_interest + '%' : '无', that.data.Width * 0.7, that.data.Height * 0.55)

    // 日期
    happy.setFontSize(14)
    happy.setFillStyle(timeC)
    happy.fillText(that.data.info.create_time, that.data.Width * timeW, that.data.Height * timeH)

    happy.draw(true, function() {
      setTimeout(function() {
        wx.canvasToTempFilePath({
          canvasId: id,
          success: function(res) {
            that.setData({
              [id]: res.tempFilePath
            })
          }
        })
      }, 50)
    });

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;

    if (wx.getStorageSync('happySetting')) {
      wx.removeStorageSync('happySetting')
    }

    wx.getSystemInfo({
      success: function(res) {
        var info = res;
        var params = {
          'type': 'qrcode'
        };
        app.util.api('userproduct/get_qr', 'GET', params, function(data) {
          console.log(data)
          if (data.success == 1) {
            that.setData({
              qrcode: data.data
            })
          }
          that.drawImage(that.data.style[0].id, that.data.style[0].image, 0, 0.7, 0.59, 'black')
        })

        that.setData({
          Height: info.screenHeight,
          Width: info.screenWidth,
          info: JSON.parse(options.info)
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.drawImage(this.data.style[this.data.index].id, this.data.style[this.data.index].image, this.data.index)

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    if (wx.getStorageSync('happySetting')) {
      that.setData({
        info: wx.getStorageSync('happySetting')
      })

      var index = that.data.index;
      if (index == 1) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index, 0.43, 0.59, 'black')
      } else if (index == 2) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index, 0.49, 0.62, '#C4A93C')
      } else if (index == 0) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index)
      } else if (index == 3) {
        that.drawImage(that.data.style[index].id, that.data.style[index].image, index, 0.51, 0.63, "white", "#3d5ebb", "white", "#00fbfb")
      }

    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var change = this.data.style[this.data.index].id;
    var path = this.data[change];
    return {
      title: '喜报',
      imageUrl: path,
      success: function(e) {
        console.log(e)
      }
    }
  }
})