// pages/customer/tools/list.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemDataArray: [{
      title: "计算器",
      image: "/img/customer/record_purpose.png",
      hint: "",
      bottomLine: true,
      topLine: true,
      rightArrow: true,
      identify: "0",
      show: true
    }, {
      title: "喜报设置",
      image: "/img/customer/record_ing.png",
      hint: "",
      bottomLine: true,
      topLine: false,
      rightArrow: true,
      identify: "1",
      show: true
    }],
  },

  // 分类点击
  itemTap: function (e) {
    var identify = e.currentTarget.dataset.identify
    if (app.data.userinfo.user_status !== 1) {
      wx.navigateTo({
        url: '/pages/user/pre_personal_authentication/index',
      })
      // wx.showModal({
      //   title: '精英未认证',
      //   content: '您还未进行精英认证，现在去认证吗？',
      //   confirmText: '去认证',
      //   cancelText: '再想想',
      //   success: function (e) {
      //     if (e.confirm) {
      //       wx.navigateTo({
      //         url: '/pages/user/personal_authentication/personal_authentication',
      //       })
      //     }
      //   }
      // })
      return;
    }
    if (identify == 0) {
      wx.navigateTo({
        url: '/pages/customer/tools/manth/repay_calculate',
      })
    } else if (identify == 1) {
      wx.navigateTo({
        url: '/pages/customer/tools/happy/create',
      })
    } 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})