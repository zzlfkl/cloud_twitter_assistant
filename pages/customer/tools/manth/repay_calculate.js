// pages/tools/repay_calculate/repay_calculate.js
var loan_file = require('../../../../utils/loan.js')
var date_util = require('../../../../utils/date_util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: { 
    pay_ways: ["等本等息", "等额本息", "等额本金", ],
    pay_ways_selected: "等本等息", 
    loan_modle : null,
    month_interest: 0,
    loan_result: null,
    isShow: false
  },
  
  /**
   * 贷款金额
   * */ 
  watchAmount: function (e) {
    var temp_model = this.data.loan_modle
    temp_model.amount = e.detail.value 
    this.reloadData(temp_model) 
  },

  /**
   * 贷款年限
   * */
  watchYear: function (e) {
    var temp_model = this.data.loan_modle
    temp_model.year = e.detail.value
    this.reloadData(temp_model) 
  },

 /**
   * 贷款月息
   * */
  watchMonthInterest: function (e) {
    var temp_model = this.data.loan_modle
    temp_model.interest = (e.detail.value * 12).toFixed(2),
    this.reloadData(temp_model) 
  },

  /**
   * 贷款折扣
   * */
  watchDiscount: function (e) {
    var temp_model = this.data.loan_modle
    temp_model.discount = e.detail.value.toFixed(2),
    this.reloadData(temp_model)
  }, 

  /**
   * 贷款年息
   * */
  watchInterest: function (e) {
    var temp_model = this.data.loan_modle
    temp_model.interest = e.detail.value
    this.reloadData(temp_model) 
  },

  /**
   * 改变日期
   */
  bindDateChange: function (e) { 
    var temp_model = this.data.loan_modle
    temp_model.changeStartDate(e.detail.value) 
    this.reloadData(temp_model) 
  }, 

  /**
   * 更换支付方式
   */
  payWayChange: function (e) {
    var that = this
    wx.showActionSheet({
      itemList: this.data.pay_ways,
      success: function (res) {    
        var temp_model = that.data.loan_modle
        temp_model.pay_way = res.tapIndex
        that.setData({
          pay_ways_selected: that.data.pay_ways[res.tapIndex],
        })
        that.reloadData(temp_model)   
      },
      fail: function (res) {
        
      }
    })
  },

  reloadData: function (modle) { 
    this.setData({
      loan_modle: modle,
      month_interest: (modle.interest / 12).toFixed(2),
      isShow: modle.verify(),
      loan_result: modle.calculation()
    })   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) { 
    console.log(this.data.loan_result)
    // var modle = new loan_file.Loan('', '', 4.9, 1, date_util.convertStringFromDate(new Date(), 'yyyy-MM'), 0)
    var modle = new loan_file.Loan('', '', 4.9, 1, "2018-05", 0)
    this.reloadData(modle)
  }
})