// pages/pay/index.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  // 开始支付
  payActivity: function (e) {
    console.log(e.detail)
    wx.showToast({
      title: e.detail.pwd,
    })
    var that = this
    setTimeout(function() {
      that.popviewPay.setData({
        pwd: ''
      })
    }, 2000)
  },

  hiddenPopup: function () {
    console.log('hiddenPopup')
  },
  
  showPopup: function () {
    console.log('showPopup')
  },

  showPay: function () {
    this.popviewPay.showPopup();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.popviewPay = this.selectComponent("#popviewPay");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})