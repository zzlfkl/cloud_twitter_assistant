// pages/pay/pay.js
var app = getApp()
Component({
  
  /**
   * 组件的属性列表
   */
  properties: {  
    userBalance:{
      type: String,
      value: '',
    },
    totalMoney: {
      type: String,
      value: '',
    },
    enoughBalance: {
      type: Boolean,
      value: true,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    flag: true,
    pwd: '',
    input_focus: false,
    someData: {},
    showDetail: true,
    payHeight: app.globalData.sysHeight*0.8,
    hide_over:false
  },

  /**
   * 组件的方法列表
   */
  methods: {

    // 隐藏弹窗
    hidePopup () {
      this.setData({
        hide_over: true
      })
      this.back();
      this.animations = wx.createAnimation({
        duration: 400,
        timingFunction: 'ease',
        delay: 0
      });
      var that = this
      this.animations.translateY(app.globalData.sysHeight*0.8).step({ duration: 400 })
      this.setData({
        //输出动画
        animations: that.animations.export()
      });
      setTimeout(function () {
        that.setData({
          flag: !that.data.flag
        });
      }, 200);
      this.hiddenKeyboard();
      this.triggerEvent("hiddenPopup")
    },

    // 展示弹框
    showPopup() {
      this.setData({
        flag: !this.data.flag,
        hide_over: false
      })
      this.animations = wx.createAnimation({
        duration: 400,
        timingFunction: 'ease',
        delay: 0
      });
      var that = this
      this.animations.translateY(-app.globalData.sysHeight * 0.8).step({ duration: 400 })
      this.setData({
        //输出动画
        animations: that.animations.export()
      })
      this.triggerEvent("showPopup")
    },
    
    // 隐藏键盘
    hiddenKeyboard () {
      this.setData({
        input_focus: false
      })
    },
    
    // 返回上一页
    back() {
      this.animation = wx.createAnimation({
        duration: 400,
        timingFunction: 'ease',
        delay: 0
      });
      var that = this
      this.animation.translate(0, 0).step({ duration: 400 })
      this.setData({
        //输出动画
        animation: that.animation.export()
      })
    },

    // 开始支付
    startPay() {
      if(app.data.userinfo.pay_pass == null){
        wx.showModal({
          title: '支付密码未设置',
          content: '请先设置支付密码',
          showCancel:false,
          success:function(res){
            if(res.confirm){
              wx.navigateTo({
                url: '/pages/pay/pwd/index',
              })
            }
          }
        })
        return;
      }
      this.animation = wx.createAnimation({
        duration: 400,
        timingFunction: 'ease',
        delay: 0
      });
      var that = this
      this.animation.translate(-app.globalData.sysWidth, 0).step({ duration: 400 })
      this.setData({
        //输出动画
        animation: that.animation.export()
      })
      this.becomeFocus();
    },

    // 获取焦点
    becomeFocus() {
      this.setData({
        input_focus: true
      })
    },

    // 输入密码
    bindKeyInput(e) {
      this.setData({
        pwd: e.detail.value
      })
      if (this.data.pwd.length == 6) {
        var pwd = this.data.pwd
        this.triggerEvent("payActivity", { pwd })
      }
    },

    // 充值
    _charge(e) { 
      var url = '/pages/user/wallet/change/inc'
      if (app.globalData.userSys == 'iOS') {
        url = '/pages/user/wallet/index/main'
      }
      wx.navigateTo({ url })
    },
    
    customMethod() { 
      
    }

  }
})
