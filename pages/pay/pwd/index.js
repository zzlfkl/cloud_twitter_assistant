// pages/pay/pwd/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pwd: '',
    input_focus: true,
    notice:'请设置支付密码'
  },

  // 获取焦点
  becomeFocus() {
    console.log('becomeFocus')
    this.setData({
      input_focus: true
    })
  },

  // 输入密码
  bindKeyInput(e) {
    this.setData({
      pwd: e.detail.value
    })
    if (this.data.pwd.length == 6 && this.data.first == undefined) {
      var pwd = this.data.pwd;
      this.setData({
        first:pwd,
        pwd:'',
        notice: '请再次输入，以确认密码'
      })
    }

    if (this.data.pwd.length == 6 && this.data.first !== undefined){
      var pwd = this.data.pwd;
      if (pwd !== this.data.first) {
        wx.showToast({
          title: '两次输入密码不一致',
          icon: 'none'
        })
        setTimeout(function(){
          wx.redirectTo({
            url: '/pages/pay/pwd/index',
          })
        },500)
      }
    }
  },

  confirm:function(){
    var this_ = this;
    var params = {
      new_pay_pass: this.data.pwd
    }
    app.util.api("userinfo/reset_pay_pass_post", 'POST', params, function (data) {
      console.log(data);
      if (data.success) {
        wx.showToast({
          title: '设置成功',
        });
        this_.loading();
        setTimeout(function () {
          if (this_.data.origin !== undefined && this_.data.origin == 'code') {
            wx.navigateBack({ delta: 2})
          }else{
            wx.navigateBack({})
          }
        }, 500)
      }
    })
  },

  /**
     * 获取用户信息
     */
  loading: function () {
    var that = this;
    var params = {
      'type': 'userinfo'
    };
    app.util.api('userinfo/center', 'POST', params, function (data) {
      if (data.success == 1) {
        app.data.userinfo = data.data;
        that.setData({
          userinfo: data.data,
        })
        //若无公司信息则获取
        if (app.data.userinfo.company_info == undefined) {
          app.util.set_company_info();
        }
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.origin !== undefined && options.origin == 'code'){
      this.setData({
        origin:'code'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})