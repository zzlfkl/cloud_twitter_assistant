var app = getApp();
var QQMapWX = require('../../../common/qqmap-wx-jssdk.js');
var demo = new QQMapWX({
  key: '4FWBZ-C3U3G-N6IQW-IRZNQ-LLZ2S-2QBJM' // 必填
});

Page({
  data: {
    locateCity:'定位中...',
    //下面是字母排序
    letter: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
    cityListId: '',
    
  },

  // 接入微信小程序JavaScript SDK，定位当前城市
  getlocaltion() {
    var this_ = this;
    demo.reverseGeocoder({
      success: function (res) {
       
        if (res.status == 0) {
          var city = res.result.address_component.city;
          this_.setData({
            locateCity: city,
            locateCode: res.result.ad_info.adcode
          })
        }
      },
      fail: function (res) {
       
        this_.setData({
          locateCity: false
        })
      }
    });
  },

  // 重新定位
  retry:function(){
    this.getlocaltion();
  },

  //点击城市
  cityTap(e) {
    var cityInfo = e.currentTarget.dataset;
    console.log(cityInfo)
    // 点击来源为客户发布
    if(this.data.kind !== undefined){
      if (this.data.kind == 'city_location'){
        app.data.city_location = cityInfo;
      }
      if (this.data.kind == 'census_register') {
        app.data.census_register = cityInfo;
      }
    }else{
      app.data.globalCity = cityInfo;
      // 存入历史记录
      var params = {
        'userId': app.data.userinfo.id,
        'cityCode': cityInfo.val,
        'cityName': cityInfo.name
      }
      app.util.api('index/choose_city_his', 'POST', params, function (data) { });
    }
    wx.navigateBack()
  },
  //点击城市字母
  letterTap(e) {
    const Item = e.currentTarget.dataset.item;
    this.setData({
      cityListId: Item
    });
  },
  
  onLoad:function(options){
    var this_ = this;
    if(options.kind !== undefined){
      this_.setData({
        kind:options.kind
      })
    };
    this_.setData({
      navH: app.globalData.navHeight
    })
    this_.getlocaltion();
    var params = {
      userId: app.data.userinfo.id
    }
    app.util.api("index/get_city", 'POST', params, function (data) {
      this_.setData({
        citylist:data.data.city_list,
        newcity:data.data.hot_city,
        hiscity:data.data.his_city
      })
    })
  },

  onShow() {
    
  }
})