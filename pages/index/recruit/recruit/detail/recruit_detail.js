// pages/tools/recruit/detail/recruit_detail.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    homeHide: true,
  },
  /**
   * 获取招聘详情
   */
  detail:function(id){
    var that = this;
    var params = {
      id:id
    };
    app.util.api('employee/detail','GET',params,function(data){
      if(data.success == 1){
          that.setData({
            detail:data.data
          })
      }else{
        wx.showToast({
          title: data.msg,
          icon:'none',
          duration:1500
        })
        wx.navigateBack({
          delta:1
        })
      }
    })
  },
  /**
   * 拨打电话
   */
  call:function(e){
    var mobile = e.currentTarget.dataset.mobile;
    wx.makePhoneCall({
      phoneNumber: mobile 
    })
  },
  /**
   * 打开工作地址位置
   */
  check_location:function(e){
    var lat = e.currentTarget.dataset.lat;
    var lng = e.currentTarget.dataset.lng;
    wx.openLocation({
      latitude: parseFloat(lat),
      longitude: parseFloat(lng),
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      emp_id: options.id
    })
    // 来源分享
    if (options.type !== undefined) {
      that.setData({
        homeHide: false
      })
    }
    if (app.data.userinfo == undefined) {
      app.util.login(function (data) {
        if (data.success == 1) {
          wx.setStorageSync('token', data.other);
          app.data.userinfo = data.data;
          that.setData({
            userInfo: data.data
          })
          that.detail(options.id);
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
        }
      })
    } else {
      that.detail(options.id);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.detail(this.data.emp_id);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
    return {
      title: '人才市场-' + this.data.detail.name,
      path: '/pages/index/recruit/recruit/detail/recruit_detail?type=share&id=' + this.data.detail.id,
    }
  }
})