// pages/tools/recruit/list/recruit_list.js
var app  = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    recruit_array: [],
    page:1,
    list: [],
    loading: true,
    complete: true,
    show: true
  },

  /**
   * 显示招聘详情
   */
  detail: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/recruit/recruit/detail/recruit_detail?id='+id,
    })
  },
  
  /**
   * 发布招聘
   */
  release: function (e) {
    wx.navigateTo({
      url: '/pages/index/recruit/recruit/release/recruit_release',
    })
  },
  /**
   * 获取招聘列表
   */
  list:function(){
    var that   = this;
    var params = {
        page:that.data.page
    }
    app.util.api('employee/emp_list','GET',params,function(data){
      console.log(data)
      if(data.success == 1){
        if(data.data.length < 10){
          that.setData({
            loading: true,
            complete: false,
            show: true
          })
        }else{
          that.setData({
            loading: false,
            complete: true,
            show: true
          }) 
        }
        that.setData({
          list: that.data.list.concat(data.data) 
        })
      } else {
        if(that.data.list.length !== 0){
           that.setData({
             show:true
           }) 
        }else{
          that.setData({
            show: false
          })
        }
        that.setData({
          loading: true,
          complete: false,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.list();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
      this.setData({
        list:[],
        page:1
      });
      this.list();
      wx.stopPullDownRefresh();
  },  

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      page:this.data.page + 1
    });
    this.list();
  }
})