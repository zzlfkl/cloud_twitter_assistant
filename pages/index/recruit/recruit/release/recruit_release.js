// pages/tools/recruit/release/recruit_release.js
var app  = getApp();
var QQMapWX = require('../../../../../common/qqmap-wx-jssdk.js');
var demo = new QQMapWX({
  key: '4FWBZ-C3U3G-N6IQW-IRZNQ-LLZ2S-2QBJM' // 必填
});
Page({

  /**
   * 页面的初始数据
   */
  data: {
    item_array: [],
    name:'请选择'
  },

  /**
   * 滚轮选择
   */
  pickerChange: function (e) {
    var temp_array = this.data.item_array
    var temp_item = e.currentTarget.dataset.item
    for (var idx = 0; idx < temp_array.length; idx++) {
      if (temp_array[idx].identify == temp_item.identify) {
        // 复制给temp_array
        temp_array[idx].sub_obj_titles_value = e.detail.value; 
        temp_array[idx].item_input = temp_array[idx].sub_obj_titles[e.detail.value];
        break
      }
    }
    // 赋值
    this.setData({
      item_array: temp_array,
    })
  },

  /**
   * 打开地图选择位置
   */
  getlocaltion() {
    var that = this;
    var temp_array = that.data.item_array
     // 地图选择
    wx.chooseLocation({
      success: function (res) {
        that.setData({ 
          name: res.name,
          address: res.address,
          lng: res.longitude,
          lat: res.latitude
        })
        //获取定位城市名称 
        that.setData({
          city_code: app.data.globalCity.val,
          city: app.data.globalCity.name
        })
        for (var idx = 0; idx < temp_array.length; idx++) {
          if (temp_array[idx].identify == 'location') {
            temp_array[idx].item_input = res.name
          }
          if (temp_array[idx].identify == 'detail_address') {
            temp_array[idx].item_input = res.address
          }
          that.setData({
            item_array: temp_array
          })
        }
        
      },
      fail: function () {
        wx.showToast({
          title: '未选择位置',
          icon: 'none'
        })
      },
    })
  },
  /**
   * view点击事件
   */
  choose: function (res) {
    var that = this;
    wx.getSetting({
      complete: function (res) {
        if (res.authSetting['scope.userLocation'] == undefined || res.authSetting['scope.userLocation']) {
          that.getlocaltion();
        } else {
          wx.openSetting({
            success: function (res) {
              if (res.authSetting['scope.userLocation']) {
                that.getlocaltion();
              } else {
                wx.navigateBack({
                  delta: 1
                })
              }
            },
            fail: function (res) {
              console.log(res)
            },
          })
        }
      }
    })

  },
  showId:function(e){
 
  },
  /**
   * 数据提交
   */
  submit:function(e){
   
    var that = this;
    var data = that.data.item_array;
    var params = {};
    
    //获取表单ID推送消息
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
  
    for(var i = 0;i < data.length;i++){
      if (data[i].item_input == '请选择' || data[i].item_input == "请输入详细地址"){
        wx.showToast({
          title: '数据不完整！',
          icon: 'none'
        })
        return;
      }
      params[data[i].identify]  = data[i].item_input
    }
    if (that.data.description == undefined) {
      wx.showToast({
        title: '请输入职位描述',
        icon: 'none'
      })
      return;
    }

    //整合参数
    params['city'] = that.data.city ? that.data.city:'未知';
    params['city_code'] = that.data.city_code ? that.data.city_code : '未知';
    params['lng'] = that.data.lng;
    params['lat'] = that.data.lat;
    params['description'] = that.data.description;

    //发起网络请求
    app.util.api('employee/emp_pub','POST',params,function(data){
    
        if(data.success == 1){
          wx.showModal({
            title: data.msg,
            content: '请等待审核',
            showCancel:false,
            success:function(res){
              if(res.confirm){
                wx.navigateBack({
                  delta:1
                })
              }
            }
          })
        }else{
          wx.showModal({
            title: '发布失败',
            content: data.msg,
          })
        }
    })
  },
  /**
   * 获取文本域内容
   */
  get_content:function(e){
    this.setData({
      description: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 处理数据
    var temp_array = app.config.recruit()
    for (var idx = 0; idx < temp_array.length; idx++) {
      if (temp_array[idx].identify == 'location') {
        temp_array[idx].item_input = this.data.name
      }
      var sub_obj = app.config.recruit_subList(temp_array[idx])
      temp_array[idx].sub_obj = sub_obj 
      var sub_obj_titles = []
      var sub_temp_array = sub_obj.array
      if (sub_temp_array) {
        for (var sub_idx = 0; sub_idx < sub_temp_array.length; sub_idx++) {
          sub_obj_titles.push(sub_temp_array[sub_idx].title);
        }
      } 
      temp_array[idx].sub_obj_titles = sub_obj_titles;
      temp_array[idx].sub_obj_titles_value = -1;
    }
    // 赋值
    this.setData({
      item_array: temp_array,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})