// pages/tools/recruit/manage/manage.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    item_array:[],
    page: 1
  },

  publishEmployee:function(){
    wx.navigateTo({
      url: '/pages/index/recruit/recruit/release/recruit_release',
    })
  },
  /**
   * 提示通过后可查看详情
   */
  notice:function(){
    wx.showToast({
      title: '审核通过后可查看详情',
      icon:'none'
    })
  },
  /**
   * 招聘详情
   */
  showRecruitDetail:function(e){
    var emp_id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/recruit/recruit/detail/recruit_detail?id=' + emp_id,
    })
  },
  /*
  请求数据
  * */
  forData: function (page) {
    var that = this
    var params = {
      page: page
    }
    app.util.api('employee/emp_mine', 'GET', params, function (data) {
        if(data.success == 1){
          that.setData({
            item_array: that.data.item_array.concat(data.data),
          })
          if(data.data.length == 0 && that.data.page == 1){
              that.setData({
                show:false,
              })
          }else{
            that.setData({
              show: true,
            })
          }
        }   
    })
  },
  /**
   * 删除招聘信息
   */
  employeeDelete:function(e){
    var that = this;
    var emp_id = e.currentTarget.dataset.id;
    var params = {
      emp_id:emp_id
    };

    // 询问
    wx.showModal({
      title: '删除',
      content: '将会删除此信息，请谨慎操作',
      success:function(res){
        if(res.confirm){
          // 删除招聘信息（修改发布状态为0）
          app.util.api('employee/emp_del', 'POST', params, function (data) {
  
            if (data.success == 1) {
              that.setData({
                item_array: []
              })
              that.forData(1);
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.forData(this.data.page)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      item_array: [],
      page:1
    })
    this.forData(this.data.page);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.item_array.length !== 0){
      this.setData({
        page: this.data.page + 1
      })
      this.forData(this.data.page);
    }
  },
  
})