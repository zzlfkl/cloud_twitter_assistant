var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    homeHide: true,
    kind: 2,
    progress: [{
        src: '/img/progress/one.png',
        content: '贷款申请',
        lei: 'luc',
        hide: false
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        hide: false
      },
      {
        src: '/img/progress/two.png',
        content: '贷前调查',
        lei: 'luc',
        hide: true
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        hide: true
      },
      {
        src: '/img/progress/three.png',
        content: '资质审核',
        lei: 'luc',
        hide: false
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        hide: false
      },
      {
        src: '/img/progress/four.png',
        content: '实地调查',
        lei: 'luc',
        hide: true
      },
      {
        src: '/img/progress/next.png',
        content: '',
        lei: 'next',
        hide: true
      },
      {
        src: '/img/progress/five.png',
        content: '放款结算',
        lei: 'luc',
        hide: false
      }
    ],
    winWidth: 0,
    winHeight: 0,
    currentTab: 0,
    flag: false,
    credit: [{
        name: '抵押贷',
        value: '1',
        checked: false
      },
      {
        name: '信用贷',
        value: '2',
        checked: false
      },
      {
        name: '大数据',
        value: '3',
        checked: false
      },
      {
        name: '信用卡',
        value: '4',
        checked: false
      },
      {
        name: '过桥类',
        value: '5',
        checked: false
      },
      {
        name: '服务类',
        value: '6',
        checked: false
      },
      {
        name: '其他',
        value: '7',
        checked: false
      },
    ],
    pro_condition: [{
        name: '房产',
        value: '1',
        checked: false
      },
      {
        name: '车产',
        value: '2',
        checked: false
      },
      {
        name: '保单',
        value: '3',
        checked: false
      },
      {
        name: '营业执照',
        value: '4',
        checked: false
      },
      {
        name: '薪金流水',
        value: '5',
        checked: false
      },
      {
        name: '学历',
        value: '6',
        checked: false
      },
      {
        name: '其他',
        value: '7',
        checked: false
      },
    ],
    mechanism: [{
        name: '正规银行',
        value: '1',
        checked: false
      },
      {
        name: '消费金融',
        value: '2',
        checked: false
      },
      {
        name: '小贷公司',
        value: '3',
        checked: false
      },
      {
        name: '个人服务',
        value: '4',
        checked: false
      },
      {
        name: '其他',
        value: '5',
        checked: false
      },
    ],
    pay_kind: [{
        name: '等本等息',
        value: '1',
        checked: false
      },
      {
        name: '等额本息',
        value: '2',
        checked: false
      },
      {
        name: '等额本金',
        value: '3',
        checked: false
      },
      {
        name: '先息后本',
        value: '4',
        checked: false
      },
      {
        name: '后本后息',
        value: '5',
        checked: false
      },
      {
        name: '复合方式',
        value: '6',
        checked: false
      },
      {
        name: '其他',
        value: '7',
        checked: false
      },
    ],
  },

  hide: function() {
    this.setData({
      flag: true
    })
  },

  // 上架
  on: function(e) {
    var params = {
      user_id: getApp().data.userinfo.id,
      pro_id: e.currentTarget.dataset.id
    };
    app.util.api('userinfo/pro_on', 'GET', params, function(data) {
      if (data.success == 1) {
        wx.showToast({
          title: '上架成功',
        })
        setTimeout(function() {
          wx.navigateBack()
        }, 1000)
      }
    })
  },

  // 下架
  down: function(e) {
    var params = {
      user_id: app.data.userinfo.id,
      pro_id: e.currentTarget.dataset.id
    };
    app.util.api('userinfo/pro_down', 'GET', params, function(data) {
      if (data.success == 1) {
        wx.showToast({
          title: '下架成功',
        })
        setTimeout(function() {
          wx.navigateBack()
        }, 1000)
      }
    })
  },

  // 编辑
  edit: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/product/publish?id=' + id + '&type=wait',
    })
  },

  // 推广
  push: function(e) {
    var id = e.currentTarget.dataset.id;
    if (app.globalData.userSys == 'iOS') {
      wx.showModal({
        title: '暂不支持',
        content: '十分抱歉，由于相关规定，您暂时无法进行此操作',
        showCancel:false,
        confirmText:'我知道了'
      })
    }else{
      wx.navigateTo({
        url: '/pages/product/push/push?id=' + id,
      })
    }
  },

  /**
   * 产品选项卡
   */
  bindChange: function(e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
  },

  gotovip: function(e) {
    wx.navigateTo({
      url: '/pages/user/vip/vip/vip_center',
    })
  },

  // 拨打电话
  phonecall: function(e) {
    if (!app.util.checkvip(app.data.userinfo)) {
      return;
    }
    wx.makePhoneCall({
      phoneNumber: this.data.product.mobile
    })
  },

  // 用户收藏操作
  star: function(e) {
    var that = this;
    var params = {
      pro_id: e.currentTarget.dataset.id,
      user_id: getApp().data.userinfo.id,
      title: e.currentTarget.dataset.title
    };
    app.util.api('userinfo/collection', 'GET', params, function(data) {
      if (data.data == 1) {
        that.setData({
          star: 1
        })
      } else {
        that.setData({
          star: 0
        })
      }
    })
  },

  swichNav: function(e) {
    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },

  /**
   * 获取产品详情
   */
  detail: function(e) {
    var that = this;
    var params = {
      pro_id: that.data.id,
    };
    app.util.api('userproduct/pro_detail', 'GET', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        // 办理流程
        if (data.data.check_before) {
          that.data.progress[2].hide = false;
          that.data.progress[3].hide = false;
        } else {
          that.data.progress[2].hide = true;
          that.data.progress[3].hide = true;
        }
        if (data.data.check_after) {
          that.data.progress[6].hide = false;
          that.data.progress[7].hide = false;
        } else {
          that.data.progress[6].hide = true;
          that.data.progress[7].hide = true;
        }
        that.setData({
          progress: that.data.progress
        })

        if (data.data.has_collection == 1) {
          that.setData({
            star: 1
          })
        } else {
          that.setData({
            star: 0
          })
        }
        data.data.credit = data.data.credit.split(',');
        data.data.pro_condition = data.data.pro_condition.split(',');
        data.data.mechanism = that.data.mechanism[data.data.mechanism - 1];
        data.data.pay_kind = that.data.pay_kind[data.data.pay_kind - 1];
        for (var k = 0, length = data.data.credit.length; k < length; k++) {
          data.data.credit[k] = that.data.credit[data.data.credit[k] - 1];
        }
        for (var k = 0, length = data.data.pro_condition.length; k < length; k++) {
          data.data.pro_condition[k] = that.data.pro_condition[data.data.pro_condition[k] - 1]
        }
        if (data.data.limit_months < 12 && data.data.limit_months != 0) {
          data.data.limit_months = data.data.limit_months + '月';
        } else if (data.data.limit_months == 0) {
          data.data.limit_months = '其他';
        } else {
          data.data.limit_months = data.data.limit_months / 12 + '年';
        }
        if (data.data.open_days == 1) {
          data.data.open_days = '当日';
        } else if (data.data.open_days == 2) {
          data.data.open_days = '次日';
        } else if (data.data.open_days == 3) {
          data.data.open_days = '3天';
        } else if (data.data.open_days == 7) {
          data.data.open_days = '1周';
        } else if (data.data.open_days == 14) {
          data.data.open_days = '2周';
        } else if (data.data.open_days == 21) {
          data.data.open_days = '3周';
        } else if (data.data.open_days == 30) {
          data.data.open_days = '1月';
        } else {
          data.data.open_days = data.data.open_days + '日';
        }

        that.setData({
          product: data.data
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  // 图片预览
  priviewImg: function(e) {
    var imgList = this.data.product.img;
    var index = e.currentTarget.dataset.index;
    var arr = [];
    for (var i = 0; i < imgList.length; i++) {
      arr.push(imgList[i].url)
    }

    wx.previewImage({
      current: arr[index], // 当前显示图片的http链接
      urls: arr // 需要预览的图片http链接列表
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if (options.type !== undefined) {
      that.setData({
        homeHide: false
      })
    }
    that.setData({
      id: options.id,
    });

    if (options.status !== undefined) {
      that.setData({
        kind: options.status
      })
    }

    if (app.data.userinfo == undefined) {
      app.util.login(function(data) {
        if (data.success == 1) {
          app.data.userinfo = data.data;
          wx.setStorageSync('token', data.other);
          that.detail();
          if (app.data.userinfo.mobile == '') {
            wx.showModal({
              title: '绑定手机号后可查看',
              content: '现在去绑定吗？',
              success: function(res) {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/user/authlist',
                  })
                }
              }
            })
          }
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
        }
      })
    } else {
      wx.getSystemInfo({
        success: function(res) {
          that.setData({
            winWidth: res.windowWidth,
            winHeight: res.windowHeight,
            userinfo: app.data.userinfo
          })
          that.detail();
        }
      });
    }

  },

  call: function(e) {
    wx.makePhoneCall({
      phoneNumber: this.data.mobile
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (this.data.kind !== undefined && this.data.kind == 0) {
      this.detail();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.detail();
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    return {
      title: '云推客-让贷款更简单',
      path: '/pages/index/detail?type=share&user_id=' + app.data.userinfo.id + '&id=' + this.data.id,
      success: function(res) {
        var params = {
          "type": "share"
        }
        app.util.api('userinfo/share_vip', 'POST', params, function(data) {
          console.log(data)
        })
      },
      fail: function(res) {
        // 转发失败
      }
    }
  }
})