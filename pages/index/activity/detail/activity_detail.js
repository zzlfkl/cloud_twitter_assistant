var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    btn:false,
    homeHide: true,
    phone_width: 0,
    timeStamp: Date.parse(new Date()) / 1000
  },
  /**
   * 马上报名
   */
  join: function(e) {
    var that = this;
    var act_id = e.currentTarget.dataset.id;
    var params = {
      'act_id': act_id
    };
    wx.showModal({
      title: '报名参加',
      content: '确定要报名吗？',
      success: function(e) {
        if (e.confirm) {
          app.util.api('activity/act_join', 'POST', params, function(data) {
            if (data.success == 1) {
              wx.showModal({
                title: '报名成功',
                content: '请准时参加！',
                showCancel: false,
                success: function(e) {
                  if (e.confirm) {
                    wx.navigateBack({
                      delta: 1
                    })
                  }
                }
              })
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          })
        }
      }
    })
  },
  /**
   * 展示参与人
   */
  showUsers: function(e) {
    var act_id = e.currentTarget.dataset.act_id;
    wx.navigateTo({
      url: '/pages/index/activity/entered/activity_entered?act_id=' + act_id,
    })
  },
  /**
   * 获取活动内容
   */
  detail: function(id) {
    var that = this;
    var params = {
      'act_id': id
    };
    app.util.api('activity/act_detail', 'GET', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        that.setData({
          detail: data.data
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  makeCall:function(){
    wx.makePhoneCall({
      phoneNumber: this.data.detail.mobile.toString() //仅为示例，并非真实的电话号码
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var this_ = this;
    if (options.type !== undefined) {
      this_.setData({
        homeHide: false,
        btn: true
      })
    }
    if (app.data.userinfo == undefined) {
      app.util.login(function(data) {
        if (data.success == 1) {
          wx.setStorageSync('token', data.other);
          app.data.userinfo = data.data;
          this_.setData({
            userInfo: data.data
          })
          this_.detail(options.id);
        } else {
          wx.showToast({
            title: data.msg,
            icon: 'none',
          })
        }
      })
    } else {
      this_.detail(options.id);
    }

    this.setData({
      phone_width: 32 * 12,
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: '活动交流-' + this.data.detail.name,
      path: '/pages/index/activity/detail/activity_detail?type=share&id=' + this.data.detail.id
    }
  }
})