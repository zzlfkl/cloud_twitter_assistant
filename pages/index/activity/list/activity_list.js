var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading: true,
    complete: true,
    show: false,
    page: 1,
    list: [],
    timestamp: Date.parse(new Date()) / 1000,
  },
  /**
   * 活动发布
   */
  release: function(e) {
    wx.navigateTo({
      url: '/pages/index/activity/release/activity_release',
    })
  },

  detail: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/index/activity/detail/activity_detail?id=" + id,
    })
  },
  /**
   * 活动列表
   */
  list: function() {
    var that = this;
    var params = {
      'type': 'activity',
      'page': that.data.page
    };
    app.util.api('activity/new_act_list', 'GET', params, function(data) {
      console.log(data)
      if (data.success == 1) {
        that.setData({
          loading: false,
          complete: true,
          list: that.data.list.concat(data.data)
        })
        if (data.data.length <= 15) {
          that.setData({
            loading: true,
            complete: false,
          })
        }
      } else {
        that.setData({
          loading: true,
          complete: false,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(this.data.timestamp)
    this.list();
  },

  /** 
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      list: [],
      page: 1
    })
    this.list();
    wx.stopPullDownRefresh();
    wx.hideLoading();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.setData({
      page: this.data.page + 1
    })
    this.list();
  },

})