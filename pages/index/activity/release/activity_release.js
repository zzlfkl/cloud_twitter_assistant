var app  = getApp();
Page({

  /**
   * 页面的初始数据 
   */
  data: {
    start_time:'2018-01-01',
    start:'01-01',
    time_start: '00:00',
    time_end: '00:00',
    nowYear:'2018',
  },
  /**
   * 选择开始时间
   */
  bindTimeChange1: function (e) {
    this.setData({
      time_start: e.detail.value
    })
  },
  /**
   * 选择结束时间
   */
  bindTimeChange2: function (e) {
    this.setData({
      time_end: e.detail.value
    })
  },
  /**
   * 选择开始日期
   */
  bindDateChange1: function (e) {
    this.setData({
      start_time: e.detail.value,
      start: e.detail.value.substring(5)
    })
  },
  /**
   * 上传图片
   */
  upload: function (e) {
    var that = this;
    app.util.UploadImage('userinfo/upload', 'activity', function (data) {
      that.setData({
        url: data.data
      })
    }, function (data) {
      wx.showToast({
        title: data.msg,
        icon: 'none'
      })
    });
  },
  /**
   * 提交信息
   */
  act:function(e){
    app.util.CollectFormId(e.detail.formId, app.data.userinfo.id, function (data) { })
    if (!app.util.checkauth(app)){
      return;
    }
    var params        = e.detail.value;
    params.start_time = this.data.start_time +' '+this.data.time_start;
    params.end_time = this.data.start_time + ' ' + this.data.time_end;
    if(this.data.url !== undefined){
      params.picture = this.data.url; 
    }
    params.formId = e.detail.formId;
    params.city_code = app.data.globalCity.val;
    //网络请求
    app.util.api('activity/publish','POST',params,function(data){
      if(data.success == 1){
          wx.showModal({
            title: '提交成功',
            content: data.msg,
            showCancel:false,
            success:function(e){
              if(e.confirm){
                wx.navigateBack({
                  delta:1
                })
              }
            }
          })
      }else{
        wx.showToast({
          title: data.msg,
          icon:'none'
        })
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var date = new Date;
    var year = date.getFullYear(); 
    this.setData({
      nowYear:year.toString()
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})