// pages/tools/activity/manage/manage.js
var app = getApp();
Page({ 
  /**
   * 页面的初始数据
   */
  data: { 
    page:1,
    bar_item_width: 0,
    swiper_current: 0,
    show:true,
    bar_item_titles: [{
      "title": "我的发布",
      "selectedTitle": "我的发布",
      "matchingId": "0",
      "isSelected": true,
      'page': 1,
      item_array: [],  
    },{
        "title": "我的参与",
        "selectedTitle": "我的参与",
        "matchingId": "1",
        "isSelected": false,
        'page': 1,
        item_array: [],
      }],
    item_array: [],
  },

  // tab绑定点击事件
  barItemTap: function (e) {
    var temp_array = this.data.bar_item_titles;
    for (var idx = 0; idx < temp_array.length; idx++) {
      temp_array[idx].isSelected = false;
      if (temp_array[idx].matchingId == e.currentTarget.dataset.identify) {
        temp_array[idx].isSelected = true;
      }
    }
    // 重置swiper选项
    this.setData({
      swiper_current: e.currentTarget.dataset.identify,
      item_array:[],
      page:1
    })
    this.getActInfo(this.data.page);
    this.reloadData(temp_array);
  }, 

  // 刷新顶部tab数据
  reloadData: function (array) {
    this.setData({
      bar_item_titles: array,
      bar_item_width: app.globalData.sysWidth / array.length
    })
  },

  /**
   * 删除已过期报名活动
   */
  JoinDelete:function(e){
    var actId = e.currentTarget.dataset.id;
    var that = this;
    wx.showActionSheet({
      itemList: ['删除'],
      success:function(res){
  
        if (res.tapIndex == 0){
          var params = {
            'act_id': actId
          };
          app.util.api('activity/join_delete','POST',params,function(data){
              if(data.success == 1){
                that.setData({
                  item_array: []
                })
                that.getActInfo(1);
              }else{
                wx.showToast({
                  title: data.msg,
                  icon:'none',
                  duration:1500
                })
              }
          });
        }
      }
    })
  },
  /**
   * 获取服务端数据
   */
  getActInfo:function(page){
    var that = this;
    if (that.data.swiper_current == 0){
      var url = 'activity/act_mine'
    }else{
      var url = 'activity/act_join_mine'
    }
    var params = {
      'user_id': app.data.userinfo.id,
      'page' :page
    }
    app.util.api(url,'GET', params, function (data) {

      if (data.success == 1) {
        that.setData({
          item_array: that.data.item_array.concat(data.data),
        })
        if(data.data.length == 0 && that.data.page ==1){
          that.setData({
            show:false,
          })
        }else{
          that.setData({
            show: true,
          })
        }
      }
    });
  },

  /**
   * 活动发布
   */
  publishActivity:function(){
    wx.navigateTo({
      url: '/pages/index/activity/release/activity_release',
    })
  },
  /**
   * 取消活动
   */
  ActCancel:function(e){
    var that = this;
    var act_id = e.currentTarget.dataset.id;
    var params = {
      act_id:act_id
    }
    wx.showModal({
      title: '取消活动',
      content: '确认要取消该活动吗？',
      success:function(res){
        if(res.confirm){
          app.util.api('activity/act_cancel', 'POST', params, function (data) {
            if (data.success == 1) {
              that.setData({
                item_array: []
              })
              that.getActInfo(1);
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          })
        }
      }
    })
  },
  /**
   * 活动删除
   */
  ActDelte:function(e){
    var that = this;
    var act_id = e.currentTarget.dataset.id;
    var params = {
      act_id: act_id
    }
    wx.showModal({
      title: '删除活动',
      content: '删除后不可恢复，请谨慎操作！',
      success: function (res) {
        if (res.confirm) {
          app.util.api('activity/act_delete', 'POST', params, function (data) {
            if (data.success == 1) {
              that.setData({
                item_array: []
              })
              that.getActInfo(1);
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          })
        }
      }
    })
  },

  /**
   * 我的参与-活动详情
   */
  detail:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/index/activity/detail/activity_detail?id=" + id,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getActInfo(this.data.page);
    this.reloadData(this.data.bar_item_titles)
    this.forData();
  },

  /**
   * 请求数据
   */
  forData: function () { 
    var title_items = this.data.bar_item_titles[this.data.swiper_current] 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      page:1,
      item_array: []
    })
    this.getActInfo(this.data.page);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.item_array.length !== 0) {
      this.setData({
        page:this.data.page+1
      })
      this.getActInfo(this.data.page)
    }
  },
})