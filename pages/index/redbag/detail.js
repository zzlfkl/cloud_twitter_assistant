// pages/index/redbag/detail.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bag: {}
  },

  // 我的钱包
  wallet: function() {
    wx.navigateTo({
      url: '/pages/user/wallet/index/main',
    })
  },

  // 红包详情
  bag_detail: function() {
    var this_ = this;
    var params = {
      'bag_id': this_.data.bag_id
    }
    app.util.api("redbag/bag_detail", 'GET', params, function(data) {
      if (data.status == 200) {
        this_.setData({
          detail: data.data
        })
      } else {
        wx.showToast({
          title: '该红包已失效',
          icon: 'none'
        })
        setTimeout(function() {
          wx.navigateBack()
        }, 1000)
      }
    })
  },

  // 广告产品
  ad_pro: function() {
    var that = this;
    var params = {
      'type': 'ad'
    };
    app.util.api("product/ad_pro", 'GET', params, function(data) {
      if (data.success == 1) {
        for (var k = 0, length = data.data.length; k < length; k++) {
          if (data.data[k].limit_months < 12) {
            data.data[k].limit_months = data.data[k].limit_months + '月';
          } else {
            data.data[k].limit_months = data.data[k].limit_months / 12 + '年';
          }
          if (data.data[k].open_days == 1) {
            data.data[k].open_days = '当日';
          } else if (data.data[k].open_days == 2) {
            data.data[k].open_days = '次日';
          } else if (data.data[k].open_days == 3) {
            data.data[k].open_days = '3天';
          } else if (data.data[k].open_days == 7) {
            data.data[k].open_days = '1周';
          } else if (data.data[k].open_days == 14) {
            data.data[k].open_days = '2周';
          } else if (data.data[k].open_days == 21) {
            data.data[k].open_days = '3周';
          } else if (data.data[k].open_days == 30) {
            data.data[k].open_days = '1月';
          } else {
            data.data[k].open_days = data.data[k].open_days + '日';
          }
        }
        that.setData({
          pro: data.data
        })
        wx.hideLoading();
      } else {
        that.setData({
          pro: ''
        })
      }
    })
  },

  // 产品列表点击
  productTap:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/detail?id=' + id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      bag_id: options.id
    })
    this.bag_detail();
    this.ad_pro();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})