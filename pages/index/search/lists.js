var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    mechanism: '',
    page: 1
  },

  publish: function() {
    wx.navigateTo({
      url: 'publish',
    })
  },
  jump: function() {
    wx.navigateTo({
      url: 'middle',
    });
  },
  search: function() {
    wx.navigateTo({
      url: 'jump',
    });
  },
  /**
   * 发布
   */
  pub: function() {
    wx.showActionSheet({
      itemList: ['自定义发布', '使用模板发布'],
      success: function(res) {

        if (res.tapIndex == 0) {
          wx.navigateTo({
            url: '/pages/product/publish',
          })
        } else {
          wx.navigateTo({
            url: '/pages/product/template',
          })
        }
      },
      fail: function(res) {

      }
    })
  },
  productTap : function(e) {
    if (!app.util.checkauth(app)) {
      return;
    }

    var id = parseInt(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/index/detail?id=' + id,
    });
  },

  /**
   * 获取产品列表
   */
  ProductList: function() {
    var that = this;
    var params = {
      mechanism: that.data.mechanism,
      page: that.data.page
    };
    if (app.data.globalCity !== undefined){
      params.city_code = app.data.globalCity.val; 
    }else{
      params.city_code = '340100';
    }
    app.util.api('product/pro_list', 'GET', params, function(data) {
      if (data.success == 1) {
        for (var k = 0, length = data.data.length; k < length; k++) {
          if (data.data[k].limit_months < 12) {
            data.data[k].limit_months = data.data[k].limit_months + '月';
          } else {
            data.data[k].limit_months = data.data[k].limit_months / 12 + '年';
          }
          if (data.data[k].open_days == 1) {
            data.data[k].open_days = '当日';
          } else if (data.data[k].open_days == 2) {
            data.data[k].open_days = '次日';
          } else if (data.data[k].open_days == 3) {
            data.data[k].open_days = '3天';
          } else if (data.data[k].open_days == 7) {
            data.data[k].open_days = '1周';
          } else if (data.data[k].open_days == 14) {
            data.data[k].open_days = '2周';
          } else if (data.data[k].open_days == 21) {
            data.data[k].open_days = '3周';
          } else if (data.data[k].open_days == 30) {
            data.data[k].open_days = '1月';
          } else {
            data.data[k].open_days = data.data[k].open_days + '日';
          }
        }
        that.setData({
          list: that.data.list.concat(data.data)
        })
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      list: []
    })
    this.ProductList();
  },
  /**
   * 选项卡选择
   */
  swichNav: function(e) {
    var that = this;
    that.setData({
      list: [],
      page: 1,
      mechanism: e.currentTarget.dataset.hi,
      currentTab: e.target.dataset.num,
    })
    that.ProductList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    that.setData({
      page: that.data.page + 1,
    })
    that.ProductList();
  }
})