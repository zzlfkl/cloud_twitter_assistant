var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    credit: [{
        name: '抵押贷 ',
        value: '0',
        checked: false
      },
      {
        name: '信用贷 ',
        value: '1',
        checked: false
      },
      {
        name: '大数据 ',
        value: '2',
        checked: false
      },
      {
        name: '信用卡 ',
        value: '3',
        checked: false
      },
      {
        name: '过桥类 ',
        value: '4',
        checked: false
      },
    ],
    change: false
  },
  /**
   * 删除历史记录
   */
  del_his: function(e) {
    var that = this;
    var params = {
      'keyword': e.detail.value
    };
    wx.showModal({
      title: '删除历史记录',
      content: '确认删除历史记录吗？',
      success: function(res) {
        if (res.confirm) {
          app.util.api('userproduct/del_his', 'GET', params, function(data) {
            that.his();
          })
        }
      }
    })
  },
  credit: function(e) {
    var credit = this.data.credit;
    var checkArr = e.detail.value;
    for (var i = 0; i < credit.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        credit[i].checked = true;
      } else {
        credit[i].checked = false;
      }
    }
    this.setData({
      credit: credit
    })
  },
  mes: function(e) {
    var that = this;
    if (e.detail.value == '') {
      that.setData({
        change: false
      })
    } else {
      that.setData({
        change: true
      })
    }
  },
  /**
   * 用户点击搜索
   */
  search: function(e) {
    var that = this;
    var params = {
      'keyword': e.detail.value
    };
    wx.navigateTo({
      url: 'list?params=' + JSON.stringify(params)
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.his();
    this.hot();
  },
  /**
   * 网络请求
   */
  his: function(e) {
    var that = this;
    var params = {
      'type': 'his'
    };
    app.util.api('userproduct/his_search', 'GET', params, function(data) {
      if (data.success == 1) {
        that.setData({
          his: data.data
        })
      } else {
        that.setData({
          his: ''
        })
      }
    })
  },

  hot: function(e) {
    var that = this;
    var params = {
      'type': 'app_commend'
    };
    app.util.api('userproduct/app_commend', 'GET', params, function(data) {
      if (data.success == 1) {
        that.setData({
          hot: data.data
        })
      } else {
        that.setData({
          hot: ''
        })
      }
    })
  },
  suit: function(e) {
    var that = this;
    var params = {
      'keyword': e.detail.value
    };
    app.util.api('userproduct/suit_keyword', 'GET', params, function(data) {
      if (data.success == 1 && e.detail.value !== '') {
        that.setData({
          suit: data.data,
          change: true
        })
      } else {
        that.setData({
          suit: '',
          change: false
        })
      }
    })
  },
  /**
   * 产品详情
   */
  detail: function(e) {

    var index = parseInt(e.currentTarget.dataset.index);
    wx.navigateTo({
      url: '/pages/index/detail?id=' + index,
    });
  },
  /**
   * 点击适配关键字
   */
  suitlist: function(e) {
    var params = {
      'keyword': e.currentTarget.dataset.index
    };
    wx.navigateTo({
      url: 'list?params=' + JSON.stringify(params)
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})