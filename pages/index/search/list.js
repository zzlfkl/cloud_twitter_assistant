var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading: true,
    complete: true,
    show: true,
    page: 1,
    pro_list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      params: JSON.parse(options.params)
    })
    this.formSubmit();
  },

  productTap: function(e) {
    if (!app.util.checkauth(app)) {
      return;
    }
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/detail?id=' + id,
    });
  },

  formSubmit: function(e) {
    var that = this;
    var params = that.data.params;
    params.page = that.data.page;
    app.util.api('userproduct/keyword_search', 'GET', params, function(data) {
      if (that.data.page == 1 && data.data.length == 0){
        that.setData({
          show:false
        })
      }
      if (data.success == 1) {
        for (var k = 0, length = data.data.length; k < length; k++) {
          if (data.data[k].limit_months < 12) {
            data.data[k].limit_months = data.data[k].limit_months + '月';
          } else {
            data.data[k].limit_months = data.data[k].limit_months / 12 + '年';
          }
          if (data.data[k].open_days == 1) {
            data.data[k].open_days = '当日';
          } else if (data.data[k].open_days == 2) {
            data.data[k].open_days = '次日';
          } else if (data.data[k].open_days == 3) {
            data.data[k].open_days = '3天';
          } else if (data.data[k].open_days == 7) {
            data.data[k].open_days = '1周';
          } else if (data.data[k].open_days == 14) {
            data.data[k].open_days = '2周';
          } else if (data.data[k].open_days == 21) {
            data.data[k].open_days = '3周';
          } else if (data.data[k].open_days == 30) {
            data.data[k].open_days = '1月';
          } else {
            data.data[k].open_days = data.data[k].open_days + '日';
          }
        }
        that.setData({
          pro_list: that.data.pro_list.concat(data.data)
        })
      } 
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var  that = this;
    that.setData({
      page: that.data.page + 1, //每次触发上拉事件，把Page+1  
    });
    this.formSubmit();
  }
})