// pages/index/jump.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    credit: [{
        name: '抵押贷 ',
        value: '0',
        checked: false
      },
      {
        name: '信用贷 ',
        value: '1',
        checked: false
      },
      {
        name: '大数据 ',
        value: '2',
        checked: false
      },
      {
        name: '信用卡 ',
        value: '3',
        checked: false
      },
      {
        name: '过桥类 ',
        value: '4',
        checked: false
      },
      {
        name: '服务类 ',
        value: '5',
        checked: false
      },
      {
        name: '其他  ',
        value: '6',
        checked: false
      },
    ],
    condition: [{
        name: '房产',
        value: '0',
        checked: false
      },
      {
        name: '车产',
        value: '1',
        checked: false
      },
      {
        name: '保单',
        value: '2',
        checked: false
      },
      {
        name: '营业执照',
        value: '3',
        checked: false
      },
      {
        name: '薪金流水',
        value: '4',
        checked: false
      },
      {
        name: '学历',
        value: '5',
        checked: false
      },
      {
        name: '其他',
        value: '6',
        checked: false
      },
    ],
    mechanism: [{
        name: '正规银行',
        value: '0',
        checked: false
      },
      {
        name: '消费金融',
        value: '1',
        checked: false
      },
      {
        name: '小贷公司',
        value: '2',
        checked: false
      },
      {
        name: '个人服务',
        value: '3',
        checked: false
      },
      // { name: '其他', value: '4', checked: false },
    ],
    // commission: [{
    //     name: '0-1%',
    //     value: '0',
    //     checked: false
    //   },
    //   {
    //     name: '1.01-2%',
    //     value: '1',
    //     checked: false
    //   },
    //   {
    //     name: '2.01-3%',
    //     value: '2',
    //     checked: false
    //   },
    //   {
    //     name: '3.01-4%',
    //     value: '3',
    //     checked: false
    //   },
    //   {
    //     name: '4.01-6%',
    //     value: '4',
    //     checked: false
    //   },
    //   {
    //     name: '6%以上',
    //     value: '5',
    //     checked: false
    //   },
    //   // { name: '10%', value: '6', checked: false },
    // ],
    mon_interest: [{
        name: '0-0.6%',
        value: '0',
        checked: false
      },
      {
        name: '0.61-1%',
        value: '1',
        checked: false
      },
      {
        name: '1.01-1.5%',
        value: '2',
        checked: false
      },
      {
        name: '1.51-2%',
        value: '3',
        checked: false
      },
      {
        name: '2.01-3%',
        value: '4',
        checked: false
      },
      {
        name: '3%以上',
        value: '5',
        checked: false
      },

    ],
    pay_kind: [{
        name: '等本等息',
        value: '0',
        checked: false
      },
      {
        name: '等额本息',
        value: '1',
        checked: false
      },
      {
        name: '等额本金',
        value: '2',
        checked: false
      },
      {
        name: '先息后本',
        value: '3',
        checked: false
      },
      {
        name: '后本后息',
        value: '4',
        checked: false
      },
      {
        name: '复合方式',
        value: '5',
        checked: false
      },
      // { name: '其他', value: '6', checked: false },
    ],
    open_days: [{
        name: '当天',
        value: '0',
        checked: false
      },
      {
        name: '2-5日',
        value: '1',
        checked: false
      },
      {
        name: '6-10日',
        value: '2',
        checked: false
      },
      {
        name: '11-20日',
        value: '3',
        checked: false
      },
      {
        name: '20日以上',
        value: '4',
        checked: false
      }
    ],
    limit_months: [{
        name: '30日以内',
        value: '0',
        checked: false
      },
      {
        name: '1-12个月',
        value: '1',
        checked: false
      },
      {
        name: '1-3年',
        value: '2',
        checked: false
      },
      {
        name: '3-5年',
        value: '3',
        checked: false
      },
      {
        name: '5-10年',
        value: '4',
        checked: false
      },
      {
        name: '10年以上',
        value: '5',
        checked: false
      },
    ],
  },
  /**
   * 表单重置
   */
  clear: function(e) {
    var credit = this.data.credit;
    var condition = this.data.condition;
    var mechanism = this.data.mechanism;
    // var commission = this.data.commission;
    var mon_interest = this.data.mon_interest;
    var pay_kind = this.data.pay_kind;
    var open_days = this.data.open_days;
    var limit_months = this.data.limit_months;

    for (var i = 0; i < open_days.length; i++) {
      credit[i].checked = false;
      condition[i].checked = false;
      open_days[i].checked = false;
    }
    for (var i = 0; i < mon_interest.length; i++) {
      mon_interest[i].checked = false;
      pay_kind[i].checked = false;
      limit_months[i].checked = false;
      // commission[i].checked = false;
    }
    for (var i = 0; i < mechanism.length; i++) {
      mechanism[i].checked = false;
    }
    this.setData({
      credit: credit,
      condition: condition,
      mechanism: mechanism,
      // commission: commission,
      mon_interest: mon_interest,
      pay_kind: pay_kind,
      open_days: open_days,
      limit_months: limit_months
    })

  },

  credit: function(e) {

    var credit = this.data.credit;
    var checkArr = e.detail.value;
    for (var i = 0; i < credit.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        credit[i].checked = true;
      } else {
        credit[i].checked = false;
      }
    }
    this.setData({
      credit: credit
    })
  },

  condition: function(e) {
    var condition = this.data.condition;
    var checkArr = e.detail.value;
    for (var i = 0; i < condition.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        condition[i].checked = true;
      } else {
        condition[i].checked = false;
      }
    }
    this.setData({
      condition: condition
    })
  },
  
  mechanism: function(e) {

    var mechanism = this.data.mechanism;
    var checkArr = e.detail.value;
    for (var i = 0; i < mechanism.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        mechanism[i].checked = true;
      } else {
        mechanism[i].checked = false;
      }
    }
    this.setData({
      mechanism: mechanism
    })
  },
  // commission: function(e) {

  //   var commission = this.data.commission;
  //   var checkArr = e.detail.value;
  //   for (var i = 0; i < commission.length; i++) {
  //     if (checkArr.indexOf(i + "") != -1) {
  //       commission[i].checked = true;
  //     } else {
  //       commission[i].checked = false;
  //     }
  //   }
  //   this.setData({
  //     commission: commission
  //   })
  // },
  mon_interest: function(e) {

    var mon_interest = this.data.mon_interest;
    var checkArr = e.detail.value;
    for (var i = 0; i < mon_interest.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        mon_interest[i].checked = true;
      } else {
        mon_interest[i].checked = false;
      }
    }
    this.setData({
      mon_interest: mon_interest
    })
  },
  pay_kind: function(e) {

    var pay_kind = this.data.pay_kind;
    var checkArr = e.detail.value;
    for (var i = 0; i < pay_kind.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        pay_kind[i].checked = true;
      } else {
        pay_kind[i].checked = false;
      }
    }
    this.setData({
      pay_kind: pay_kind
    })
  },
  open_days: function(e) {

    var open_days = this.data.open_days;
    var checkArr = e.detail.value;
    for (var i = 0; i < open_days.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        open_days[i].checked = true;
      } else {
        open_days[i].checked = false;
      }
    }
    this.setData({
      open_days: open_days
    })
  },
  limit_months: function(e) {

    var limit_months = this.data.limit_months;
    var checkArr = e.detail.value;
    for (var i = 0; i < limit_months.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        limit_months[i].checked = true;
      } else {
        limit_months[i].checked = false;
      }
    }
    this.setData({
      limit_months: limit_months
    })
  },

  /**
   * 搜索
   */
  formSubmit: function(e) {
    var that = this;
    var params = e.detail.value;
    wx.navigateTo({
      url: 'search_result/list?params=' + JSON.stringify(params)
    })
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})