var  app  = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {

  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that   = this;
    var scene = options.scene;
    if (!scene) {
      wx.showToast({
        title: '获取失败，请重新扫码！',
        icon: 'none'
      });
      return;
    }
    that.setData({
      user_id: scene
    })
    that.login();
  },

  // 显示头像
  showAvatar: function () {
    var that = this
    var avatar = that.data.avatar
    if (avatar && avatar.length > 0) {
      wx.previewImage({
        urls: [avatar],
      })
    } 
  },

  // 获取二维码信息
  get_qr:function(){
    var that = this;
    var params = {
      user_id: that.data.user_id,
    };
    app.util.api('index/qr_extension', 'GET', params, function (data) {
      if (data.success == 1) {
        that.setData({
          avatar: data.data.avatar,
          nickname: data.data.user_nickname
        })
      } else {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 登录操作
   */
  login:function(){
    var that = this;
    wx.login({
      success: function (res) {
        if (res.code) {
          //发起网络请求
          var params = {
            "code": res.code
          }
          app.util.api('login/login', 'POST', params, function (data) {
            if (data.success == 1) {
              app.data.userinfo = data.data;
              wx.setStorageSync('token', data.other);
              that.get_qr();
              if (data.data.mobile == '') {
                that.setData({
                  MobileStatus: 0
                })
              } else {
                that.setData({
                  MobileStatus: 1
                })
              }
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      },
      fail: function (e) {
        console.log(e)
      }
    }); 
  },

  /**
   * 绑定推广人
   */
  bind:function(e){
    var that = this;
    var params = {
      user_id: that.data.user_id,
    };
    if (app.data.userinfo == undefined){
      wx.showToast({
        title: '获取用户信息失败，请重启应用',
        icon: 'none'
      })
      return;
    }
    
    app.util.api('userinfo/qr_post','POST',params,function(data){
      if (data.success == 1) {
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
        setTimeout(function () {
          wx.reLaunch({
            url: '/pages/index/index'
          })
        }, 1500)
      } else {
        wx.showModal({
          title: '绑定失败',
          content: data.msg,
          showCancel: false
        })
      }
    })
  },

  /**
 * 绑定手机号
 */
  getPhoneNumber: function (e) {
    var that = this;
    var params = {
      'status': e.detail.errMsg,
      'iv': e.detail.iv,
      'encryptedData': e.detail.encryptedData,
    };
    app.util.api('userinfo/bind_mobile', 'POST', params, function (data) {
      if (data.success == 1) {
        // 绑定手机号成功，自动绑定推广人
        var params = {
          user_id: that.data.user_id,
        };
        app.util.api('userinfo/qr_post', 'POST', params, function (data) {
          if (data.success == 1) {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.reLaunch({
                url: '/pages/index/index'
              })
            }, 1500)
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        })

        //  判断是否返回了TOKEN
        if (data.data !== 1) {
          wx.setStorageSync('token', data.data)
        }
        that.setData({
          phone: true
        })
        wx.showToast({
          title: data.msg,
        })
      } else {

        // 获取手机号失败，提示通过验证码绑定
        wx.showModal({
          title: '获取号码失败',
          content: '通过验证码绑定手机号',
          confirmText: '去获取',
          success: function (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/user/userinfo/bindPhone/bind_phone?extension_id=' + that.data.user_id,
              })
            }
          }
        })
      }
    })
  },
  /**
   * 取消
   */
  cancel:function(){
    wx.reLaunch({
      url: '/pages/index/index'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})